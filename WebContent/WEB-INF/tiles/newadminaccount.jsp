<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<head>

	<script src="${pageContext.request.contextPath}/static/script/angular.min.js"></script>
	<script src="${pageContext.request.contextPath}/static/script/createadmin/createadmin.js"></script>
	<script src="${pageContext.request.contextPath}/static/script/createreport/lodash.min.js"></script>
	<link href="${pageContext.request.contextPath}/static/css/createreport/bootstrap.min.css" rel="stylesheet" >
	<link href="${pageContext.request.contextPath}/static/css/createadmin/createadmin.css" rel="stylesheet" >
	<script src="${pageContext.request.contextPath}/static/script/createreport/moment-with-locales.js"></script>

</head>

<div ng-app="prasbicApp" class="divContainer" ng-controller="CreateAdminCtrl">
	<div class="formDiv">
	<sec:authentication property="principal" var="principal"/>
		<h1 class="titleH">Create Admin Account</h1>
		<form class="createAdminForm" ng-submit="onSubmit()"
				novalidate="novalidate"
			  	ng-hide="submitted" 
			  	ng-model = "username"
			  	ng-init = "username='${principal.username}'">
			  
			  <div class="form-group">
				  <label for="username" class="control-label">Username</label >
				  <input type="text"
				       class="form-control"
					   ng-model="formModel.username"
				       id="username"
					   required="required" >
			   </div >
			   <div class="form-group">
				  <label for="email" class="control-label">Email</label >
				  <input type="email"
				       class="form-control"
					   ng-model="formModel.email"
				       id="email"
					   required="required" >
			   </div >
			   <div class="form-group">
				  <label for="password" class="control-label">Password</label >
				  <input type="password"
				       class="form-control"
					   ng-model="formModel.password"
				       id="password"
					   required="required" >
			   </div >
			   <div class="form-group">
				  <label for="confirmpass" class="control-label">Confirm Password</label >
				  <input type="password"
				       class="form-control"
					   ng-model="formModel.confirmpass"
				       id="confirmpass"
					   required="required" >
					   <div id="matchpass"></div>
			   </div >
			   <div class="form-group">
				  <label for="firstname" class="control-label">First Name</label >
				  <input type="text"
				       class="form-control"
					   ng-model="formModel.firstname"
				       id="firstname"
					   required="required" >
			   </div >
			   <div class="form-group">
				  <label for="lastname" class="control-label">Last Name</label >
				  <input type="text"
				       class="form-control"
					   ng-model="formModel.lastname"
				       id="lastname"
					   required="required" >
			   </div >
			   <div class="form-group">
			   		<button class="btn btn-primary"type="submit">Create Admin</button>
		   	   </div>
	  	</form>
	</div>
</div>
<script src="${pageContext.request.contextPath}/static/script/createreport/jcs-auto-validate.min.js" ></script >
<script src="${pageContext.request.contextPath}/static/script/createreport/spin.min.js" ></script >
<script src="${pageContext.request.contextPath}/static/script/createreport/ladda.min.js" ></script >
<script src="${pageContext.request.contextPath}/static/script/createreport/angular-ladda.min.js" ></script >
<!--<sf:form class="myform" method="post"
	action="${pageContext.request.contextPath}/createadminaccount"
	commandName="user">
	<div class="form-group">
		<label for="username">Username:</label>
		<sf:input type="text" class="form-control control" path="username"
			id="username" placeholder="Username" />
		<div class="forminputerror">
			<sf:errors path="username"></sf:errors>
		</div>
	</div>
	<div class="form-group">
		<label for="email">Email:</label>
		<sf:input type="text" class="form-control control" path="email"
			id="email" placeholder="Email" />
		<div class="forminputerror">
			<sf:errors path="email"></sf:errors>
		</div>
	</div>
	<div class="form-group">
		<label for="password">Password:</label>
		<sf:input type="password" class="form-control control" path="password"
			id="password" placeholder="Password" />
		<div class="forminputerror">
			<sf:errors path="password"></sf:errors>
		</div>
	</div>
	<div class="form-group">
		<label for="confirmpass">Confirm Password:</label>
		<input type="password" class="form-control control"
			id="confirmpass" placeholder="Confirm Password" />
		<div id="matchpass"></div>
	</div>
	<div class="form-group">
		<label for="firstname">First Name:</label>
		<sf:input type="text" class="form-control control" path="firstname"
			id="firstname" placeholder="First Name" />
		<div class="forminputerror">
			<sf:errors path="firstname"></sf:errors>
		</div>
	</div>
	<div class="form-group">
		<label for="lastname">Last Name:</label>
		<sf:input type="text" class="form-control control" path="lastname"
			id="lastname" placeholder="Last Name" />
		<div class="forminputerror">
			<sf:errors path="lastname"></sf:errors>
		</div>
	</div>
	<button type="submit" class="btn btn-default control">Submit</button>
</sf:form>-->