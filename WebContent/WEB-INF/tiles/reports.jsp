<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<head>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9z1qJLw1fTEoHn1_JxuggptNZFLOz56o&callback=initMap"></script>
<script
	src="${pageContext.request.contextPath}/static/script/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/reports/livenewsfeed.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/reports/appdirective.js"></script>
<link href="${pageContext.request.contextPath}/static/css/bootstrap-theme.min.css" rel="stylesheet" media="screen"/> 
<link href="${pageContext.request.contextPath}/static/css/reports/reports.css" rel="stylesheet" media="screen"/> 

</head>
<sec:authentication property="principal" var="principal" />
<div class="overall-container" ng-app="myApp" ng-model="username"
	ng-init="username='${principal.username}'">
	<div>
		<div class="container1">
			<div class="myMap">
				<my-maps ng-init="getReports()" id="map"></my-maps>
			</div>
			<div class="seperation"></div>
			<div ng-controller="ResidentInfoCtrl" class="statistical_container" ng-init="setVolInfo()" class="statistical_container" ng-init="setVolInfo()">
				<div class="row_container">

					<div class="modal fade" id="myModal" role="dialog">
						<div class="modal-dialog modal-lg" ng-model="user"
							ng-init="getUser(memId)">
							<div class="modal-content">

								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Detailed Report View</h4>
								</div>

								<div class="modal-body">
									<div id="main_content">
										<div id="user_content">
											<div id="image_container">
												<img class="img-child"
													src="${pageContext.request.contextPath}/static/images/blank_user.png"
													alt="Blank User">
											</div>
											<div id="contact_info"><b>Username:</b> {{currUser.username}}</div>
											<div id="contact_info"><b>Address:</b> {{currUser.address}}</div>
											<div id="contact_info"><b>Phone:</b> {{currUser.phone}}</div>
											<div id="contact_info"><b>ECR:</b> {{currUser.ecr}}</div>
											<div id="contact_info"><b>ECN:</b> {{currUser.ecn}}</div>
											<div id="contact_info" ng-show="currUser.disability == true"><b>Disabilities:</b> Yes</div>
											<div id="contact_info" ng-show="currUser.disability == false"><b>Disabilities:</b> No</div>
											<div id="contact_info"><b>Elderly:</b> {{currUser.isElderly}}</div>
										</div>
										<div id="report_content">
											<div class="my_report_container">
												<table class="table table-striped">
													<thead>
														<tr>
															<th>Address</th>
															<th>Category</th>
															<th>Description</th>
															<th>Status</th>
														</tr>
													</thead>
													<tbody>
														<tr ng-repeat="row in currUser.reports | orderBy:'-id'">
															<td>{{row.address1}}</td>
															<td>{{row.category}}</td>
															<td ng-class="class">{{class}} {{row.description}}</td>
															<td ng-if="row.status">
																<button ng-disabled="buttonAvailability" ng-click="openUserBtnClicked(row, currUser)" ng-class="btntoggle">{{buttonText}}</button>
															</td>
															<td ng-if="!row.status">
																<button class="status_button btn btn-default" disabled>Closed</button>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<table class="table table-striped">
						<thead>
							<tr>
								<th>Username</th>
								<th>Email</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Reports</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="row in resObjs">
								<td>{{row.username}}</td>
								<td>{{row.email}}</td>
								<td>{{row.firstname}}</td>
								<td>{{row.lastname}}</td>
								<td><a href="#" data-toggle="modal"
									ng-click="setUserInfo(row)" data-target="#myModal"> <u>{{row.checkupslength}}/{{row.reportlength}}</u>
								</a></td>
							</tr>

						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
	<div>
		<div class="container2">
			<div class="inline_container">
				<div class="live_feed_title">
					<u><h2>Live Reports</h2></u>
				</div>
				<div class="report-container">
					<div class="individual_report_container">
						<div class="individual_report"
							ng-repeat="report in reportsInVolArea | orderObjectBy:'id':true track by $index"
								ng-class="{'selected':$index == selectedRow}" ng-click="setClickedRow($index, report)">
							<div class="username_date_container">
								<div class="username_container">
									<a href="#">{{report.user.username}}</a>
								</div>
								<div class="date_container">{{report.date | date:'dd MMM yyyy'}}</div>
							</div>
							<div class="category_container">
								<b>Category:</b> {{report.category}}
							</div>
							<div class="address_container">
								<b>Address:</b> {{report.address1}}
							</div>
							<div class="description_container">
								<b>Description:</b> {{report.description}}
							</div>
							<hr />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>