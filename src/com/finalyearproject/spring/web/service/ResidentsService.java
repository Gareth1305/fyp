package com.finalyearproject.spring.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.finalyearproject.spring.web.dao.Resident;
import com.finalyearproject.spring.web.dao.ResidentsDao;

@Service("residentsService")
public class ResidentsService {
	
	private ResidentsDao residentsDao;

	@Autowired
	public void setResidentsDao(ResidentsDao residentsDao) {
		this.residentsDao = residentsDao;
	}
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public void createResident(Resident resident) {
		residentsDao.create(resident);
	}
	
	public boolean exists(String username) {
		return residentsDao.exists(username);
	}
	
	public List<Resident> getAllResidents() {
		return residentsDao.getAllResidents();
	}
	
	public Resident getResidentByUsername(String username, String password) {
		Resident resident = residentsDao.getResidentByUsername(username);
		if(passwordEncoder.matches(password, resident.getPassword())) {
			return resident;
		} else {
			return null;
		}
	}
	
	public Resident getResidentByUsername(String username) {
		Resident resident = residentsDao.getResidentByUsername(username);
		return resident;
	}

	public List<Resident> getElderlyAndDisabledResidents() {
		return residentsDao.getElderlyAndDisabledResidents();
	}

	public List<Resident> getResidentsByCouncilName(String councilname) {
		return residentsDao.getResidentsByCouncilName(councilname);
	}

	public boolean doesUsernameExist(String username) {
		return residentsDao.doesUsernameExist(username);
	}
	
}
