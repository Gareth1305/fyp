var viewAllUsers = angular.module("viewAllUsers", []);

viewAllUsers.controller('ViewAllUsersCtrl', ['$scope', '$http', function($scope, $http) {
	$scope.sortType = 'username';
	$scope.sortReverse = false;
	$scope.searchUser = '';
	$scope.currentPage = 0;
    $scope.pageSize = 12;
	$scope.allUsers = [];

	$scope.numberOfPages = function() {
        return Math.ceil($scope.allUsers.length/$scope.pageSize);                
    };
	
	$http.get('http://localhost:8080/prasbic/api/getAllUsers').success(function(users) {
		users.forEach(function(user) {
			$scope.allUsers.push(user);
		});
	});
	
	$scope.suspendAccount = function(user) {
		console.log(user);
		alert("You want to suspend " + user.username);
	}
}]);

viewAllUsers.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});
