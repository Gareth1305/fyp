package com.finalyearproject.spring.web.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finalyearproject.spring.web.dao.Checkup;
import com.finalyearproject.spring.web.dao.Volunteer;
import com.finalyearproject.spring.web.service.CheckupsService;
import com.finalyearproject.spring.web.service.VolunteersService;

@Controller
@RequestMapping("/api")
public class RestCheckupsController {
	
	private CheckupsService checkupsService;
	
	@Autowired
	public void setCheckupsService(CheckupsService checkupsService) {
		this.checkupsService = checkupsService;
	}
	
	private VolunteersService volunteersService;

	@Autowired
	public void setVolunteersService(VolunteersService volunteersService) {
		this.volunteersService = volunteersService;
	}
	
	@RequestMapping(value = "/getAllCheckups", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Checkup> getAllCheckups() {
		return checkupsService.getAllCheckups();
	}
	
	@RequestMapping(value = "/getAllCheckedCheckups", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Checkup> getAllCheckedCheckups() {
		return checkupsService.getAllCheckedCheckups();
	}
	
	@RequestMapping(value = "/getAllCheckedCheckupsByCouncil/{council}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Checkup> getAllCheckedCheckupsByCouncil(@PathVariable String council) {
		ArrayList<Checkup> checkupsArray = 
				(ArrayList<Checkup>) checkupsService.getAllCheckedCheckupsByCouncil(council);
		return checkupsArray;
	}
	
	@RequestMapping(value = "/getCheckupsByUsername/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Checkup> getReportsByUsername(@PathVariable String username) {
		ArrayList<Checkup> checkupsArray = 
				(ArrayList<Checkup>) checkupsService.getCheckupsByUsername(username);
		return checkupsArray;
	}
	
	@RequestMapping(value = "/getAllVolunteersFromCheckups", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<String> getAllVolunteersFromCheckups() {
		ArrayList<String> volunteersNames = (ArrayList<String>) checkupsService.getVolunteersNames();
		return volunteersNames;
	}
	
	@RequestMapping(value = "/checkedOnUser", method = RequestMethod.POST)
	@ResponseBody
	public void checkedOnUser(@RequestBody String user_report_info) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
				map = mapper.readValue(user_report_info, new TypeReference<HashMap<String, String>>() {
				});
		updateCheckupsTable(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateCheckupsTable(Map<String, String> map) {
		String id = (String) map.get("id");
		int idToUse = Integer.parseInt(id);
		String volUsername = (String) map.get("volUsername");
		Volunteer vol = volunteersService.getVolunteerByUsername(volUsername);
		Checkup checkup = checkupsService.getCheckupByReportId(idToUse);
		checkup.setChecked(true);
		checkup.setVolunteer(vol);
		checkupsService.saveOrUpdate(checkup);
	}
}
