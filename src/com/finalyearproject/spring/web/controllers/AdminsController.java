package com.finalyearproject.spring.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.finalyearproject.spring.web.dao.FormValidationGroup;
import com.finalyearproject.spring.web.dao.User;
import com.finalyearproject.spring.web.service.UsersService;

@Controller
public class AdminsController {

	private UsersService usersService;

	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}

	@RequestMapping("/admin")
	public String showAdmin(Model model) {
		List<User> users = usersService.getAllUsers();
		model.addAttribute("users", users);
		return "admin";
	}

	@RequestMapping("/newadminaccount")
	public String showNewAdminAccount(Model model) {
		model.addAttribute("user", new User());
		return "newadminaccount";
	}

	@RequestMapping("/viewallreports")
	public String viewAllReports() {
		return "viewallreports";
	}
	
	@RequestMapping(value = "/createadminaccount", method = RequestMethod.POST)
	public String createAdminAccount(@Validated(FormValidationGroup.class) User user, BindingResult result) {
		System.out.println("IM CALLED");
		if (result.hasErrors()) {
			System.out.println("IN FIRST IF");
			return "newadminaccount";
		}

		user.setEnabled(true);
		user.setAuthority("ROLE_ADMIN");

		if (usersService.exists(user.getUsername())) {
			System.out.println("IN SECOND IF");
			result.rejectValue("username", "DuplicateKey.user.username");
			return "newadminaccount";
		}

		try {
			System.out.println("IN TRY");
			usersService.create(user);
		} catch (DataAccessException e) {
			result.rejectValue("username", "DuplicateKey.user.username");
			return "newadminaccount";
		}

		return "adminaccountcreated";
	}
	
	@RequestMapping("/adminaccountcreated")
	public String adminAccountCreated() {
		return "adminaccountcreated";
	}

	@RequestMapping("/viewallusers")
	public String viewAllUsers() {
		return "viewallusers";
	}
}
