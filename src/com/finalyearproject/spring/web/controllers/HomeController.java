package com.finalyearproject.spring.web.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.finalyearproject.spring.web.dao.Report;
import com.finalyearproject.spring.web.service.ReportsService;

@Controller
public class HomeController {
	
	@Autowired
	private ReportsService reportsService;
	
	@RequestMapping("/")
	public String showHome(Model model, Principal principal) {
		List<Report> reports = reportsService.getCurrent();
		model.addAttribute("reports", reports);
		return "home";
	}

	@RequestMapping("/home")
	public String showHome2() {
		return "/";
	}
	
	@RequestMapping("/statistics")
	public String statistics() {
		return "statisticalview";
	}
}
