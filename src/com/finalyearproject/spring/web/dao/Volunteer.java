package com.finalyearproject.spring.web.dao;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "volunteers")
public class Volunteer extends User {

    private String address;
    private String city;
    private String county;
    private Date dob;
    private String phonenumber;
	private double latitude;
	private double longitude;
	@ManyToOne
	@JoinColumn(name="councilname")
	private Council council;
	
    public Volunteer() {

    }

    public Volunteer(String username, String password, String email, boolean enabled, String authority, String firstname,
                     String lastname, String address, String city, String county, Date dob, String phonenumber,
                     double latitude, double longitude, Council council) {
        super(username, password, email, enabled, authority, firstname, lastname);
        this.address = address;
        this.city = city;
        this.county = county;
        this.dob = dob;
        this.phonenumber = phonenumber;
        this.latitude = latitude;
        this.longitude = longitude;
        this.council = council;
    }

    public Council getCouncil() {
		return council;
	}

	public void setCouncil(Council council) {
		this.council = council;
	}

	public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Volunteer [address=" + address + ", city=" + city + ", county=" + county + ", dob=" + dob
				+ ", phonenumber=" + phonenumber + ", latitude=" + latitude
				+ ", longitude=" + longitude + "]";
	}
	
	
}
