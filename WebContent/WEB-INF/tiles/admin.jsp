<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<table class="formtable">
	<tr>
		<th class="formtabledata">Username</th>
		<th class="formtabledata">Email</th>
		<th class="formtabledata">Role</th>
		<th class="formtabledata">Enabled</th>
	</tr>
	<c:forEach var="user" items="${users}">
		<tr>
			<td class="formtabledata"><c:out value="${user.username}"></c:out></td>
			<td class="formtabledata"><c:out value="${user.email}"></c:out></td>
			<td class="formtabledata"><c:out value="${user.authority}"></c:out></td>
			<td class="formtabledata"><c:out value="${user.enabled}"></c:out></td>
		</tr>
	</c:forEach>
</table>
