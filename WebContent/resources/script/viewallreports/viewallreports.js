var myApp = angular.module("allReportsView", []);

myApp.controller('AllReportsCtrl', ['$scope', '$http', function($scope, $http, $dialogs) {

	$scope.sortType = 'id';
	$scope.sortReverse = false;
	$scope.allReports = [];
	$scope.pageSize = 12;
	$scope.searchReport = '';
	$scope.currentPage = 0;
	$scope.activeReport = {};
	
	$http.get('http://localhost:8080/prasbic/api/getAllReports').success(function(reports) {
		reports.forEach(function(report) {
			$scope.allReports.push(report);
		});
		console.log(reports);
	});
	
	$scope.viewReport = function(report) {
		$scope.activeReport = report;
		console.log(report);
		switch ($scope.activeReport.category) {
	    case "Theft":
	    	report.imageTitle = "theftLarge.png";
	        break;
	    case "Burglary":
	    	report.imageTitle = "burglaryLarge.png";
	        break;
	    case "Fighting":
	    	report.imageTitle = "fightingLarge.png";
	        break;
	    case "Alcohol Abuse":
	    	report.imageTitle = "alcoholLarge.jpg";
	        break;
	    case "Drug Abuse":
	    	report.imageTitle = "drugLarge.png";
	        break;
	    case "Vandalism":
	    	report.imageTitle = "vandalismLarge.png";
	        break;
	    case "Joy Riding":
	    	report.imageTitle = "joyRidingLarge.jpg";
	        break;
	    case "Other":
	    	report.imageTitle = "otherLarge.png";
	    	break;
		}
		console.log(report)
		$http.get('http://localhost:8080/prasbic/api/getNumOfReportsPerUser/'+report.user.username).success(function(numberOfReports) {
			report.user.numberOfReports = numberOfReports;
		});
	};
	
	$scope.numberOfPages = function() {
		return Math.ceil($scope.allReports.length / $scope.pageSize);
	};
}]);

myApp.filter('startFrom', function() {
	return function(input, start) {
		start = +start; // parse to int
		return input.slice(start);
	}
});