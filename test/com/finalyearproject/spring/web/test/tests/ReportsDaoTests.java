package com.finalyearproject.spring.web.test.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.finalyearproject.spring.web.dao.Council;
import com.finalyearproject.spring.web.dao.CouncilsDao;
import com.finalyearproject.spring.web.dao.Report;
import com.finalyearproject.spring.web.dao.ReportsDao;
import com.finalyearproject.spring.web.dao.Resident;
import com.finalyearproject.spring.web.dao.ResidentsDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = { "classpath:com/finalyearproject/spring/web/config/dao-context.xml",
		"classpath:com/finalyearproject/spring/web/config/security-context.xml",
		"classpath:com/finalyearproject/spring/web/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class ReportsDaoTests {

	@Autowired
	private ReportsDao reportsDao;
	
	@Autowired
	private CouncilsDao councilsDao;
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private ResidentsDao residentsDao;
	
	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		jdbc.execute("delete from checkups");
		jdbc.execute("delete from reports");
		jdbc.execute("delete from residents");
		jdbc.execute("delete from volunteers");
		jdbc.execute("delete from councils");
		jdbc.execute("delete from users");
	}

	@SuppressWarnings("deprecation")
	public void createDummyData() {
		Council council = new Council("Fingal County Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council);
		
		Council council2 = new Council("Dublin City Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council2);
		
		Date myDob1 = new Date();
		myDob1.setYear(myDob1.getYear() - 72);
		Resident resident = new Resident("JBloggs1", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob1, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council);
		residentsDao.create(resident);
		
		Date dateInstance1 = new Date();
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(dateInstance1);
		cal1.add(Calendar.HOUR, -2);
		Date last24Hours = cal1.getTime();
		//4 Reports in the last 24 hours
		Report report1 = new Report(resident, council2, last24Hours, "12 Swords Manor Way", 
				"Swords", "Dublin", 53.461638, -6.242750, "Theft", "Someone stole my car");
		Report report2 = new Report(resident, council, last24Hours, "1 Swords Manor Drive", 
				"Swords", "Dublin", 53.461638, -6.242750, "Fighting", "Two men fighting at this house");
		Report report3 = new Report(resident, council2, last24Hours, "13 Swords Manor Drive", 
				"Swords", "Dublin", 53.461638, -6.242750, "Burglary", "A woman broke  into my house");
		Report report4 = new Report(resident, council, last24Hours, "22 Swords Manor Grove", 
				"Swords", "Dublin", 53.461638, -6.242750, "Burglary", "A man broke into my car");
		
		Date dateInstance2 = new Date();
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(dateInstance2);
		cal2.add(Calendar.DATE, -2);
		Date lastWeek = cal2.getTime();
		//3 Reports in the last hour
		Report report5 = new Report(resident, council2, lastWeek, "10 Swords Manor Way", 
				"Swords", "Dublin", 53.461638, -6.242750, "Theft", "A man stole my car");
		Report report6 = new Report(resident, council, lastWeek, "9 Swords Manor Drive", 
				"Swords", "Dublin", 53.461638, -6.242750, "Fighting", "Two girls fighting at this house");
		Report report7 = new Report(resident, council, lastWeek, "7 Swords Manor Drive", 
				"Swords", "Dublin", 53.461638, -6.242750, "Burglary", "A teenager broke  into my house");
		
		Date dateInstance3 = new Date();
		Calendar cal3 = Calendar.getInstance();
		cal3.setTime(dateInstance3);
		cal3.add(Calendar.DATE, -17);
		Date lastMonth = cal3.getTime();
		//2 Reports in the last hour
		Report report8 = new Report(resident, council2, lastMonth, "111 Swords Manor Way", 
				"Swords", "Dublin", 53.461638, -6.242750, "Theft", "A man stole my motorbike");
		Report report9 = new Report(resident, council2, lastMonth, "221 Swords Manor Drive", 
				"Swords", "Dublin", 53.461638, -6.242750, "Fighting", "Two boys fighting at this house");
		
		List<Report> reports = new ArrayList<>();
		reports.add(report1);
		reports.add(report2);
		reports.add(report3);
		reports.add(report4);
		reports.add(report5);
		reports.add(report6);
		reports.add(report7);
		reports.add(report8);
		reports.add(report9);
		
		for(Report rep: reports) {
			reportsDao.saveOrUpdate(rep);
		}
	}
	
	@Test
	public void testReportCreation() {
		createDummyData();
		assertEquals("Report table should not be empty", 9, reportsDao.getReports().size());
		assertNotEquals("Report table should not be empty", -9, reportsDao.getReports().size());
	}

	@Test
	public void testReportRetrievalForPast24Hours() {
		createDummyData();
		assertEquals("4 reports have been created that occur in past 24 hours", 4, reportsDao.getReportsFromPast24Hours().size());
	}
	
	@Test
	public void testReportRetrievalForPastWeek() {
		createDummyData();
		assertEquals("3 reports have been created that occur in past week", 7, reportsDao.getReportsFromPastWeek().size());
	}
	
	@Test
	public void testReportRetrievalForPastMonth() {
		createDummyData();
		assertEquals("2 reports have been created that occur in past month", 9, reportsDao.getReportsFromPastMonth().size());
	}
	
	@Test
	public void testReportRetrievalForCouncils() {
		createDummyData();
		assertEquals("0 reports have been created that occur in Cork City Council", 0, reportsDao.getAllReportsByCouncil("Cork City Council").size());
		assertEquals("5 reports have been created that occur in Dublin City Council", 5, reportsDao.getAllReportsByCouncil("Dublin City Council").size());
		assertEquals("4 reports have been created that occur in Fingal County Council", 4, reportsDao.getAllReportsByCouncil("Fingal County Council").size());
	}
	

}
