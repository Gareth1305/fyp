package com.finalyearproject.spring.web.service;

import com.finalyearproject.spring.web.dao.Volunteer;
import com.finalyearproject.spring.web.dao.VolunteersDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("volunteersService")
public class VolunteersService {
	
	private VolunteersDao volunteersDao;

	@Autowired
	public void setVolunteersDao(VolunteersDao volunteersDao) {
		this.volunteersDao = volunteersDao;
	}
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public void createVolunteer(Volunteer volunteer) {
		volunteersDao.create(volunteer);
	}
	
	public boolean exists(String username) {
		return volunteersDao.exists(username);
	}
	
	public List<Volunteer> getAllVolunteers() {
		return volunteersDao.getAllVolunteers();
	}
	
	public Volunteer getVolunteerByUsername(String username, String password) {
		Volunteer volunteer = volunteersDao.getVolunteerByUsername(username);
		if(passwordEncoder.matches(password, volunteer.getPassword())) {
			return volunteer;
		} else {
			return null;
		}
	}
	
	public Volunteer getVolunteerByUsername(String username) {
		Volunteer volunteer = volunteersDao.getVolunteerByUsername(username);
		return volunteer;
	}
	
}
