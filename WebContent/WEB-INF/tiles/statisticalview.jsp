<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<head>
<script
	src="${pageContext.request.contextPath}/static/script/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/statisticalview/statisticalview.js"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9z1qJLw1fTEoHn1_JxuggptNZFLOz56o"></script>
<script type="text/javascript"
	src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript"
	src="http://www.amcharts.com/lib/3/themes/light.js"></script>
<script type="text/javascript"
	src="http://www.amcharts.com/lib/3/pie.js"></script>
<script type="text/javascript"
	src="http://www.amcharts.com/lib/3/radar.js"></script>
<script type="text/javascript"
	src="http://www.amcharts.com/lib/3/serial.js"></script>
<script type="text/javascript"
	src="http://www.amcharts.com/lib/3/plugins/export/export.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript"
	src="http://www.amcharts.com/lib/3/plugins/export/export.js"></script>
<script type="text/javascript" src="https://cdn.emailjs.com/dist/email.min.js"></script>

<link href="${pageContext.request.contextPath}/static/css/statisticalview/statisticalview.css" rel="stylesheet" media="screen"/> 

</head>

<div class="divContainer" ng-app="statsView" ng-controller="statsCtrl"
	ng-init="getAllInfo()">
	<div class="sidebarContainer">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Council - <a href="#" ng-click="exportCharts()">Export to PDF</a>	</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="#" ng-click="allClicked()">All</a></td>
				</tr>
				<tr ng-repeat="council in councilInformation">
					<td><a href="#" ng-click="councilClicked(council)">{{council.councilname}}</a></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div ng-init="getAllUsers()" class="contentContainer">
		<div class="userInfoContainer">
			<div class="userChartsContainer">
				<div class="chart1Container">
					<div class="userInfoContainer" id="chartdiv1"
						style="width: 100%; height: 100%; background-color: #FFFFFF;"></div>
				</div>
				<div class="chart2Container">
					<div class="userInfoContainer" id="chartdiv2"
						style="width: 100%; height: 100%; background-color: #FFFFFF;"></div>
				</div>
			</div>
		</div>
		<div class="reportInfoContainer">
			<div class="reportChartsContainer">
				<div class="chart1Container">
					<div id="chartdiv"
						style="width: 100%; height: 100%; background-color: #FFFFFF;"></div>
				</div>
				<div class="report2Container">
					<div class="userInfoContainer" id="chartdiv3"
						style="width: 100%; height: 100%; background-color: #FFFFFF;"></div>
				</div>
			</div>
		</div>
		<div class="reportInfoContainer2">
			<div class="reportChartsContainer">
				<div class="chart1Container">
					<div id="chartdiv4"
						style="width: 100%; height: 100%; background-color: #FFFFFF;"></div>
				</div>
				<div class="report2Container">
					<div id="chartdiv5"
						style="width: 100%; height: 100%; background-color: #FFFFFF;"></div>
				</div>
			</div>
			<!--<input type="button" value="Export charts to PDF" ng-click="exportCharts()" />-->
		</div>
		<div class="userStatsContainer">
			{{reportResidentsCount}}
			<!--<input type="button" value="Export charts to PDF" ng-click="exportCharts()" />-->
			<table class="table table-bordered table-striped">
				<tr>
					<td class="resTitle"><b>Most Active Residents</b></td>
					<td class="resData"
						ng-repeat="res in allMyRes | orderBy:'-value' | limitTo:5">
						{{res.name}} ({{res.value}})</td>
				</tr>
				<tr>
					<td class="volTitle"><b> Most Active Volunteers </b></td>
					<td class="volData"
						ng-repeat="vol in allMyVol | orderBy:'-value' | limitTo:5">
						{{vol.name}} ({{vol.value}})</td>
				</tr>
			</table>		
		</div>
	</div>
</div>