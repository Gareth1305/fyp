package com.finalyearproject.spring.web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalyearproject.spring.web.dao.Checkup;
import com.finalyearproject.spring.web.dao.CheckupsDao;

@Service("checkupsService")
public class CheckupsService {
	
	private CheckupsDao checkupsDao;

	@Autowired
	public void setCheckupsDao(CheckupsDao checkupsDao) {
		this.checkupsDao = checkupsDao;
	}
	
	public void saveOrUpdate(Checkup checkup) {
		checkupsDao.saveOrUpdate(checkup);
	}
	
	public List<Checkup> getAllCheckups() {
		return checkupsDao.getAllCheckups();
	}
	
	public List<Checkup> getAllCheckedCheckups() {
		return checkupsDao.getAllCheckedCheckups();
	}

	public List<Checkup> getCheckupsByUsername(String username) {
		return checkupsDao.getCheckupsByUsername(username);
	}

	public Checkup getCheckupByReportId(int idToUse) {
		return checkupsDao.getCheckupByReportId(idToUse);
	}

	public ArrayList<String> getVolunteersNames() {
		return checkupsDao.getVolunteersNames();
	}

	public ArrayList<Checkup> getAllCheckedCheckupsByCouncil(String council) {
		return checkupsDao.getAllCheckedCheckupsByCouncil(council);
	}
}
