var app = angular.module("myApp", [ 'uiGmapgoogle-maps', 'jcs-autoValidate',
		'angular-ladda' ]);

app.run(function(defaultErrorMessageResolver) {
	defaultErrorMessageResolver
		.getErrorMessages()
		.then(function(errorMessages) {
			errorMessages['tooYoung'] = 'You must be at least {0} years old to use this site';
			errorMessages['tooOld'] = 'You must be max {0} years old to use this site';
			errorMessages['badUsername'] = 'Username can only contain numbers and letters and _';
	});
});

app.controller('MinMaxCtrl', function($scope, $log, $timeout, $http) {
	$scope.formModel = {};
	$scope.submitting = false;
	$scope.submitted = false;
	$scope.has_error = false;
	$scope.coords = {};
	
	$scope.coords = {
		latitude : 53.4616379,
		longitude : -6.242749799999956
	};
	
	$scope.testMe = function() {
		console.log("Address Field: " + $scope.formModel.address1);
		console.log("City Field: " + $scope.formModel.city);
		console.log("County Field: " + $scope.formModel.county);
		geocodeAddressToLatLng($scope.formModel.address1+", "+$scope.formModel.city+", "+$scope.formModel.county)
		console.log($scope.coords);
	}
	
	function showPosition(position) {
		$scope.geocodeAddress(position.coords.latitude, position.coords.longitude);
		$scope.coords.latitude = position.coords.latitude;
		$scope.coords.longitude = position.coords.longitude;
	}
	
	var date = new Date();
	var yyyy = date.getFullYear().toString();
	var mm = (date.getMonth()+1).toString();
	var dd  = date.getDate().toString();   
	var min = date.getMinutes().toString();
	var hours = date.getHours().toString();
	if(mm<10) {
		mm = "0"+mm;
	}
	if(dd<10) {
		dd = "0"+dd;
	}
	if(min<10) {
		min = "0"+min;
	}
	if(hours<10) {
		hours = "0"+hours;
	}
	$scope.formModel.datetime = dd+"-"+mm+"-"+yyyy + " " + hours+":"+min;
	
	
	function formatAMPM(date) {
		  var hours = date.getHours();
		  var minutes = date.getMinutes();
		  var ampm = hours >= 12 ? 'PM' : 'AM';
		  hours = hours % 12;
		  hours = hours ? hours : 12; 
		  minutes = minutes < 10 ? '0'+minutes : minutes;
		  var strTime = hours + ':' + minutes + ' ' + ampm;
		  return strTime;
		}
	
	$scope.setMapValues = function() {
		$http.get('http://localhost:8080/prasbic/api/getResidentByUsername/'+$scope.username).success(function (resident) {
			$scope.coords.latitude = resident.latitude;
			$scope.coords.longitude = resident.longitude;
		});
	}
	
	$scope.map = {
		center : $scope.coords,
		zoom : 17
	}

	function showPosition(position) {
	    map.setZoom(9);
	    map.setCenter(position);
	}
	
	$scope.options = {
		scrollwheel : true,
		mapTypeId : google.maps.MapTypeId.HYBRID
	};
	$scope.coordsUpdates = 0;
	$scope.dynamicMoveCtr = 0;
	$scope.marker = {
		id : 0,
		coords : $scope.coords,
		options : {
			draggable : true
		},
		events : {
			dragend : function(marker, eventName, args) {
				var lat = marker.getPosition().lat();
				var lon = marker.getPosition().lng();

				$scope.geocodeAddress(lat, lon);

				$scope.marker.options = {
					draggable : true,
					labelContent : "",
					labelAnchor : "100 0",
					labelClass : "marker-labels"
				};
			}
		}
	};
	
	function geocodeAddressToLatLng(address) {
		var geocoder = new google.maps.Geocoder;
		geocoder.geocode({'address': address}, function(results, status) {
		    if (status === google.maps.GeocoderStatus.OK) {
		      $scope.marker.coords.latitude = results[0].geometry.location.lat();
		      $scope.marker.coords.longitude = results[0].geometry.location.lng();
		    } else {
		    	alert("You have entered an invalid address")
		    }
		  });
	}	
	
	$scope.geocodeAddress = function(latitude, longitude) {	
		var geocoder = new google.maps.Geocoder;
		var latlng = {
			lat : latitude,
			lng : longitude
		};
		geocoder.geocode({'location' : latlng}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				if (results[1]) {
					console.log(results[0].address_components)
					var houseNum = results[0].address_components[0].long_name;
					var street = results[0].address_components[1].long_name;
					var city = results[0].address_components[2].long_name;
					$scope.formModel.address1 = houseNum + " " + street;
					$scope.formModel.city = city;
					
					results[0].address_components.forEach(function(entry) {
						var isPolitical = false;
						var isAdminAreaLevel2 = false;
						entry.types.forEach(function(type) {
							if(type == "administrative_area_level_2") {
								isAdminAreaLevel2 = true;
							}
							if(type == "political") {
								isPolitical = true;
							}
						});
						if(isAdminAreaLevel2 && isPolitical) {
							console.log(entry.long_name);
							if(entry.long_name == "Limerick" || entry.long_name == "Limerick City") {
								$scope.formModel.council = "Limerick City and County Council";
							} else if(entry.long_name == "Waterford" || entry.long_name == "Waterford City") {
								$scope.formModel.council = "Waterford City and County Council";
							} else if(entry.long_name == "Cork City") {
								$scope.formModel.council = "Cork City Council";
							} else if(entry.long_name == "Dublin City") {
								$scope.formModel.council = "Dublin City Council";
							} else if(entry.long_name == "Galway City") {
								$scope.formModel.council = "Galway City Council";
							} else if(entry.long_name == "County Donegal") {
								$scope.formModel.council = "Donegal County Council";
							} else if(entry.long_name == "North Tipperary") {
								$scope.formModel.council = "Tipperary County Council";
							} else if(entry.long_name == "South Tipperary") {
								$scope.formModel.council = "Tipperary County Council";
							} else {
								$scope.formModel.council = entry.long_name + " County Council";
							}
						}
					});
					
					results[0].address_components.forEach(function(entry) {
						var isCorrectType = false;
						entry.types.forEach(function(type) {
							if(type == "administrative_area_level_1") {
								isCorrectType = true;
							}
						});
						if(isCorrectType) {
							$scope.formModel.county = entry.long_name;
						}
					});
					console.log($scope.coords);				
				} else {
					window.alert('No results found');
				}
			} else {
				window.alert('Geocoder failed due to: ' + status);
			}
		});
	};	

	$scope.onSubmit = function() {
		$scope.formModel.username = $scope.username;
		var reportObjToSend = {};
		reportObjToSend.datetime = $scope.formModel.datetime;
		reportObjToSend.address1 = $scope.formModel.address1;
		reportObjToSend.city = $scope.formModel.city;
		reportObjToSend.county = $scope.formModel.county;
		reportObjToSend.category = $scope.formModel.category.value;
		reportObjToSend.description = $scope.formModel.description;
		reportObjToSend.latitude = $scope.coords.latitude;
		reportObjToSend.longitude = $scope.coords.longitude;
		reportObjToSend.username = $scope.formModel.username;
		reportObjToSend.council = $scope.formModel.council;
		console.log(reportObjToSend);
		alert("The address is... " + reportObjToSend.datetime);
		$http.post('http://localhost:8080/prasbic/api/create_report2', JSON.stringify(reportObjToSend))
        .success(function (data, status, headers, config) {
       	 	console.log("Success");
       	 	console.log(status);
       	 	if(status == 200) {
       	 		window.location = "http://localhost:8080/prasbic/residentsreports";
       	 	}
           })
           .error(function (error) {
           	alert("An error has occurred")
           });
	};
	
   	$scope.categories =
   		[{id : 1,value : "Theft"}, 
   		 {id : 2,value : "Burglary"},
   		 {id : 3,value : "Fighting"}, 
   		 {id : 4,value : "Alcohol Abuse"}, 
   		 {id : 5,value : "Drug Abuse"},
   		 {id : 6,value : "Vandalism"}, 
   		 {id : 7,value : "Joy Riding"}, 
   		 {id : 8,value : "Other"} ];	
});