package com.finalyearproject.spring.web.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalyearproject.spring.web.dao.Report;
import com.finalyearproject.spring.web.dao.ReportsDao;

@Service("reportsService")
public class ReportsService {
	
	private ReportsDao reportsDao;
	
	@Autowired
	public void setReportsDao(ReportsDao reportsDao) {
		this.reportsDao = reportsDao;
	}

	public List<Report> getCurrent() {
		return reportsDao.getReports();
	}

	public void saveOrUpdate(Report report) {
		reportsDao.saveOrUpdate(report);
	}

	public List<Report> getReportsByUsername(String username) {
		return reportsDao.getReportsByUsername(username);
	}

	public List<Report> getReportsFromPast24Hours() {
		return reportsDao.getReportsFromPast24Hours();
	}

	public List<Report> getReportsFromPastWeek() {
		return reportsDao.getReportsFromPastWeek();
	}

	public List<Report> getReportsFromPastMonth() {
		return reportsDao.getReportsFromPastMonth();
	}

	public long getNumOfJanReports(int monthNum, int year) {
		return reportsDao.getNumOfJanReports(monthNum, year);
	}

	public ArrayList<String> getResidentsNames() {
		return reportsDao.getResidentsNames();
	}

	public ArrayList<Report> getAllReportsByCouncil(String councilname) {
		return reportsDao.getAllReportsByCouncil(councilname);
	}

	public ArrayList<Report> getBusiestHoursWithCouncil(String councilname) {
		return reportsDao.getBusiestHoursWithCouncil(councilname);
	}

	public ArrayList<Report> getBusiestHours() {
		return reportsDao.getBusiestHours();

	}

	public Map<String, Long> getBusiestCouncils() {
		return reportsDao.getBusiestCouncils();
	}
	
}
