<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!--<link
	href="${pageContext.request.contextPath}/static/css/bootstrap.min.css"
	rel="stylesheet">
	<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/script/jquery.js"></script> -->

<link
	href="${pageContext.request.contextPath}/static/css/jumbotron/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/static/css/jumbotron/jumbotron.css"
	rel="stylesheet">
<link rel="icon" href="${pageContext.request.contextPath}/static/css/jumbotron/favicon.ico">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/script/jumbotron/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/static/script/jumbotron/bootstrap.min.js"></script>
<title><tiles:insertAttribute name="title"></tiles:insertAttribute></title>
<tiles:insertAttribute name="includes"></tiles:insertAttribute>

<style>
.footer {
	position: fixed;
	bottom: 0;
}
</style>

</head>
<body>
	<div class="header">
		<tiles:insertAttribute name="header"></tiles:insertAttribute>
	</div>

	<div class="content">
		<tiles:insertAttribute name="content"></tiles:insertAttribute>
	</div>

	<div class="footer">
		<tiles:insertAttribute name="footer"></tiles:insertAttribute>
	</div>
</body>
</html>