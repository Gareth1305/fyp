package com.finalyearproject.spring.web.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("residentsDao")
public class ResidentsDao {
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public void create(Resident resident) {
		resident.setPassword(passwordEncoder.encode(resident.getPassword()));
		session().save(resident);
	}
	
	public boolean exists(String username) {
		Criteria crit = session().createCriteria(Resident.class);
		crit.add(Restrictions.idEq(username));
		Resident resident = (Resident) crit.uniqueResult();
		return resident != null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Resident> getAllResidents() {
		return session().createQuery("from Resident").list();
	}
	
	public Resident getResidentByUsername(String username) {
		Criteria crit = session().createCriteria(Resident.class);
		crit.add(Restrictions.idEq(username));
		Resident resident = (Resident) crit.uniqueResult();
		return resident;		
	}

	@SuppressWarnings("unchecked")
	public List<Resident> getElderlyAndDisabledResidents() {
		Date dateInstance = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateInstance);
		cal.add(Calendar.YEAR, -70);
		Date dateMinus70 = cal.getTime();
	        
		Criteria crit = session().createCriteria(Resident.class);
		Criterion rest1 = Restrictions.lt("dob", dateMinus70); 
		Criterion rest2 = Restrictions.eq("disability", true); 
		
		crit.add(Restrictions.or(rest1, rest2));
		List<Resident> residents = crit.list();
		return residents;	
	}

	@SuppressWarnings("unchecked")
	public List<Resident> getResidentsByCouncilName(String councilname) {
		Criteria crit = session().createCriteria(Resident.class);
		crit.add(Restrictions.eq("council.councilname", councilname));
		return crit.list();
	}

	public boolean doesUsernameExist(String username) {
		Criteria crit = session().createCriteria(User.class);
		crit.add(Restrictions.idEq(username));
		return crit.uniqueResult() != null;
	}
}
