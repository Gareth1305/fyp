<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(document).ready(function() {
		document.f.j_username.focus();
	});
</script>
<link
	href="${pageContext.request.contextPath}/static/css/login/login.css"
	rel="stylesheet" type="text/css">
	
<h3>Login with Username and Password</h3>

<c:if test="${param.error != null}">
	<p class="loginerror">Login Failed: Check that your username and
		password are correct!</p>
</c:if>

<div class="wrapper">
	<div class="logInContainer">
		<form class="form-signin" name='f'
				action='${pageContext.request.contextPath}/j_spring_security_check'
				method='POST'>
			<h2 class="form-signin-heading">Please login</h2>
			<div class="input-group">
				<span class="input-group-addon"><i
					class="glyphicon glyphicon-lock"></i></span> 
					<input id="username"
					type="text" class="form-control" name='j_username' required=""
					autofocus="" value="" placeholder="Username">
			</div>
			<div class="input-group">
				<span class="input-group-addon"><i
					class="glyphicon glyphicon-user"></i></span> 
					<input id="password"
					type="text" class="form-control" name='j_password' required=""
					value="" placeholder="Password">
			</div>
			<div class="checkDiv">
				<label class="checkbox"> 
				<input type="checkbox"
					value="remember-me" id="rememberMe" name='_spring_security_remember_me' checked="checked">
					Remember me
				</label>
			</div>
			<button class="btn btn-lg btn-success btn-block" type="submit">Login</button>
		</form>
	</div>
</div>