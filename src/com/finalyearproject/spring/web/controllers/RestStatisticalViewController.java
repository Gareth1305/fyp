package com.finalyearproject.spring.web.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finalyearproject.spring.web.dao.Council;
import com.finalyearproject.spring.web.dao.Report;
import com.finalyearproject.spring.web.service.CouncilsService;
import com.finalyearproject.spring.web.service.ReportsService;

@Controller
@RequestMapping("/api")
public class RestStatisticalViewController {
	
	private ReportsService reportsService;

	@Autowired
	public void setReportsService(ReportsService reportsService) {
		this.reportsService = reportsService;
	}
	
	private CouncilsService councilsService;

	@Autowired
	public void setCouncilsService(CouncilsService councilsService) {
		this.councilsService = councilsService;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getAllReports2", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Report> getAllReports() {
		ArrayList<Report> reportsArray = (ArrayList<Report>) reportsService.getCurrent();
		Collections.sort(reportsArray);
		return reportsArray;
	}
	
	@RequestMapping(value = "/getAllCouncils", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Council> getAllCouncils() {
		return councilsService.getAllCouncils();
	}
	
	@RequestMapping(value = "/getNumOfJanReports/{monthNum}/{year}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public long getNumOfReports(@PathVariable int monthNum, @PathVariable int year) {
		System.out.println(monthNum);
		System.out.println(year);
		long numOfJanReports = reportsService.getNumOfJanReports(monthNum, year);
		return numOfJanReports;
	}
	
}