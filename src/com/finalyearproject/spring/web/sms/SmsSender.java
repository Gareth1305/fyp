package com.finalyearproject.spring.web.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

public class SmsSender {
	
	public void sendSMS(String phoneNum, String firstName) {
		System.out.println("Phone Num: " + phoneNum);
		System.out.println("Message: " + phoneNum);
		try {
			String formattedNumber = "353" + phoneNum.substring(1);
			System.out.println(formattedNumber);
			
            URL url = new URL("https://bulksms.vsms.net/eapi/submission/send_sms/2/2.0?"
            		+ "username=Gareth5031&password=Gareth1994&message=Attention+"+firstName+"!!+A+burglary"
            		+ "+has+been+reported+in+your+area.+Stay+alert+and+be+aware!!&msisdn="+formattedNumber);
            
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                System.out.println(line);
            }
            wr.close();
            rd.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
}
