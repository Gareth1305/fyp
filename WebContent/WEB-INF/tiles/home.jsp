<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>

<link
	href="${pageContext.request.contextPath}/static/css/home/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/static/css/home/clean-blog.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/static/css/home/home.css"
	rel="stylesheet">
<script async src="//jsfiddle.net/zjv5ob21/9/embed/"></script>
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css'>


<script
	src="${pageContext.request.contextPath}/static/script/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/home/home.js"></script>
<script type="text/javascript"
	src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript"
	src="http://www.amcharts.com/lib/3/serial.js"></script>
<script>
	function getURLParameter(name) {
		return decodeURIComponent((new RegExp('[?|&]' + name + '='
				+ '([^&;]+?)(&|#|;|$)').exec(location.search) || [ , "" ])[1]
				.replace(/\+/g, '%20'))
				|| null;
	}
	function callMe() {
		myvar = getURLParameter('error');
		if (myvar != null) {
			document.getElementById('formCtrlID').classList.add('loginError');
			document.getElementById('formCtrlID2').classList.add('loginError');
		}
	}
	window.onload = callMe;
	function viewReportsClicked() {
		console.log("Clicked")
	}
</script>

<div class="test">
	<header class="intro-header"
		style="background-image: url('${pageContext.request.contextPath}/static/images/liffey-bg.jpg')">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
					<div class="page-heading">
						<h1>PRASBIC</h1>
						<hr class="small">
						<br> <span class="mysubheading subheading"><u><b>PR</b></u>eventing
							<u><b>A</b></u>nti <u><b>S</b></u>ocial <u><b>B</b></u>ehaviour <u><b>I</b></u>n
							<u><b>C</b></u>ommunities</span>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div class="row">
		<!-- /.col-md-4 -->
		<div style="text-align: center;" class="col-md-4">
			<sec:authorize access="!isAuthenticated()">
				<h2>View Reports For Your Area</h2>
				<img style="width: 90%; height: 200px;" alt=""
					src="${pageContext.request.contextPath}/static/images/viewreports.PNG" />
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_VOLUNTEER')">
				<h2>View Reports For Your Area</h2>
				<img style="width: 90%; height: 200px;" alt=""
					src="${pageContext.request.contextPath}/static/images/viewreports.PNG">
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_USER')">
				<h2>View Reports For Your Area</h2>
				<img style="width: 90%; height: 200px;" alt=""
					src="${pageContext.request.contextPath}/static/images/viewreports.PNG" />
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<h2>View Overall Report Information</h2>
				<img style="width: 90%; height: 200px;" alt=""
					src="${pageContext.request.contextPath}/static/images/viewreports.PNG" />
			</sec:authorize>
		</div>
		<!-- /.col-md-4 -->
		<div style="text-align: center;" class="col-md-4">
			<sec:authorize access="!isAuthenticated()">
				<h2>Create Reports</h2>
				<p class="homePagePara">Create reports of anti-social behaviour
					that you witness.</p>
				<p class="homePagePara">If you are elderly or disabled, a
					volunteer will check on you to ensure you are safe.</p>
				<p class="homePagePara">Contribute to the gathering of
					statistics in attempt to improve facilities in areas blighted with
					anti-social behaviour.</p>
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_ADMIN')">
				<h2>Your Responsibilities</h2>
				<p class="homePagePara">Analyse statistical information to
					identify irregularities.</p>
				<p class="homePagePara">Export charts to PDF and email to
					specific councils.</p>
				<p class="homePagePara">Create more administrators to ensure
					there are no down-time for the site.</p>
				<p class="homePagePara">Monitor Facebook and Twitter accounts.
					Ensure these sites regularly post information.</p>
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_USER')">
				<h2>Responsibilities/Functionality</h2>
				<p class="homePagePara">Identify and report incidents of
					anti-social behaviour. No matter how small, we want to hear about
					it.</p>
				<p class="homePagePara">View all reports for your area,
					introduce date ranges, radius around your location, address etc.</p>
				<p class="homePagePara">Contribute to up keep of our statistical
					information. This will be passed on to council members in order to
					improve facilities in your area.</p>
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_VOLUNTEER')">
				<h2>Responsibilities & Functionality</h2>
				<p class="homePagePara">View statistical for your area.</p>
				<p class="homePagePara">View reports that have occured within
					the last 24 hours in your area.</p>
				<p class="homePagePara">Ensure the safety of residents within
					your area. This involves meeting/calling elderly/disabled users
					once a report has been made by them.</p>
			</sec:authorize>
		</div>
		<!-- /.col-md-4 -->
		<div style="text-align: center;" class="col-md-4">
			<sec:authorize access="!hasRole('ROLE_USER')">
				<h2>View Area Statistics</h2>
				<img style="width: 90%; height: 200px;" alt=""
					src="${pageContext.request.contextPath}/static/images/stats.PNG" />
			</sec:authorize>
			<sec:authorize access="hasRole('ROLE_USER')">
				<h2>Encourage Others To.....</h2>
				<img style="width: 90%; height: 200px; border: 1px solid black"
					alt=""
					src="${pageContext.request.contextPath}/static/images/getInvolved.jpg" />
			</sec:authorize>
		</div>
		<!-- /.col-md-4 -->
	</div>

	<hr>

	<!-- Footer -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
					<ul class="list-inline text-center">
						<li><a href="https://twitter.com/Prasbic" target="_blank">
								<span class="fa-stack fa-lg"> <i
									class="fa fa-circle fa-stack-2x"></i> <i
									class="fa fa-twitter fa-stack-1x fa-inverse"></i>
							</span>
						</a></li>
						<sec:authorize access="hasRole('ROLE_ADMIN')">
							<li><a href="https://bitbucket.org/Gareth1305/fyp/src"
								target="_blank"> <span class="fa-stack fa-lg"> <i
										class="fa fa-circle fa-stack-2x"></i> <i
										class="fa fa-bitbucket fa-stack-1x fa-inverse"></i>
								</span>
							</a></li>
						</sec:authorize>
						<li><a href="https://www.facebook.com/prasbic/"
							target="_blank"> <span class="fa-stack fa-lg"> <i
									class="fa fa-circle fa-stack-2x"></i> <i
									class="fa fa-facebook fa-stack-1x fa-inverse"></i>
							</span>
						</a></li>
					</ul>
					<p class="copyright text-muted">Gareth McGrath &copy;
						www.prasbic.com</p>
				</div>
			</div>
		</div>
	</footer>
</div>