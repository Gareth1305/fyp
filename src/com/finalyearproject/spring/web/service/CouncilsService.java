package com.finalyearproject.spring.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.finalyearproject.spring.web.dao.Council;
import com.finalyearproject.spring.web.dao.CouncilsDao;

@Service("councilsService")
public class CouncilsService {

	private CouncilsDao councilsDao;

	@Autowired
	public void setCouncilsDao(CouncilsDao councilsDao) {
		this.councilsDao = councilsDao;
	}
	
	public void saveOrUpdate(Council council) {
		councilsDao.saveOrUpdate(council);
	}
	
	public List<Council> getAllCouncils() {
		return councilsDao.getAllCouncils();
	}
	
	public Council getCouncilByCouncilName(String councilname) {
		Council council = councilsDao.getCouncilByCouncilName(councilname);
		return council;
	}
	
}
