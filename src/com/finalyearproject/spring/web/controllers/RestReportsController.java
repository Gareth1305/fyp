package com.finalyearproject.spring.web.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finalyearproject.spring.web.dao.Checkup;
import com.finalyearproject.spring.web.dao.Council;
import com.finalyearproject.spring.web.dao.Report;
import com.finalyearproject.spring.web.dao.Resident;
import com.finalyearproject.spring.web.service.CheckupsService;
import com.finalyearproject.spring.web.service.CouncilsService;
import com.finalyearproject.spring.web.service.ReportsService;
import com.finalyearproject.spring.web.service.ResidentsService;
import com.finalyearproject.spring.web.service.UsersService;
import com.finalyearproject.spring.web.sms.SmsSender;

@Controller
@RequestMapping("/api")
public class RestReportsController {

	private ResidentsService residentsService;

	@Autowired
	public void setResidentsService(ResidentsService residentsService) {
		this.residentsService = residentsService;
	}

	private CheckupsService checkupsService;

	@Autowired
	public void setCheckupsService(CheckupsService checkupsService) {
		this.checkupsService = checkupsService;
	}

	private ReportsService reportsService;

	@Autowired
	public void setReportsService(ReportsService reportsService) {
		this.reportsService = reportsService;
	}

	@SuppressWarnings("unused")
	private UsersService usersService;

	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}

	private CouncilsService councilsService;

	@Autowired
	public void setCouncilsService(CouncilsService councilsService) {
		this.councilsService = councilsService;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/getAllReports", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Report> getAllReports() {
		ArrayList<Report> reportsArray = (ArrayList<Report>) reportsService.getCurrent();
		Collections.sort(reportsArray);
		return reportsArray;
	}

	@RequestMapping(value = "/getBusiestCouncils", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Map<String, Long> getBusiestCouncils() {
		Map<String, Long> reportsMap = (Map<String, Long>) reportsService.getBusiestCouncils();
		return reportsMap;
	}

	@RequestMapping(value = "/getAllResidentsFromReports", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<String> getAllResidentsFromReports() {
		ArrayList<String> residentsNames = (ArrayList<String>) reportsService.getResidentsNames();
		return residentsNames;
	}

	@RequestMapping(value = "/getReportsByUsername/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Report> getReportsByUsername(@PathVariable String username) {
		ArrayList<Report> reportsArray = (ArrayList<Report>) reportsService.getReportsByUsername(username);
		return reportsArray;
	}

	@RequestMapping(value = "/getBusiestHoursWithCouncil/{councilname}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Report> getBusiestHoursWithCouncil(@PathVariable String councilname) {
		System.out.println(councilname);
		ArrayList<Report> reportsArray = (ArrayList<Report>) reportsService.getBusiestHoursWithCouncil(councilname);
		return reportsArray;
	}

	@RequestMapping(value = "/getBusiestHours", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Report> getBusiestHours() {
		ArrayList<Report> reportsArray = (ArrayList<Report>) reportsService.getBusiestHours();
		return reportsArray;
	}

	@RequestMapping(value = "/getAllReportsByCouncil/{councilname}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Report> getAllReportsByCouncil(@PathVariable String councilname) {
		ArrayList<Report> reportsArray = (ArrayList<Report>) reportsService.getAllReportsByCouncil(councilname);
		return reportsArray;
	}

	@RequestMapping(value = "/getReportsFromLast24Hours", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Report> getReportsFromPast24Hours() {
		return reportsService.getReportsFromPast24Hours();
	}

	@RequestMapping(value = "/getReportsFromPastWeek", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Report> getReportsFromPastWeek() {
		return reportsService.getReportsFromPastWeek();
	}

	@RequestMapping(value = "/getReportsFromPastMonth", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Report> getReportsFromPastMonth() {
		return reportsService.getReportsFromPastMonth();
	}

	@RequestMapping(value = "/create_report2", method = RequestMethod.POST)
	@ResponseBody
	public void registerUser(@RequestBody String reportInJson) {
		System.out.println(reportInJson);
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(reportInJson, new TypeReference<HashMap<String, String>>() {
			});
			createReportForUser2(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean createReportForUser2(Map<String, String> map) {
		String datetime = (String) map.get("datetime");
		System.out.println(datetime);
		String newDate = datetime.replaceAll("-", "/");
		String address1 = (String) map.get("address1");
		String city = (String) map.get("city");
		String county = (String) map.get("county");
		String latitudeToConvert = (String) map.get("latitude");
		double latitude = Double.parseDouble(latitudeToConvert);
		String longitudeToConvert = (String) map.get("longitude");
		double longitude = Double.parseDouble(longitudeToConvert);
		String category = (String) map.get("category");
		String description = (String) map.get("description");
		String username = (String) map.get("username");
		String councilname = (String) map.get("council");
		SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		try {
			Date date = ft.parse(newDate);
			Resident resident = residentsService.getResidentByUsername(username);
			if (resident != null) {
				Council council = councilsService.getCouncilByCouncilName(councilname);
				Date dateInstance = new Date();
				Calendar cal = Calendar.getInstance();
				cal.setTime(dateInstance);
				cal.add(Calendar.YEAR, -70);
				Date dateMinus70 = cal.getTime();
				Report report = new Report(resident, council, date, address1, city, county, latitude, longitude,
						category, description);
				reportsService.saveOrUpdate(report);
				if (dateMinus70.after(resident.getDob()) || (resident.isDisability() == true)) {
					Checkup checkup = new Checkup(false, report, resident);
					checkupsService.saveOrUpdate(checkup);
				}
				
				if(category.equalsIgnoreCase("Burglary")) {
					List<Resident> residents = residentsService.getResidentsByCouncilName(councilname);
					for(Resident res: residents) {
						if(distFrom(res.getLatitude(), res.getLongitude(), latitude, longitude) < 350) {
							//TODO - Remove the break statement in order to send texts to all users
							SmsSender sender = new SmsSender();
							sender.sendSMS(res.getPhone(), res.getFirstname());
							break;
						}
					}
				}
				return true;
			}
		} catch (ParseException p) {
			p.printStackTrace();
		}
		
		return false;
	}

	@RequestMapping(value = "/create_report_from_android", method = RequestMethod.POST)
	@ResponseBody
	public String createReportFromAndroid(@RequestBody String reportInJson) {
		System.out.println(reportInJson);
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(reportInJson, new TypeReference<HashMap<String, String>>() {
			});
			createReportForUser2(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		createReportFromAndroid(map);
		return "SUCCESS";
	}

	private void createReportFromAndroid(Map<String, String> map) {
		String time = (String) map.get("datetime");
		String date = (String) map.get("date");
		String address = (String) map.get("address");
		String city = (String) map.get("city");
		String county = (String) map.get("county");
		String category = (String) map.get("category");
		String description = (String) map.get("description");
		String username = (String) map.get("username");
		String latitudeToConvert = (String) map.get("latitude");
		double latitude = Double.parseDouble(latitudeToConvert);
		String longitudeToConvert = (String) map.get("longitude");
		double longitude = Double.parseDouble(longitudeToConvert);
		String councilname = (String) map.get("council");
		if (councilname.equals("")) {
			System.out.println("Council name is empty.... ");
		}
		System.out.println("address " + address);
		System.out.println("category " + category);
		System.out.println("username " + username);
		System.out.println("latitude " + latitude);
		System.out.println("councilname " + councilname);
		Resident resident = residentsService.getResidentByUsername(username);
		SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		try {
			Date date2 = ft.parse(date + " " + time);
			if (resident != null) {
				if (councilname != null || !councilname.equals("")) {
					Council council = councilsService.getCouncilByCouncilName(councilname);
					Date dateInstance = new Date();
					Calendar cal = Calendar.getInstance();
					cal.setTime(dateInstance);
					cal.add(Calendar.YEAR, -70);
					Date dateMinus70 = cal.getTime();
					Report report = new Report(resident, council, date2, address, city, county, latitude, longitude,
							category, description);
					// reportsService.saveOrUpdate(report);
					if (dateMinus70.after(resident.getDob()) || (resident.isDisability() == true)) {
						Checkup checkup = new Checkup(false, report, resident);
						// checkupsService.saveOrUpdate(checkup);
					}
				}
			}
		} catch (ParseException p) {
			p.printStackTrace();

		}
	}

	public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
		double earthRadius = 6371000; // miles (or 6371.0 kilometers)
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double a = Math.pow(sindLat, 2)
				+ Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;

		return dist;
	}

}
