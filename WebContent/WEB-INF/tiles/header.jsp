<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
	
<head>
	<link
	href="${pageContext.request.contextPath}/static/css/header/header.css"
	rel="stylesheet">
</head>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container header-container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="active navbar-brand" href="${pageContext.request.contextPath}/">Home</a>
		  <sec:authorize access="hasRole('ROLE_USER')">
		  	<a class="navbar-brand" href="${pageContext.request.contextPath}/createreport">Create Report</a>
		  </sec:authorize>
		  <sec:authorize access="hasRole('ROLE_VOLUNTEER')">
		  		<a class="navbar-brand" href="${pageContext.request.contextPath}/reports">View Reports</a>
  		  </sec:authorize>
  		  <sec:authorize access="hasRole('ROLE_USER')">
		  		<a class="navbar-brand" href="${pageContext.request.contextPath}/residentsreports">View Reports</a>
  		  </sec:authorize>
		  <sec:authorize access="hasRole('ROLE_ADMIN')">
		  	<a class="navbar-brand" href="${pageContext.request.contextPath}/statistics">Statistical Information</a>
		  </sec:authorize>
		  <sec:authorize access="hasRole('ROLE_ADMIN')">
		  	<a class="navbar-brand" href="${pageContext.request.contextPath}/viewallreports">View All Reports</a>
		  </sec:authorize>
		  <sec:authorize access="hasRole('ROLE_ADMIN')">
		  	<a class="navbar-brand" href="${pageContext.request.contextPath}/newadminaccount">Create Admin</a>
		  </sec:authorize>
		  <sec:authorize access="!isAuthenticated()">
		  	<a class="navbar-brand" href="${pageContext.request.contextPath}/newresidentaccount">Create Resident Account</a>
		  </sec:authorize>
		  <sec:authorize access="!isAuthenticated()">
		  	<a class="navbar-brand" href="${pageContext.request.contextPath}/newvolunteeraccount">Volunteer For Your Area</a>
		  </sec:authorize>
		  <sec:authorize access="hasRole('ROLE_VOLUNTEER')">
		  	<a class="navbar-brand" href="${pageContext.request.contextPath}/statisticalviewvolres">View Statistics</a>
		  </sec:authorize>		  
        </div>

        <div id="navbar" class="navbar-collapse collapse">
          <form name='f' 
          		class="navbar-form navbar-right"
          		action='${pageContext.request.contextPath}/j_spring_security_check'
				method='POST'>
            <sec:authorize access="!isAuthenticated()">
	            <div class="form-group">
	            <!--checked="checked"-->
	              <label class="remMe">Remember Me</label>
	              <input type="checkbox" name='_spring_security_remember_me' class="boxToCheck">
	            </div>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
	            <div class="form-group">
	              <input id="formCtrlID" type="text" name='j_username' value='' placeholder="Username" class="form-control">
	            </div>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
	            <div class="form-group">
	              <input id="formCtrlID2" type='password' name='j_password'  placeholder="Password" class="form-control">
	            </div>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
            	<button type="submit" name="submit" class="btn btn-success">Log In</button>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
            	<a class="btn btn-success" href="<c:url value='/j_spring_security_logout'/>">Log Out</a>
            </sec:authorize>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

<!--<nav id="navbar-black" class="navbar navbar-static-top navbar-dark bg-inverse">
    <a class="navbar-brand" href="${pageContext.request.contextPath}/">PRASBIC</a>
    <ul class="nav navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="${pageContext.request.contextPath}/">Home<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
             <sec:authorize access="isAuthenticated()"><a class="nav-link" href="${pageContext.request.contextPath}/createreport">Create-Report</a></sec:authorize>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/reports">View-Reports</a>
        </li>
        <li class="nav-item">
            <sec:authorize access="hasRole('ROLE_ADMIN')"><a class="nav-link login pull-right" href="<c:url value='/newadminaccount'/>">Create Admin</a></sec:authorize>
        </li>
        <li class="nav-item">
            <sec:authorize access="!isAuthenticated()"><a class="nav-link login pull-right" href="<c:url value='/login'/>">Login</a></sec:authorize>
        </li>
        <li class="nav-item">
            <sec:authorize access="isAuthenticated()"><a class="nav-link pull-right" href="<c:url value='/j_spring_security_logout'/>">Logout</a></sec:authorize>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
    	<li><p class="nav-item">Already have an account?</p></li>
    </ul>
</nav>-->
