<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<head>
<script
	src="${pageContext.request.contextPath}/static/script/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/residentsreports/residentsreports.js"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9z1qJLw1fTEoHn1_JxuggptNZFLOz56o"></script>
<link href="${pageContext.request.contextPath}/static/css/residentreports/residentsreports.css" rel="stylesheet" media="screen"/> 

</head>
<sec:authentication property="principal" var="principal" />
<div class="div_container" ng-app="resReports"
	ng-controller="ResidentReportsCtrl" ng-model="username"
	ng-init="username='${principal.username}'">
	<div ng-init="setLoggedInUser()"></div>
	<div class="map_and_form_container">
		<div class="map_container">
			<div id="map"></div>
		</div>
		<div class="form_container">
			<div class="report_form">
				<form name="reportForm" novalidate="novalidate" role="form">
					<div>
						<label for="address">Address:</label>
					</div>
					<div class="input-group form_field">
						<input type="text" class="form-control"
							ng-model="formModel.addressField" placeholder="Enter Address"
							id="address" ng-blur="callMe(formModel.addressField)"
							required="required"> <span class="input-group-btn">
							<button ng-click="updateAdress()" class="btn btn-default"
								type="button">Use Current Location</button>
						</span>
					</div>
					<div class="form-group form_field">
						<label for="radius">Radius: {{formModel.radius}}</label> <input
							type="range" id="rangeInput" min="0" max="500"
							ng-init="formModel.radius = 250" ng-model="formModel.radius"
							ng-change="clickMe(formModel.radius)" value="100" step="25" />
					</div>

					<div class="form-group">
						<label for="datetime" class="control-label">Time/Date</label> <select
							id="datetime" class="form-control" ng-model="formModel.datetime"
							value="Choose Category"
							ng-options="c.value for c in dateCategories"
							ng-change="time_date_changed()" required>
						</select>
					</div>

					<div class="form-group">
						<div class="align-btn">
							<button class="customized_bubtton btn btn-warning"
								ng-click="clearFormFields()">Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="report_container">
		<div class="live_feed_title">
			<h3>
				<u>Report Information</u>
			</h3>
		</div>
		<div class="reports_feed">
			<div class="individual_report"
				ng-repeat="report in reportInformation | orderBy:'-date'"
				ng-class="{'selected':$index == selectedRow}"
				ng-click="setClickedRow($index, report)">
				<div class="category_date_container">
					<div class="category_container">
						<b> Category:</b> {{report.category}}
					</div>
					<div class="date_container">{{report.date | date:'dd MMM
						yyyy'}}</div>
				</div>
				<br />
				<div class="info_container description_container">
					<b>Description:</b> {{report.description}}
				</div>
				<div class="info_container description_container">
					<b>Address:</b> {{report.address1}}, {{report.city}}
				</div>
				<hr />
			</div>
		</div>
	</div>
</div>

<script
	src="${pageContext.request.contextPath}/static/script/createreport/jcs-auto-validate.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/spin.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/ladda.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/angular-ladda.min.js"></script>