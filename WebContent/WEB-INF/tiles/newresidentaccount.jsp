<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<head>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9z1qJLw1fTEoHn1_JxuggptNZFLOz56o&callback=initMap"></script>
<link
	href="${pageContext.request.contextPath}/static/css/createreport/bootstrap.min.css"
	rel="stylesheet">
<script
	src="${pageContext.request.contextPath}/static/script/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createresident/createresident.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/lodash.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/moment-with-locales.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/bootstrap-datetimepicker.js"></script>
<link
	href="${pageContext.request.contextPath}/static/css/createreport/ladda-themeless.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/static/css/createreport/bootstrap-datetimepicker.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/static/css/createresident/createresident.css"
	rel="stylesheet">
</head>
<div ng-app="prasbicApp" class="divContainer"
	ng-controller="CreateResidentCtrl">
	<div class="innerDiv">
		<h1 class="titleH">Create Resident Account</h1>
		<form name="myForm" ng-submit="onSubmit()" class="resForm" novalidate="novalidate"
			ng-hide="submitted" ng-init="setCouncils()">
			<div class="split-form-container">
				<div class="first-split-form">
					<div class="form-group form-form-group">
						<label for="username" class="control-label">Username</label> <input
							type="text" class="form-control form-form-control" ng-blur="checkUsername(formModel.username)"
							ng-model="formModel.username" id="username">
					</div>
					<div class="form-group form-form-group">
						<label for="email" class="control-label">Email</label> <input
							type="email" class="form-control form-form-control"
							ng-model="formModel.email" id="email" required="required">
					</div>
					<div class="form-group form-form-group">
						<label for="password" class="control-label">Password</label> <input
							type="password" class="form-control form-form-control" ng-blur="checkPasswordMatch()"
							ng-model="formModel.password" id="password" required="required">
					</div>
					<div class="form-group form-form-group">
						<label for="confirmpass" class="control-label">Confirm
							Password</label> <input type="password"
							class="form-control form-form-control confirmPassInput" ng-blur="checkPasswordMatch()"
							ng-model="formModel.confirmpass" id="confirmpass"
							required="required">
					</div>
					<div class="form-group form-form-group">
						<label for="firstname" class="control-label">First Name</label> <input
							type="text" class="form-control form-form-control"
							ng-model="formModel.firstname" id="firstname" required="required">
					</div>
					<div class="form-group form-form-group">
						<label for="lastname" class="control-label">Last Name</label> <input
							type="text" class="form-control form-form-control"
							ng-model="formModel.lastname" id="lastname" required="required">
					</div>
					<div class="form-group form-form-group">
						<label for="dob" class="control-label">DOB</label> <input
							type="date" format="dd/MM/yyyy"
							class="form-control form-form-control" ng-model="formModel.dob"
							max="1998-04-21" ng-blur="returnDate()"
							id="dob" required="required">
					</div>
				</div>
				<div class="second-split-form">
					<div class="form-group form-form-group">
						<label for="address" class="control-label">Address</label> <input
							type="text" class="form-control form-form-control"
							ng-model="formModel.address" id="address" required="required">
					</div>
					<div class="form-group form-form-group">
						<label for="city" class="control-label">City</label> <input
							type="text" class="form-control form-form-control"
							ng-model="formModel.city" id="city" required="required">
					</div>
					<div class="form-group form-form-group">
						<label for="council" class="control-label">County Council</label>
						<select type="text" class="form-control form-form-control"
							ng-model="formModel.council" id="council" required="required"
							ng-options="d.value for d in councils">
							<option value="">-- Choose a Council --</option>
						</select>
					</div>
					<div class="form-group form-form-group">
						<label for="county" class="control-label">County</label> <input
							type="text" class="form-control form-form-control"
							ng-model="formModel.county" id="county" required="required">
					</div>
					<div class="form-group form-form-group">
						<label for="phone" class="control-label">Phone</label> <input
							type="text" class="form-control form-form-control"
							ng-model="formModel.phone" id="phone" required="required">
					</div>
					<div class="form-group form-form-group">
						<label for="disability" class="control-label">Disability?</label>
						<select type="text" class="form-control form-form-control"
							ng-model="formModel.disability" id="disability"
							required="required" ng-options="d.value for d in options">
							<option value="">-- Choose an option --</option>
						</select>
					</div>


					<div class="emergencyInfo">
						<div class="form-group ecnDiv">
							<label for="ecn" class="control-label">Emergency Contact
								Phone</label> <input type="text" class="form-control form-form-control"
								ng-model="formModel.ecn" id="ecn" required="required">
						</div>
						<div class="form-group ecrDiv">
							<label for="ecr" class="control-label">Emergency Contact
								Relation</label> <input type="text"
								class="form-control form-form-control" ng-model="formModel.ecr"
								id="ecr" required="required">
						</div>
					</div>
				</div>
			</div>
			<div align="center" class="usernameMsg" ng-show="doesUserExist == true">Username already exists</div> 
			<div align="center" class="usernameMsg" ng-show="doPasswordMatch == false">Passwords do not match</div> 
			<div class="form-group form-form-group submit-button-div">
				<button ng-disabled="doesUserExist == true || myForm.$invalid || doPasswordMatch == false" class="btn btn-primary submit-button" type="submit">Create
					Resident Account</button>
			</div>
		</form>
	</div>
</div>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/jcs-auto-validate.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/spin.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/ladda.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/angular-ladda.min.js"></script>
