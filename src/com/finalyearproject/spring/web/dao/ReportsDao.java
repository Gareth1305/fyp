package com.finalyearproject.spring.web.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("reportsDao")
public class ReportsDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public List<Report> getReports() {
		Criteria crit = session().createCriteria(Report.class);
		crit.createAlias("user", "u").add(Restrictions.eq("u.enabled", true));
		return crit.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Report> getReports(String username) {
		Criteria crit = session().createCriteria(Report.class);
		crit.createAlias("user", "u");
		crit.add(Restrictions.eq("u.enabled", true));
		crit.add(Restrictions.eq("u.username", username));
		return crit.list();
	}

	public void saveOrUpdate(Report report) {
		session().saveOrUpdate(report);
	}

	public boolean delete(int id) {
		Query query = session().createQuery("delete from Report where id=:id");
		query.setLong("id", id);
		return query.executeUpdate() == 1;
	}

	public Report getReport(int id) {
		Criteria crit = session().createCriteria(Report.class);
		crit.createAlias("user", "u");
		crit.add(Restrictions.eq("u.enabled", true));
		crit.add(Restrictions.idEq(id));
		return (Report) crit.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Report> getReportsByUsername(String username) {
		Criteria crit = session().createCriteria(Report.class);
		crit.createAlias("user", "u");
		crit.add(Restrictions.eq("u.username", username));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Report> getReportsFromPast24Hours() {
		Date dateInstance = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateInstance);
		cal.add(Calendar.DATE, -1);
		Date dateMinusOne = cal.getTime();
		
		Criteria crit = session().createCriteria(Report.class);
		crit.add(Restrictions.gt("date", dateMinusOne));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Report> getReportsFromPastWeek() {
		Date dateInstance = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateInstance);
		cal.add(Calendar.DATE, -7);
		Date dateMinusSeven = cal.getTime();
		
		Criteria crit = session().createCriteria(Report.class);
		crit.add(Restrictions.gt("date", dateMinusSeven));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	public List<Report> getReportsFromPastMonth() {
		Date dateInstance = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateInstance);
		cal.add(Calendar.MONTH, -1);
		Date monthMinusOne = cal.getTime();
		
		Criteria crit = session().createCriteria(Report.class);
		crit.add(Restrictions.gt("date", monthMinusOne));
		return crit.list();
	}

	public long getNumOfJanReports(int monthNum, int year) {
		Query query = session().createQuery("select count(*) from Report where MONTH(date)=:month and YEAR(date)=:year");
		query.setLong("month", monthNum);
		query.setLong("year", year);
		return (long) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<String> getResidentsNames() {
		Criteria crit = session().createCriteria(Report.class);
		crit.setProjection(Projections.projectionList()
				.add(Projections.property("user.username")));
		return (ArrayList<String>) crit.list();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Report> getAllReportsByCouncil(String councilname) {
		Criteria crit = session().createCriteria(Report.class);
		crit.add(Restrictions.eq("council.councilname", councilname));
		return (ArrayList<Report>) crit.list();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Report> getBusiestHoursWithCouncil(String councilname) {
		Query query = session().createQuery
				("select dayofweek(date) from Report where council.councilname=:councilname group by dayofweek(date) order by count(date) desc limit 1");
		query.setString("councilname", councilname);
		query.setMaxResults(1);
		int busiestDay = (int) query.uniqueResult();
		String myQuery = "from Report where council.councilname='"+councilname+"' and dayofweek(date) = "+busiestDay+"";
		Query q = session().createQuery(myQuery);
		
		return (ArrayList<Report>) q.list();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Report> getBusiestHours() {
		Query query = session().createQuery
				("select dayofweek(date) from Report group by dayofweek(date) order by count(date) desc limit 1");
		query.setMaxResults(1);
		int busiestDay = (int) query.uniqueResult();
		
		String myQuery = "from Report where dayofweek(date) = "+busiestDay+"";
		Query q = session().createQuery(myQuery);
		
		return (ArrayList<Report>) q.list();
	}

	@SuppressWarnings("unchecked")
	public Map<String, Long> getBusiestCouncils() {
		Query query = session().createQuery
				("select count(*) from Report group by council.councilname order by count(*) desc limit 5");
		query.setMaxResults(5);
		ArrayList<Long> councilCounts = (ArrayList<Long>) query.list();
		Query query2= session().createQuery
				("select council.councilname from Report group by council.councilname order by count(*) desc limit 5");
		query2.setMaxResults(5);
		ArrayList<String> councilNames = (ArrayList<String>) query2.list();
		
		Map<String, Long> busyCouncilMap = new HashMap<>();

		for(int i = 0; i < 5; i++) {
			String newValue = councilNames.get(i);
			if(newValue.contains("City and County Council")) {
				newValue = newValue.replace("and County Council", "");
			} else if(newValue.contains("City Council")) {
				newValue = newValue.replace("Council", "");
			} else if(newValue.contains("Dun Laoghaire-Rathdown")) {
				newValue = newValue.replace("Dun Laoghaire-Rathdown County Council", "Dun/Rath");
			} else {
				newValue = newValue.replace("County Council", "");
			}
			busyCouncilMap.put(newValue, councilCounts.get(i));
		}
		
		return busyCouncilMap;
	}
	
}
