package com.finalyearproject.spring.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.finalyearproject.spring.web.dao.FormValidationGroup;
import com.finalyearproject.spring.web.dao.Resident;
import com.finalyearproject.spring.web.service.ResidentsService;

@Controller
public class ResidentsController {

	private ResidentsService residentsService;
	
	@Autowired
	public void setResidentsService(ResidentsService residentsService) {
		this.residentsService = residentsService;
	}
	
	@RequestMapping("/newresidentaccount")
	public String showNewAccount(Model model) {
		model.addAttribute("resident", new Resident());
		return "newresidentaccount";
	}

	@RequestMapping("/statisticalviewvolres")
	public String statisticalviewvolres() {
		return "statisticalviewvolres";
	}
	
	@RequestMapping(value = "/createresidentaccount", method = RequestMethod.POST)
	public String createResidentAccount(@Validated(FormValidationGroup.class) Resident resident, BindingResult result) {
		if (result.hasErrors()) {
			return "newresidentaccount";
		}
		
		resident.setEnabled(true);
		resident.setAuthority("ROLE_USER");
		
		if(residentsService.exists(resident.getUsername())) {
			result.rejectValue("username", "DuplicateKey.resident.username");
			return "newresidentaccount";
		}
		
		try {
			residentsService.createResident(resident);
		} catch (DataAccessException e) {
			result.rejectValue("username", "DuplicateKey.resident.username");
			return "newresidentaccount";
		}

		return "residentaccountcreated";
	}
}
