package com.finalyearproject.spring.web.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finalyearproject.spring.web.dao.Council;
import com.finalyearproject.spring.web.dao.Volunteer;
import com.finalyearproject.spring.web.service.CouncilsService;
import com.finalyearproject.spring.web.service.VolunteersService;

@Controller
@RequestMapping("/api")
public class RestVolunteersController {

	private VolunteersService volunteersService;

	@Autowired
	public void setVolunteersService(VolunteersService volunteersService) {
		this.volunteersService = volunteersService;
	}

	@RequestMapping(value = "/getVolunteerById/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Volunteer getVolunteerByUsername(@PathVariable String username) {
		return volunteersService.getVolunteerByUsername(username);
	}
	
	@RequestMapping(value = "/getAllVolunteers", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Volunteer> getAllVolunteers() {
		return volunteersService.getAllVolunteers();
	}
	
	private CouncilsService councilsService;

	@Autowired
	public void setCouncilsService(CouncilsService councilsService) {
		this.councilsService = councilsService;
	}
	
	@RequestMapping(value = "/createVolunteer", method = RequestMethod.POST)
	@ResponseBody
	public void registerVolunteer(@RequestBody String volunteerJson) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(volunteerJson, new TypeReference<HashMap<String, String>>() {
			});
			createVolunteer(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createVolunteer(Map<String, String> map) {
		String username = (String) map.get("username");
		String password = (String) map.get("password");
		String email = (String) map.get("email");
		String firstname = (String) map.get("firstname");
		String lastname = (String) map.get("lastname");
		String address = (String) map.get("address");
		String city = (String) map.get("city");
		String county = (String) map.get("county");
		String dob = (String) map.get("dob");
		String phonenumber = (String) map.get("phonenumber");
		String latitude = (String) map.get("latitude");
		String longitude = (String) map.get("longitude");
		String councilname = (String) map.get("council");
		
		double lat = Double.parseDouble(latitude);
		double lng = Double.parseDouble(longitude);
		
		SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		try {
			Date date = ft.parse(dob);
			boolean enabled = true;
			String authority = "ROLE_VOLUNTEER";
			
			Council council = councilsService.getCouncilByCouncilName(councilname);
			
			Volunteer newVolunteer = new Volunteer(username, password, email, enabled, authority, firstname,
					lastname, address, city, county, date, phonenumber, lat, lng, council);
			if(newVolunteer != null) { 
				volunteersService.createVolunteer(newVolunteer);
			}
		} catch (ParseException p) {
            p.printStackTrace();
        }
	}

}
