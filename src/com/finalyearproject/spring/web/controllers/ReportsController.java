package com.finalyearproject.spring.web.controllers;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.finalyearproject.spring.web.dao.FormValidationGroup;
import com.finalyearproject.spring.web.dao.Report;
import com.finalyearproject.spring.web.service.ReportsService;

@Controller
public class ReportsController {

	private ReportsService reportsService;

	@Autowired
	public void setReportsService(ReportsService reportsService) {
		this.reportsService = reportsService;
	}

	@RequestMapping("/reports")
	public String showReports(Model model, ModelMap model2) {
		List<Report> reports = reportsService.getCurrent();
		model.addAttribute("reports", reports);
		return "reports";
	}

	@RequestMapping("/residentsreports")
	public String showReportsForResidents(Model model, ModelMap model2) {
		List<Report> reports = reportsService.getCurrent();
		model.addAttribute("reports", reports);
		return "residentsreports";
	}
	
	@RequestMapping("/createreport")
	public String createReport(Model model) {
		model.addAttribute("report", new Report());
		return "createreport";
	}

	@RequestMapping(value = "/docreate", method = RequestMethod.POST)
	public String doCreate(Model model, @Validated(value = FormValidationGroup.class) Report report,
			BindingResult result, Principal principal) {

		if (result.hasErrors()) {
			return "createreport";
		}
		String username = principal.getName();
		report.getUser().setUsername(username);
		;
		reportsService.saveOrUpdate(report);
		return "reportcreated";
	}
}
