<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>

<head>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9z1qJLw1fTEoHn1_JxuggptNZFLOz56o"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/angular.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/appdirective.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/angular-google-maps.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/lodash.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/moment-with-locales.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/bootstrap-datetimepicker.js"></script>

<link
	href="${pageContext.request.contextPath}/static/css/createreport/styles.css"
	rel="stylesheet" type="text/css">
<link
	href="${pageContext.request.contextPath}/static/css/createreport/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/static/css/createreport/ladda-themeless.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/static/css/createreport/bootstrap-datetimepicker.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/static/css/createreport/createreport.css"
	rel="stylesheet">
</head>
<sec:authentication property="principal" var="principal" />

<div ng-app="myApp" class="divContainer" ng-controller="MinMaxCtrl"
	ng-model="username" ng-init="username='${principal.username}'">
	<h1 align="center"><u>Create Anti-Social Behaviour Report</u></h1>
	<div class="formDiv" ng-init="setMapValues()">
		<form ng-submit="onSubmit()" novalidate="novalidate"
			ng-hide="submitted" role="form" name="createReportForm">
			<div class="formInputMargin">
				<label for="datetime" class="control-label">Date/Time</label>
				<div class="input-group date" id="datetimepicker1">
					<input type="text" class="form-control"
						ng-model="formModel.datetime" format="dd/MM/yyyy HH:MM a"
						id="datetime" required="required" /> <span
						class="input-group-addon"> <span
						class="glyphicon-calendar glyphicon"></span>
					</span>
				</div>
				<script>
					var d = new Date();
					var hours = d.getHours();
					var mins = d.getMinutes();
					console.log(hours);
					console.log(mins);
					console.log("Day is... " + d.getDate());
					var firstDayOfMonth = function() {
					    // your special logic...
					    return d.getDate();
					};
					
					var dd = new Date();
					console.log(dd)
					var currMonth = dd.getMonth();
					var currYear = dd.getFullYear();
					var startDate = new Date(currYear,currMonth,firstDayOfMonth());
					console.log(startDate)
					$('#datetimepicker1').datetimepicker({
						format: 'DD-MM-YYYY HH:mm',
						minDate : moment({h:0, minute: 0}),
						maxDate: moment({h:hours, minute: mins})
					});
				</script>
			</div>

			<div class="formInputMargin">
				<label for="address1" class="control-label">Address 1</label> <input
					type="text" class="form-control" ng-model="formModel.address1"
					 id="address1" required="required">
			</div>

			<div class="formInputMargin">
				<label for="city" class="control-label">City</label> <input
					type="text" class="form-control"
					ng-model="formModel.city" 
					id="city" required="required">
			</div>

			<div class="formInputMargin">
				<label for="county" class="control-label">County</label> 
				<div class="input-group date">
					<input type="text" class="form-control" ng-model="formModel.county"
					id="county" required="required">
					<span class="input-group-addon" ng-click="testMe()"><b>Update Address On Map</b></span>
					</div>
			</div>
			
			<div class="formInputMargin">
				<label for="category" class="control-label">Category</label> <select
					id="category" class="form-control" ng-model="formModel.category"
					required="required" value="Choose Category"
					ng-options="c.value for c in categories">
					<option value="">-- Choose an option --</option>
				</select>
			</div>

			<div class="formInputMargin">
				<label for="description" class="control-label">Description</label>
				<textarea type="text" class="form-control"
					ng-model="formModel.description" id="description"
					required="required"></textarea>
			</div>

			<div align="center" class="formInputMargin">
				<button ng-disabled="createReportForm.$invalid" class="btn btn-primary" type="submit">Create Report</button>
			</div>

		</form>
	</div>
	<div class="mapDiv">
		<div class="actualMap">
			<ui-gmap-google-map center="map.center" zoom="map.zoom"
				draggable="true" options="options"> <ui-gmap-marker
				coords="marker.coords" options="marker.options"
				events="marker.events" idkey="marker.id"></ui-gmap-marker> </ui-gmap-google-map>
		</div>
	</div>
</div>



<script
	src="${pageContext.request.contextPath}/static/script/createreport/jcs-auto-validate.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/spin.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/ladda.min.js"></script>
<script
	src="${pageContext.request.contextPath}/static/script/createreport/angular-ladda.min.js"></script>