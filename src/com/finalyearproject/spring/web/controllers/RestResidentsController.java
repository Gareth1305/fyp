package com.finalyearproject.spring.web.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finalyearproject.spring.web.dao.Council;
import com.finalyearproject.spring.web.dao.Resident;
import com.finalyearproject.spring.web.service.CouncilsService;
import com.finalyearproject.spring.web.service.ResidentsService;

@Controller
@RequestMapping("/api")
public class RestResidentsController {

	private ResidentsService residentsService;

	@Autowired
	public void setResidentsService(ResidentsService residentsService) {
		this.residentsService = residentsService;
	}

	private CouncilsService councilsService;

	@Autowired
	public void setCouncilsService(CouncilsService councilsService) {
		this.councilsService = councilsService;
	}
	
	@RequestMapping(value = "/getResidentByUsername/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Resident getVolunteerByUsername(@PathVariable String username) {
		return residentsService.getResidentByUsername(username);
	}
	
	@RequestMapping(value = "/getResidentsByCouncilName/{councilname}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Resident> getResidentsByCouncilName(@PathVariable String councilname) {
		return residentsService.getResidentsByCouncilName(councilname);
	}
	
	@RequestMapping(value = "/doesUsernameExist/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public boolean doesUsernameExist(@PathVariable String username) {
		return residentsService.doesUsernameExist(username);
	}
	
	@RequestMapping(value = "/getAllResidents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Resident> getAllResidents() {
		return residentsService.getAllResidents();
	}
	
	@RequestMapping(value = "/getElderlyAndDisabledResidents", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Resident> getElderlyAndDisabledResidents() {
		return residentsService.getElderlyAndDisabledResidents();
	}
	
	@RequestMapping(value = "/createResident", method = RequestMethod.POST)
	@ResponseBody
	public void registerResident(@RequestBody String residentJson) {
		System.out.println(residentJson);
		System.out.println(residentJson);
	
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(residentJson, new TypeReference<HashMap<String, String>>() {
			});
			createResident(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createResident(Map<String, String> map) {
		String username = (String) map.get("username");
		String password = (String) map.get("password");
		String email = (String) map.get("email");
		String firstname = (String) map.get("firstname");
		String lastname = (String) map.get("lastname");
		String address = (String) map.get("address");
		String city = (String) map.get("city");
		String county = (String) map.get("county");
		String dob = (String) map.get("dob");
		String ecn = (String) map.get("ecn");
		String ecr = (String) map.get("ecr");
		String phone = (String) map.get("phone");
		String disability = (String) map.get("disability");
		String latitude = (String) map.get("latitude");
		String longitude = (String) map.get("longitude");
		String councilname = (String) map.get("council");

		double lat = Double.parseDouble(latitude);
		double lng = Double.parseDouble(longitude);
		
		SimpleDateFormat ft = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		try {
			Date date = ft.parse(dob);
			boolean enabled = true;
			String authority = "ROLE_USER";
			boolean isDisabled;
			if(disability.equals("Yes")) {
				isDisabled = true;
			} else {
				isDisabled = false;
			}

			Council council = councilsService.getCouncilByCouncilName(councilname);

			Resident newResident = new Resident(username, password, email, enabled, authority, firstname,
					lastname, address, city, county, date, ecn, ecr, phone, isDisabled, lat, lng, council);
			if(newResident != null) { 
				residentsService.createResident(newResident);
			}
		} catch (ParseException p) {
            p.printStackTrace();
        }
//		SmsSender sender = new SmsSender();
//		sender.sendSMS();
	}

}
