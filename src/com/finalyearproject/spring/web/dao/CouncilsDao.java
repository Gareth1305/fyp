package com.finalyearproject.spring.web.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("councilsDao")
public class CouncilsDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	public void saveOrUpdate(Council council) {
		session().saveOrUpdate(council);
	}
	
	public Council getCouncilByCouncilName(String councilname) {
		Criteria crit = session().createCriteria(Council.class);
		crit.add(Restrictions.idEq(councilname));
		Council council = (Council) crit.uniqueResult();
		return council;
	}
	
	@SuppressWarnings("unchecked")
	public List<Council> getAllCouncils() {
		return session().createQuery("from Council").list();
	}
	
	
}
