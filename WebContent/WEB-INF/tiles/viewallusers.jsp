<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<head>
	<script
		src="${pageContext.request.contextPath}/static/script/angular.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/static/script/viewallusers/viewallusers.js"></script>

	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="screen"/> 

	<link
	href="${pageContext.request.contextPath}/static/css/viewallusers/viewallusers.css"
	rel="stylesheet">
</head>

<div class="container" ng-app="viewAllUsers" ng-controller="ViewAllUsersCtrl">

  <form class="search_form">
    <div class="form-group">
      <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-search"></i></div>
        <input type="text" class="form-control" placeholder="Search Users" ng-model="searchUser">
      </div>      
    </div>
  </form>
  
  <table class="table table-bordered table-striped">
    
    <thead>
      <tr>
        <td>
          <a href="#" ng-click="sortType = 'username'; sortReverse = !sortReverse">
            Username:
            <span ng-show="sortType == 'username' && !sortReverse" class="fa fa-caret-down"></span>
            <span ng-show="sortType == 'username' && sortReverse" class="fa fa-caret-up"></span>
          </a>
        </td>
        <td>
          <a href="#" ng-click="sortType = 'firstname'; sortReverse = !sortReverse">
          First Name: 
            <span ng-show="sortType == 'firstname' && !sortReverse" class="fa fa-caret-down"></span>
            <span ng-show="sortType == 'firstname' && sortReverse" class="fa fa-caret-up"></span>
          </a>
        </td>
        <td>
          <a href="#" ng-click="sortType = 'lastname'; sortReverse = !sortReverse">
          Last Name:
            <span ng-show="sortType == 'lastname' && !sortReverse" class="fa fa-caret-down"></span>
            <span ng-show="sortType == 'lastname' && sortReverse" class="fa fa-caret-up"></span>
          </a>
        </td>
        <td>
          <a href="#" ng-click="sortType = 'authority'; sortReverse = !sortReverse">
          User Type:
            <span ng-show="sortType == 'authority' && !sortReverse" class="fa fa-caret-down"></span>
            <span ng-show="sortType == 'authority' && sortReverse" class="fa fa-caret-up"></span>
          </a>
        </td>
        <td class="suspendAccountsColumn" align="center" width="137px">Suspend Accounts</td>
      </tr>
    </thead>
    
    <tbody>
      <tr ng-repeat="user in allUsers | orderBy:sortType:sortReverse | filter:searchUser | startFrom:currentPage*pageSize | limitTo:pageSize">
        <td>{{ user.username }}</td>
        <td>{{ user.firstname }}</td>
        <td>{{ user.lastname }}</td>
        <td ng-if="user.authority=='ROLE_USER'">Resident</td>
        <td ng-if="user.authority=='ROLE_ADMIN'">Admin</td>
        <td ng-if="user.authority=='ROLE_VOLUNTEER'">Volunteer</td>
        <td align="center" class="btn_cell"><button class="btn btn-primary" ng-click="suspendAccount(user)">Suspend Account</button></td>
      </tr>
      
    </tbody>
    
  </table>
    <button ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">
        Previous
    </button>
    {{currentPage+1}}/{{numberOfPages()}}
    <button ng-disabled="currentPage >= data.length/pageSize - 1" ng-click="currentPage=currentPage+1">
        Next
    </button>
</div>