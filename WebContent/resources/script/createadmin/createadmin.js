var app = angular.module("prasbicApp", ['jcs-autoValidate']);

app.controller('CreateAdminCtrl', function($scope, $log, $timeout, $http) {
	console.log("Controller has been called");
	$scope.formModel = {};
	
	$scope.onSubmit = function() {
		if($scope.formModel.password == $scope.formModel.confirmpass) {
			var adminObjToSend = {};
			adminObjToSend.username = $scope.formModel.username;
			adminObjToSend.email = $scope.formModel.email;
			adminObjToSend.password = $scope.formModel.password;
			adminObjToSend.firstname = $scope.formModel.firstname
			adminObjToSend.lastname = $scope.formModel.lastname;
			console.log(adminObjToSend);
			
			$http.post('http://localhost:8080/prasbic/api/createAdmin', JSON.stringify(adminObjToSend))
	        .success(function (data, status, headers, config) {
	       	 	if(status == 200) {
	       	 		alert("Admin Account Successfully Created")
	       	 		window.location = "http://localhost:8080/prasbic/newadminaccount";
	       	 	}
	           })
	           .error(function (error) {
	           	alert("An error has occurred")
	           });		
		} else {
			alert("Passwords do nt match");
		}	
	};
});