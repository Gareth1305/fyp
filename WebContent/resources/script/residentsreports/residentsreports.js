var myApp = angular.module("resReports", []);

myApp.controller('ResidentReportsCtrl', ['$scope', '$interval', '$http', function($scope, $interval, $http, $dialogs) {
	
	var circle;
	var marker;
	$scope.markers = [];
	$scope.reportInformation = [];
	var map;
	$scope.locObj = {};
	$scope.geocoder = new google.maps.Geocoder();
	$scope.formModel = {};
	$scope.dateCategories =
   		[{id : 1,value : "Past 24 Hours"}, 
   		 {id : 2,value : "Past Week"},
   		 {id : 3,value : "Past Month"}, 
   		 {id : 4,value : "Since the beginning of time"}];	
	$scope.formModel.datetime = $scope.dateCategories[1];
	
	$scope.callMe = function(address) {
		if(address == undefined) {
			console.log("Address field is empty")
		} else {
			$scope.codeAddress(address);
		}
	};

	$scope.last24 = function() {
		$scope.removeMarkers();
		var resLatlng = new google.maps.LatLng($scope.locObj.latitude, $scope.locObj.longitude);
		$scope.reportInformation = [];
		$http.get('http://localhost:8080/prasbic/api/getReportsFromLast24Hours')
	    .success(function (reports) {
	    	reports.forEach(function(singleReport) {
				var reportLatlng = new google.maps.LatLng(singleReport.latitude, singleReport.longitude);
	    		var d = $scope.getDistance(resLatlng, reportLatlng);
	    		if(d < $scope.formModel.radius) {
					$scope.reportInformation.push(singleReport)
					$scope.displayMarkers(singleReport);
				}
	    	});
	    	$scope.showMarkersOnMap();
	    });
	};
	
	$scope.showMarkersOnMap = function() {
		$scope.markerArray.forEach(function(individualMarker) {
		});
	}
	
	$scope.markerArray = [];
	
	$scope.lastWeek = function() {
		$scope.removeMarkers();
		var resLatlng = new google.maps.LatLng($scope.locObj.latitude, $scope.locObj.longitude);
		$scope.reportInformation = [];
		$http.get('http://localhost:8080/prasbic/api/getReportsFromPastWeek')
	    .success(function (reports) {
	    	reports.forEach(function(singleReport) {
				var reportLatlng = new google.maps.LatLng(singleReport.latitude, singleReport.longitude);
	    		var d = $scope.getDistance(resLatlng, reportLatlng);
	    		if(d < $scope.formModel.radius) {
					$scope.reportInformation.push(singleReport);
					$scope.displayMarkers(singleReport);
				}
	    	});
	    	$scope.showMarkersOnMap();
	    });
	};

	$scope.displayMarkers = function(report) {
		
		var icon;
		switch(report.category) {
			case 'Alcohol Abuse':
				icon = 'http://localhost:8080/prasbic/static/images/alcohalabuse.png';
				break;
			
			case 'Drug Abuse':
				icon = 'http://localhost:8080/prasbic/static/images/drugabuse.png';
				break;
				
			case 'Burglary':
				icon = 'http://localhost:8080/prasbic/static/images/burglary.png';
				break;
				
			case 'Fighting':
				icon = 'http://localhost:8080/prasbic/static/images/fighting.png';
				break;
				
			case 'Other':
				icon = 'http://localhost:8080/prasbic/static/images/other.png';
				break;
				
			case 'Theft':
				icon = 'http://localhost:8080/prasbic/static/images/theft.png';
				break;
				
			case 'Joy Riding':
				icon = 'http://localhost:8080/prasbic/static/images/joyriding.png';
				break;
				
			case 'Vandalism':
				icon = 'http://localhost:8080/prasbic/static/images/vandalism.png';
				break;
		}

	  marker = new google.maps.Marker({
	    position: new google.maps.LatLng(report.latitude, report.longitude),
	    map: map,
	    icon: icon,
	    title: report.category,
	    animation: google.maps.Animation.DROP,
	    id: report.id,
	    category: report.category
	  });
	  $scope.markerArray.push(marker);
	  google.maps.event.addListener(marker, 'click', (function(marker, i) {
		 return function() {
			 toggleBounce(marker);
		 }
	  })(marker, report));
	};
	
	$scope.removeMarkers = function() {
		for (var i = 0; i <  $scope.markerArray.length; i++) {
			 $scope.markerArray[i].setMap(null);
	    }
	};
	
	$scope.rad = function(x) {
    	return x * Math.PI / 180;
	};

	$scope.getDistance = function(p1, p2) {
		var R = 6378137;
		var dLat = $scope.rad(p2.lat() - p1.lat());
		var dLong = $scope.rad(p2.lng() - p1.lng());
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos($scope.rad(p1.lat())) * Math.cos($scope.rad(p2.lat())) *
			Math.sin(dLong / 2) * Math.sin(dLong / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;
		return d;
	};
	
	$scope.lastMonth = function() {
		$scope.removeMarkers();
		var resLatlng = new google.maps.LatLng($scope.locObj.latitude, $scope.locObj.longitude);
		$scope.reportInformation = [];
		$http.get('http://localhost:8080/prasbic/api/getReportsFromPastMonth')
	    .success(function (reports) {
	    	reports.forEach(function(singleReport) {
				var reportLatlng = new google.maps.LatLng(singleReport.latitude, singleReport.longitude);
	    		var d = $scope.getDistance(resLatlng, reportLatlng);
	    		if(d < $scope.formModel.radius) {
					$scope.reportInformation.push(singleReport)
					$scope.displayMarkers(singleReport);
				}
	    	});
	    	$scope.showMarkersOnMap();
	    });
	};
	
	$scope.allTime = function() {
		$scope.removeMarkers();
		var resLatlng = new google.maps.LatLng($scope.locObj.latitude, $scope.locObj.longitude);
		$scope.reportInformation = [];
		$http.get('http://localhost:8080/prasbic/api/getAllReports')
	    .success(function (reports) {
	    	reports.forEach(function(singleReport) {
				var reportLatlng = new google.maps.LatLng(singleReport.latitude, singleReport.longitude);
	    		var d = $scope.getDistance(resLatlng, reportLatlng);
	    		if(d < $scope.formModel.radius) {
					$scope.reportInformation.push(singleReport);
					$scope.displayMarkers(singleReport);
				}
	    	});
	    	$scope.showMarkersOnMap();
	    });
	};
	
	$scope.codeAddress = function(address) {
		$scope.geocoder.geocode( { 'address': address}, function(results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	        	$scope.locObj.latitude = results[0].geometry.location.lat();
	    		$scope.locObj.longitude = results[0].geometry.location.lng();
	    		$scope.setMap();
	        }
	        else {
	            alert("Please enter a valid address");
	        }
	    });
	};
	
	
	$scope.setLoggedInUser = function() {
		$http.get('http://localhost:8080/prasbic/api/getResidentByUsername/'+$scope.username)
	    .success(function (data2) {
	    	$scope.formModel.latitude = data2.latitude;
	    	$scope.formModel.longitude = data2.longitude;
	    	$scope.locObj.latitude = data2.latitude;
    		$scope.locObj.longitude = data2.longitude;
    		$scope.loggedInUsersAddress = data2.address + ", " + data2.city + ", Co. " + data2.county + ", Ireland";
    		$scope.formModel.addressField = data2.address + ", " + data2.city + ", Co. " + data2.county + ", Ireland";
    		$scope.setMap();
	    });

	}
	
	$scope.setMap = function() {
		var mapOptions = {
	        zoom: 15,
	        center: new google.maps.LatLng($scope.locObj.latitude, $scope.locObj.longitude),
	        mapTypeId: google.maps.MapTypeId.HYBRID
		}

		map  = new google.maps.Map(document.getElementById('map'), mapOptions);
		
		marker = new google.maps.Marker({
		    position: new google.maps.LatLng($scope.locObj.latitude, $scope.locObj.longitude),
		    map: map,
		    icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
		    title: 'Your Address'
		  });
		
		circle = new google.maps.Circle({
    		  map: map,
    		  radius: parseFloat($scope.formModel.radius),
    		  fillColor: '#AA0000',
    		  strokeColor: '#000000',
    		  strokeWeight: 1,
		});
		circle.bindTo('center', marker, 'position');
		if($scope.reportInformation.length == 0) {
			$scope.submitForm();
		}
	}
	
	$scope.clickMe = function(val) {				
		circle.set('radius', parseInt(val));
		$scope.submitForm()
	}

	$scope.submitForm = function() {
		switch ($scope.formModel.datetime.id) {
	    case 1:
	        $scope.last24();
	        break;
	    case 2:
	        $scope.lastWeek();
	        break;
	    case 3:
	    	$scope.lastMonth();
	        break;
	    case 4:
	    	$scope.allTime();
	        break;
		}
	}
	
	$scope.selectedRow = null;		
	
	$scope.setClickedRow = function(index, report){
		$scope.markerArray.forEach(function(m) {
			if(m.id == report.id) {
				console.log(m);
				console.log(report)
				toggleBounce(m);
			}
		});
		$scope.selectedRow = index;
	}
	
	function toggleBounce(marker) {
		if (marker.getAnimation() !== null) {
			marker.setAnimation(null);
		} else {
			marker.setAnimation(google.maps.Animation.BOUNCE);
			setTimeout(function () {
		        marker.setAnimation(null);
		    }, 4560);
		}
	}
	
	$scope.getLocation = function() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition($scope.showPosition);
	    } else {
	        console.log("Geolocation is not supported by this browser.");
	    }
	}
	
	$scope.showPosition = function(position) {
		$scope.locObj.latitude = position.coords.latitude;
		$scope.locObj.longitude = position.coords.longitude;
		$scope.setMap();
		$scope.getAddressFormat(position.coords.latitude, position.coords.longitude);
	}

	$scope.updateRadius = function(val) {
		$scope.circle.radius = val;
	}
	
	$scope.time_date_changed = function() {
		$scope.submitForm();
	}
	
	$scope.getAddressFormat = function(lat, lng) {
		var latlng = new google.maps.LatLng(lat, lng);
		$scope.geocoder.geocode({'latLng': latlng}, function(results, status) {	
			if (status == google.maps.GeocoderStatus.OK) {
				$scope.$apply(function () {
		            $scope.formModel.addressField = results[0].formatted_address;
		        });
			}		
		});
		$scope.submitForm();
	}
	
	$scope.updateAdress = function() {
		$scope.getLocation();
	}
	
	$scope.clearFormFields = function() {
		$scope.locObj.latitude = $scope.formModel.latitude;
		$scope.locObj.longitude = $scope.formModel.longitude;
		$scope.formModel.datetime = $scope.dateCategories[1];
		$scope.setMap();
		$scope.reportInformation = [];
		$scope.removeMarkers();
		$scope.clickMe(250);
		$scope.formModel.radius = 250;
		$scope.formModel.addressField = $scope.loggedInUsersAddress;
	}

}]);
