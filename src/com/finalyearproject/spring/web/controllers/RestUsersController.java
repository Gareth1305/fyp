package com.finalyearproject.spring.web.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finalyearproject.spring.web.dao.User;
import com.finalyearproject.spring.web.service.UsersService;

@Controller
@RequestMapping("/api")
public class RestUsersController {

	private UsersService usersService;

	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}

	@RequestMapping(value = "/getAllUsers", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<User> getAllUsers() {
		return usersService.getAllUsers();
	}
	
	@RequestMapping(value = "/getNumOfReportsPerUser/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public int getVolunteerByUsername(@PathVariable String username) {
		System.out.println(username);
		return usersService.getNumOfReportsPerUser(username);
	}
	
	@RequestMapping(value = "/createAdmin", method = RequestMethod.POST)
	@ResponseBody
	public void registerUser(@RequestBody String adminJson) {
		System.out.println("Ive been hit");
		System.out.println(adminJson);
		System.out.println("After printing report");
		
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			map = mapper.readValue(adminJson, new TypeReference<HashMap<String, String>>() {
			});
			createAdmin(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createAdmin(Map<String, String> map) {
		String username = (String) map.get("username");
		String email = (String) map.get("email");
		String password = (String) map.get("password");
		String firstname = (String) map.get("firstname");
		String lastname = (String) map.get("lastname");
		
		User newAdmin = new User();
		newAdmin.setEmail(email);
		newAdmin.setFirstname(firstname);
		newAdmin.setLastname(lastname);
		newAdmin.setUsername(username);;
		newAdmin.setPassword(password);
		newAdmin.setAuthority("ROLE_ADMIN");
		newAdmin.setEnabled(true);
		if(newAdmin != null) { 
			usersService.create(newAdmin);
		}
	}
//
//	@RequestMapping(value = "/checklogininformation/{username}/{password}", method = RequestMethod.GET, produces = "application/json")
//	@ResponseBody
//	public String testGetWithBody(@PathVariable String username, @PathVariable String password) {
//		User user = (User) usersService.getUserByUsername(username, password);
//		if(user!=null) {
//			return "SUCCESS,"+user.getEmail();
//		} else {
//			return "FAILURE";
//		}		
//	}
//
//	@RequestMapping(value = "/create_user", method = RequestMethod.POST)
//	@ResponseBody
//	public String savePerson(User user) {
//		user.setAuthority("ROLE_USER");
//		user.setEnabled(true);
//		usersService.create(user);
//		return "User Saved: " + user.toString();
//	}

}
