package com.finalyearproject.spring.web.dao;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="checkups")
public class Checkup {
	@Id
	private int id;
	private boolean isChecked = false;
	@ManyToOne
	@JoinColumn(name="report_id")
	private Report report;
	@ManyToOne
	@JoinColumn(name="res_username")
	private Resident resident;
	@ManyToOne
	@JoinColumn(name="vol_username")
	private Volunteer volunteer;
	public Checkup() {
		
	}
	
	public Checkup(boolean isChecked, Report report, Resident resident) {
		this.isChecked = isChecked;
		this.report = report;
		this.resident = resident;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}

	public Resident getResident() {
		return resident;
	}

	public void setResident(Resident resident) {
		this.resident = resident;
	}

	public Volunteer getVolunteer() {
		return volunteer;
	}

	public void setVolunteer(Volunteer volunteer) {
		this.volunteer = volunteer;
	}

	@Override
	public String toString() {
		return "Checkup [id=" + id + ", isChecked=" + isChecked + ", report=" + report + ", resident=" + resident
				+ ", volunteer=" + volunteer + "]";
	}

}
