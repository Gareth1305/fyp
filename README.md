# README #

This repository is a representation of my final year project which was completed in May 2016. This projects name is **PRASBIC** (**PR**eventing **A**nti-**S**ocial **B**ehavior **I**n **C**ommunities). There are three user types for this application, the resident, the volunteer and the administrator. The backbone of this web application is that residents create reports of anti social behavior, volunteers respond accordingly and administrators view reports and monitor statistics. Technologies, architecture and screenshots are shown below.

### Technologies Used? ###

* Spring
* AngularJS
* Java
* JavaScript
* CSS
* Bootstrap
* AmCharts
* BulkSMS API
* RESTful web services
* Apache Tomcat
* MySQL database
* Apache Tiles
* Google Maps API

### System Architecture ###
![System_Architecture_3.png](https://bitbucket.org/repo/BKLxXp/images/738650311-System_Architecture_3.png)

### Application Screenshots ###
![Homepage.PNG](https://bitbucket.org/repo/BKLxXp/images/86951471-Homepage.PNG)
______________________________________________________________________________________




![stats.PNG](https://bitbucket.org/repo/BKLxXp/images/2823376633-stats.PNG)
______________________________________________________________________________________




![viewReportsPC.PNG](https://bitbucket.org/repo/BKLxXp/images/162413033-viewReportsPC.PNG)
______________________________________________________________________________________




![viewReportsRes.png](https://bitbucket.org/repo/BKLxXp/images/1140978897-viewReportsRes.png)
______________________________________________________________________________________

