package com.finalyearproject.spring.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.finalyearproject.spring.web.dao.User;
import com.finalyearproject.spring.web.dao.UsersDao;

@Service("usersService")
public class UsersService {

	private UsersDao usersDao;

	@Autowired
	public void setUsersDao(UsersDao usersDao) {
		this.usersDao = usersDao;
	}

	public void create(User user) {
		usersDao.create(user);
	}

	public boolean exists(String username) {
		return usersDao.exists(username);
	}

	@Autowired
	private PasswordEncoder passwordEncoder;

	public List<User> getAllUsers() {
		return usersDao.getAllUsers();
	}

	public User getUserByUsername(String username, String password) {

		User user = usersDao.getUserByUsername(username);

		if (passwordEncoder.matches(password, user.getPassword())) {
			return user;
		} else {
			return null;
		}
	}

	public User getUserByUsername(String username) {
		User user = usersDao.getUserByUsername(username);
		return user;
	}

	public int getNumOfReportsPerUser(String username) {
		return usersDao.getNumOfReportsPerUser(username);
	}

}
