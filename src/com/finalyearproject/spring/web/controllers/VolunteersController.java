package com.finalyearproject.spring.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finalyearproject.spring.web.dao.FormValidationGroup;
import com.finalyearproject.spring.web.dao.Volunteer;
import com.finalyearproject.spring.web.service.VolunteersService;

@Controller
public class VolunteersController {

    private VolunteersService volunteersService;

    @Autowired
    public void setVolunteersService(VolunteersService volunteersService) {
        this.volunteersService = volunteersService;
    }

    @RequestMapping("/newvolunteeraccount")
    public String showNewAccount(Model model) {
        model.addAttribute("volunteer", new Volunteer());
        return "newvolunteeraccount";
    }

    @RequestMapping("/volunteeraccountcreated")
    public String showNewAccofunt() {
        return "volunteeraccountcreated";
    }

    @RequestMapping(value = "/createvolunteeraccount", method = RequestMethod.POST)
    public String createVolunteerAccount(@Validated(FormValidationGroup.class) Volunteer volunteer, BindingResult result) {
        if (result.hasErrors()) {
            return "newvolunteeraccount";
        }

        volunteer.setEnabled(true);
        volunteer.setAuthority("ROLE_VOLUNTEER");

        if (volunteersService.exists(volunteer.getUsername())) {
            result.rejectValue("username", "DuplicateKey.volunteer.username");
            return "newvolunteeraccount";
        }
        try {
            volunteersService.createVolunteer(volunteer);
        } catch (DataAccessException e) {
            result.rejectValue("username", "DuplicateKey.volunteer.username");
            return "newvolunteeraccount";
        }

        return "volunteeraccountcreated";
    }
    
	@RequestMapping(value = "/api/getVolunteerByUsername/{username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Volunteer getVolunteerByUsername(@PathVariable String username) {
		Volunteer vol = (Volunteer) volunteersService.getVolunteerByUsername(username);
		return vol;		
	}
}
