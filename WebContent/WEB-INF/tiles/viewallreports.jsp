<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<head>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="screen"/> 
<script src="${pageContext.request.contextPath}/static/script/createreport/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/static/script/viewallreports/viewallreports.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/js/jasny-bootstrap.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.1/css/jasny-bootstrap.css" rel="stylesheet" media="screen"/> 
<link href="${pageContext.request.contextPath}/static/css/bootstrap-theme.min.css" rel="stylesheet" media="screen"/> 
<link href="${pageContext.request.contextPath}/static/css/viewallreports/viewallreports.css" rel="stylesheet" media="screen"/> 

</head>

<div class="divContainer" ng-app="allReportsView" ng-controller="AllReportsCtrl">
	<div class="contentContainer">
		<form class="search_form">
    		<div class="form-group">
      			<div class="input-group">
        			<div class="input-group-addon"><i class="fa fa-search"></i></div>
        			<input type="text" class="form-control" placeholder="Search Reports" ng-model="searchReport">
   				</div>      
   			</div>
  		</form>
  		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<td class="tableCell reportId">
						<a href="#"
							ng-click="sortType = 'id'; sortReverse = !sortReverse">ID:
								<span ng-show="sortType == 'id' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'id' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell reportCategory">
						<a href="#"
							ng-click="sortType = 'category'; sortReverse = !sortReverse">Category:
								<span ng-show="sortType == 'category' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'category' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell reportDescription">
						<a href="#"
							ng-click="sortType = 'description'; sortReverse = !sortReverse">Description:
								<span ng-show="sortType == 'description' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'description' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell reportCity">
						<a href="#"
							ng-click="sortType = 'city'; sortReverse = !sortReverse">City:
								<span ng-show="sortType == 'city' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'city' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell reportCounty">
						<a href="#"
							ng-click="sortType = 'county'; sortReverse = !sortReverse">County:
								<span ng-show="sortType == 'county' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'county' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell reportDate">
						<a href="#"
							ng-click="sortType = 'date'; sortReverse = !sortReverse">Date:
								<span ng-show="sortType == 'date' && !sortReverse"
								class="fa fa-caret-down"></span> <span
								ng-show="sortType == 'date' && sortReverse"
								class="fa fa-caret-up"></span>
						</a>
					</td>
					<td class="tableCell detailedViewColumn" align="center" width="137px">Detailed View</td>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="report in allReports | orderBy:sortType:sortReverse | filter:searchReport | startFrom:currentPage*pageSize | limitTo:pageSize">
			    	<td>{{ report.id }}</td>
			    	<td>{{ report.category }}</td>
			    	<td>{{ report.description }}</td>
			        <td>{{ report.city }}</td>
			        <td>{{ report.county }}</td>
			        <td>{{ report.date | date:'dd MMM yyyy'}}</td>
			        <td align="center" class="btn_cell">
			        	<button ng-click="viewReport(report)" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Select</button>
		        	</td>
				</tr>
			</tbody>
		</table>
		<div ng-show="selected">Selection from a modal: {{ selected }}</div>
		<div class="overflowPageDiv">
			<button class="switchPageBtn" ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">Previous</button>
	    		{{currentPage+1}}/{{numberOfPages()}}
	    	<button class="switchPageBtn" ng-disabled="currentPage >= data.length/pageSize - 1" ng-click="currentPage=currentPage+1">Next</button>
		</div>
	</div>
	<div id="myModal" class="modal fade">
	    <div class="first_modal_dialog modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                <h4 class="modal-title">Detailed View Of Selected Report</h4>
	            </div>
	            <div class="modal-body clearfix">
	                <div class="modalPictureDiv">
	                	<div align="center"><h4><u>Report Information</u></h4></div>
		    			<img class="imageIcon" align="middle"  src="${pageContext.request.contextPath}/static/images/{{activeReport.imageTitle}}" width="100px" height="100px">
						<div class="startOfInfo" ><b>Category: </b>{{activeReport.category}}</div>
						<div class="reportDescriptionDiv"><b>Date Created:</b> {{activeReport.date | date:'dd MMM yyyy'}}</div>
						<div class="reportDescriptionDiv"><b>Description:</b> {{activeReport.description}}</div>
						<div class="reportDescriptionDiv"><b>Address:</b> {{activeReport.address1}}, {{activeReport.city}}, {{activeReport.county}}</div>
					</div>
					<div class="modalEditableDiv">
	                	<div class="split-form-container">
							<div class="first-split-form">
								<h4><u>Council Information</u></h4>
								<div class="councilname"><b>Name:</b> {{activeReport.council.councilname}}</div>
								<div class="wrapperDiv">
									<div class=councilemail><b>Email:</b> {{activeReport.council.councilemail}}</div>
									<div class=councilphone><b>Phone:</b> {{activeReport.council.contactnum}}</div>
								</div>
								<div class="councilhq"><b>Headquarters:</b> {{activeReport.council.councilhq}}</div>
							</div>
							<div class="second-split-form">
								<h4><u>User Information</u></h4>
								<div class="councilname"><b>Name:</b> {{activeReport.user.firstname}} {{activeReport.user.lastname}}</div>
								<div class="wrapperDiv">
									<div class=councilemail><b>Emergency Contact:</b> {{activeReport.user.ecn}}</div>
									<div class=councilphone><b>Phone:</b> {{activeReport.user.phone}}</div>
								</div>
								<div class="councilhq"><b>Address:</b> {{activeReport.user.address}}, {{activeReport.user.city}}, {{activeReport.user.county}}</div>
								<div class="councilhq"><b>Number of Reports to Date:</b> {{activeReport.user.numberOfReports}}</div>
							</div>
						</div>
	                </div> 
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>