package com.finalyearproject.spring.web.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@Component("volunteersDao")
public class VolunteersDao {
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public void create(Volunteer volunteer) {
		volunteer.setPassword(passwordEncoder.encode(volunteer.getPassword()));
		session().save(volunteer);
	}
	
	public boolean exists(String username) {
		Criteria crit = session().createCriteria(Volunteer.class);
		crit.add(Restrictions.idEq(username));
		Volunteer volunteer = (Volunteer) crit.uniqueResult();
		return volunteer != null;
	}
	
	@SuppressWarnings("unchecked")
	public List<Volunteer> getAllVolunteers() {
		return session().createQuery("from Volunteer").list();
	}
	
	public Volunteer getVolunteerByUsername(String username) {
		Criteria crit = session().createCriteria(Volunteer.class);
		crit.add(Restrictions.idEq(username));
		Volunteer volunteer = (Volunteer) crit.uniqueResult();
		return volunteer;		
	}
	
}
