var app = angular.module("prasbicApp", [ 'jcs-autoValidate' ]);

app.controller('CreateVolunteerCtrl', function($scope, $log, $timeout, $http) {
	console.log("Controller has been called");
	
	$scope.formModel = {};
	$scope.add = {};
	
   	$scope.options = [ 
		{id : 1,value : "Yes"}, 
		{id : 2,value : "No"}
	];
	
   	$scope.councils = [];
   	
   	$scope.setCouncils = function() {
   		$http.get('http://localhost:8080/prasbic/api/getAllCouncils').success(function (councils) {
   			var counter = 1;
   			councils.forEach(function(council) {
				$scope.councils.push({"id":counter,"value":council.councilname});
				counter = counter + 1;
			})
		});
   	}
   	
   	$scope.valid = false;
   	$scope.doesUserExist = false;
   	$scope.checkUsername = function(username) {
   		console.log("Check has been called")
   		console.log(username)
   		$http.get('http://localhost:8080/prasbic/api/doesUsernameExist/'+username).success(function (exist) {
   			console.log(exist)
   			if(exist == true) {
   				$scope.doesUserExist = true;
   			} else if(exist == false) {
   				$scope.doesUserExist = false;
   			}
   		});
   	}
   	   	
   	$scope.checkPasswordMatch = function() {
   		if($scope.formModel.confirmpass == $scope.formModel.password) {
   			$scope.doPasswordMatch = true;
   		} else {
   			$scope.doPasswordMatch = false;
   		}		
 	}
   	
   	$scope.returnDate = function() {
   		var jsDate = new Date($scope.formModel.dob);
   		var month;
   		var date;
   		
   		if(jsDate.getMonth() < 10) {
   			month = 0+""+jsDate.getMonth();
   		} else {
   			month = jsDate.getMonth();
   		}
   		
   		if(jsDate.getDate() < 10) {
   			date = 0+""+jsDate.getDate();
   		} else {
   			date = jsDate.getDate();
   		}
   		
   		var dateStr = month + "/" + date + "/" + jsDate.getFullYear() + " 00:00 AM";
   		console.log(dateStr)
   		return dateStr;
   	}
   	
   	$scope.valid = false;
   	
   	$scope.geocodeAddressToLatLng = function(address) {
		var geocoder = new google.maps.Geocoder;
		geocoder.geocode({'address': address}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
		    	$scope.add.latitude = results[0].geometry.location.lat();
		    	$scope.add.longitude = results[0].geometry.location.lng();
		    	if($scope.add.latitude != undefined || $scope.add.longitude != undefined) {
		    		$scope.valid = true;
		    	}
		    	$scope.onSubmit();
		    }
		});
	}
   	$scope.count = 1;
	$scope.onSubmit = function() {
		$scope.geocodeAddressToLatLng($scope.formModel.address+", "+$scope.formModel.city+", "+$scope.formModel.county);

		if($scope.add.latitude != undefined || $scope.add.longitude != undefined) {
			
			var resObjToSend = {};
			resObjToSend.username = $scope.formModel.username;
			resObjToSend.email = $scope.formModel.email;
			resObjToSend.password = $scope.formModel.password;
			resObjToSend.firstname = $scope.formModel.firstname;
			resObjToSend.lastname = $scope.formModel.lastname;
			resObjToSend.dob = $scope.returnDate();
			resObjToSend.address = $scope.formModel.address;
			resObjToSend.city = $scope.formModel.city;
			resObjToSend.council = $scope.formModel.council.value;
			resObjToSend.county = $scope.formModel.county;
			resObjToSend.phonenumber = $scope.formModel.phonenumber;
			resObjToSend.latitude = $scope.add.latitude;
			resObjToSend.longitude = $scope.add.longitude;
			console.log(resObjToSend);
			if($scope.valid = true && $scope.count == 1) {
				$http.post('http://localhost:8080/prasbic/api/createVolunteer',
						JSON.stringify(resObjToSend))
						.success(function(data, status, headers, config) {
							if (status == 200) {
								window.location = "http://localhost:8080/prasbic/";
							}
							$scope.count = $scope.count + 1;
						})
						.error(function(error) {
							alert("An error has occurred");
						});
				$scope.count = $scope.count + 1;
			}
			return;
		} else {
			$scope.geocodeAddressToLatLng($scope.formModel.address+", "+$scope.formModel.city+", "+$scope.formModel.county);
		}
	}
	
});