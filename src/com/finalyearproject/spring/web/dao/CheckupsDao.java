package com.finalyearproject.spring.web.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("checkupsDao")
public class CheckupsDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	public void saveOrUpdate(Checkup checkup) {
		session().saveOrUpdate(checkup);
	}

	@SuppressWarnings("unchecked")
	public List<Checkup> getAllCheckups() {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" FROM Checkup");
		queryBuilder.append(" WHERE isChecked= :value");
		Query query = session().createQuery(queryBuilder.toString());
		query = query.setParameter("value", false);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Checkup> getAllCheckedCheckups() {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append(" FROM Checkup");
		queryBuilder.append(" WHERE isChecked= :value");
		Query query = session().createQuery(queryBuilder.toString());
		query = query.setParameter("value", true);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Checkup> getCheckupsByUsername(String username) {
		Criteria crit = session().createCriteria(Checkup.class);
		crit.createAlias("resident", "r");
		crit.add(Restrictions.eq("isChecked", false));
		crit.add(Restrictions.eq("r.username", username));
		return crit.list();
	}

	public Checkup getCheckupByReportId(int idToUse) {
		Criteria crit = session().createCriteria(Checkup.class);
		crit.add(Restrictions.eq("report.id", idToUse));
		return (Checkup) crit.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<String> getVolunteersNames() {
		Criteria crit = session().createCriteria(Checkup.class);
		crit.add(Restrictions.eq("isChecked", true));
		crit.setProjection(Projections.projectionList()
				.add(Projections.property("volunteer.username")));
		return (ArrayList<String>) crit.list();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Checkup> getAllCheckedCheckupsByCouncil(String council) {
		Criteria crit = session().createCriteria(Checkup.class);
		crit.add(Restrictions.eq("isChecked", true));

		ArrayList<Checkup> checkups = (ArrayList<Checkup>) crit.list();
		ArrayList<Checkup> checkupsWithCheckedCouncils = new ArrayList<>();
		
		for(Checkup checkup: checkups) {
			if(checkup.getVolunteer().getCouncil().getCouncilname().equalsIgnoreCase(council)) {
				checkupsWithCheckedCouncils.add(checkup);
			}
		}
		return checkupsWithCheckedCouncils;
	}

}
