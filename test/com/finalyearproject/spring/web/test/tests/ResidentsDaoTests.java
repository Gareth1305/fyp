package com.finalyearproject.spring.web.test.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.finalyearproject.spring.web.dao.Council;
import com.finalyearproject.spring.web.dao.CouncilsDao;
import com.finalyearproject.spring.web.dao.Resident;
import com.finalyearproject.spring.web.dao.ResidentsDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = { "classpath:com/finalyearproject/spring/web/config/dao-context.xml",
		"classpath:com/finalyearproject/spring/web/config/security-context.xml",
		"classpath:com/finalyearproject/spring/web/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class ResidentsDaoTests {

	@Autowired
	private ResidentsDao residentsDao;

	@Autowired
	private CouncilsDao councilsDao;

	@Autowired
	private DataSource dataSource;

	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		jdbc.execute("delete from checkups");
		jdbc.execute("delete from reports");
		jdbc.execute("delete from residents");
		jdbc.execute("delete from volunteers");
		jdbc.execute("delete from councils");
		jdbc.execute("delete from users");
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testCreateResident() {

		Council council = new Council("Fingal County Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council);

		Date myDob = new Date();
		myDob.setYear(myDob.getYear() - 18);
		Resident resident = new Resident("JBloggs123", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council);

		assertEquals("Resident created but not saved.", 0, residentsDao.getAllResidents().size());
		residentsDao.create(resident);
		assertEquals("The resident has been saved so there should be one in the database", 1,
				residentsDao.getAllResidents().size());
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testRetrieveResident() {

		Council council = new Council("Fingal County Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council);

		Date myDob = new Date();
		myDob.setYear(myDob.getYear() - 18);
		Resident resident = new Resident("JBloggs123", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council);

		residentsDao.create(resident);

		Resident retrievedResident = residentsDao.getResidentByUsername("JBloggs123");
		assertNotNull("Check to see if the user exists in db", retrievedResident);
		
		Resident retrievedResident2 = residentsDao.getResidentByUsername("dummUsername");
		assertNull("Check to see if the user exists in db", retrievedResident2);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testPasswordEncryption() {
		Council council = new Council("Fingal County Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council);

		Date myDob = new Date();
		myDob.setYear(myDob.getYear() - 18);
		Resident resident = new Resident("JBloggs123", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council);		
		String password = resident.getPassword();
		residentsDao.create(resident);
		
		Resident retrievedResident = residentsDao.getResidentByUsername("JBloggs123");
		String retreivedPassword = retrievedResident.getPassword();
		assertNotEquals("The password should be encrypted", password, retreivedPassword);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testElderlyAndDisabledResidents() {
		Council council = new Council("Fingal County Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council);

		//Elderly Resident
		Date myDob1 = new Date();
		myDob1.setYear(myDob1.getYear() - 72);
		Resident resident1 = new Resident("JBloggs1", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob1, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council);		
		
		//Elderly Resident
		Date myDob2 = new Date();
		myDob2.setYear(myDob2.getYear() - 75);
		Resident resident2 = new Resident("JBloggs2", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob2, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council);	
		
		//Normal Resident
		Date myDob3 = new Date();
		myDob3.setYear(myDob3.getYear() - 22);
		Resident resident3 = new Resident("JBloggs3", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob3, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council);	
		
		//Disabled Resident
		Date myDob4 = new Date();
		myDob4.setYear(myDob4.getYear() - 19);
		Resident resident4 = new Resident("JBloggs4", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob4, "0892052140", "Father", "0892220928", true,
				53.461638, -6.242750, council);	
		
		//Normal Resident
		Date myDob5 = new Date();
		myDob5.setYear(myDob5.getYear() - 25);
		Resident resident5 = new Resident("JBloggs5", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob5, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council);	
		
		residentsDao.create(resident1);
		residentsDao.create(resident2);
		residentsDao.create(resident3);
		residentsDao.create(resident4);
		residentsDao.create(resident5);
		
		assertEquals("Five residents have been created", 5, residentsDao.getAllResidents().size());
		
		assertEquals("Expecting a value of 3.", 3, residentsDao.getElderlyAndDisabledResidents().size());
		
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testResidentsByCouncil() {
		Council council1 = new Council("Fingal County Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council1);
		Council council2 = new Council("Dublin City Council", "(0)1 222 2222", "customerservices@dublincity.ie",
				"Civic Offices, Wood Quay, Dublin 8");
		councilsDao.saveOrUpdate(council2);

		//Elderly Resident
		Date myDob1 = new Date();
		myDob1.setYear(myDob1.getYear() - 72);
		Resident resident1 = new Resident("JBloggs6", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob1, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council1);		
		
		//Elderly Resident
		Date myDob2 = new Date();
		myDob2.setYear(myDob2.getYear() - 75);
		Resident resident2 = new Resident("JBloggs7", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob2, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council1);	
		
		//Normal Resident
		Date myDob3 = new Date();
		myDob3.setYear(myDob3.getYear() - 22);
		Resident resident3 = new Resident("JBloggs8", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob3, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council1);	
		
		//Disabled Resident
		Date myDob4 = new Date();
		myDob4.setYear(myDob4.getYear() - 19);
		Resident resident4 = new Resident("JBloggs9", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob4, "0892052140", "Father", "0892220928", true,
				53.461638, -6.242750, council2);	
		
		//Normal Resident
		Date myDob5 = new Date();
		myDob5.setYear(myDob5.getYear() - 25);
		Resident resident5 = new Resident("JBloggs10", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob5, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council2);	
		
		residentsDao.create(resident1);
		residentsDao.create(resident2);
		residentsDao.create(resident3);
		residentsDao.create(resident4);
		residentsDao.create(resident5);
		
		assertEquals("Five residents have been created", 5, residentsDao.getAllResidents().size());
		
		assertEquals("Expecting a value of 3.", 3, residentsDao.getResidentsByCouncilName("Fingal County Council").size());
		assertEquals("Expecting a value of 2.", 2, residentsDao.getResidentsByCouncilName("Dublin City Council").size());
		
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testUniqueUsername() {
		Council council = new Council("Fingal County Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council);

		//Elderly Resident
		Date myDob1 = new Date();
		myDob1.setYear(myDob1.getYear() - 72);
		Resident resident = new Resident("JBloggs11", "Joe1994", "Joe@gmail.com", true, "ROLE_USER", "Joe", "Bloggs",
				"16 Swords Manor Drive", "Swords", "Dublin", myDob1, "0892052140", "Father", "0892220928", false,
				53.461638, -6.242750, council);	
		residentsDao.create(resident);
		
		assertFalse("Paul12345 should be a unique username", residentsDao.doesUsernameExist("Paul12345"));
		assertTrue("JBloggs11 should already exist", residentsDao.doesUsernameExist("JBloggs11"));
	}
}
