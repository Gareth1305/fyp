<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<script
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9z1qJLw1fTEoHn1_JxuggptNZFLOz56o&callback=initMap"></script>
<script src="${pageContext.request.contextPath}/static/script/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/static/script/statisticalviewvolres/statisticalviewvolres.js"></script>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/pie.js"></script>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
<link href="${pageContext.request.contextPath}/static/css/statisticalviewvolres/statisticalviewvolres.css" rel="stylesheet" media="screen"/> 

<sec:authentication property="principal" var="principal"/>
<div class="divContainer" ng-app="statsVolResView" ng-controller="statsVolResCtrl"
	ng-model = "username" ng-init = "username='${principal.username}'">
	<div ng-init="getUserInfo()" class="contentContainer">
		<div class="titleDiv2" style="text-align:center;">
			<h2><u>Statistics For Your Area</u></h2>
		</div>
		<div class="titleDiv" style="text-align:center;">
			<!--<h4>This page allows volunteers to view statistical information about their area. The aim of this page is to
			provide the volunteer with a better insight into when he/she will be needed most. Each graph provides a unique
			set of information to the volunteer.</h4>-->
		</div>
		<div class="userInfoContainer">
			<div class="userChartsContainer">
				<div class="chart1Container chartContainer">
					<div id="chartdiv1" style="width: 100%; height: 100%; background-color: #FFFFFF;" ></div>
				</div>
				<div class="chart2Container chartContainer">
					<div id="chartdiv2" style="width: 100%; height: 100%; background-color: #FFFFFF;" ></div>
				</div>
			</div>
		</div>
		<div class="userInfoContainer secondInfoContainer">
			<div class="userChartsContainer">
				<div class="chart3Container">
					<div id="chartdiv3" style="width: 100%; height: 100%; background-color: #FFFFFF;"></div>
				</div>
				<div class="chart4Container">
					<div id="chartdiv4" style="width: 100%; height: 100%; background-color: #FFFFFF;"></div>
				</div>
			</div>
		</div>
	</div>
</div>