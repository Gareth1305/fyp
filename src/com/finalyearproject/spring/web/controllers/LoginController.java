package com.finalyearproject.spring.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.finalyearproject.spring.web.dao.Resident;
import com.finalyearproject.spring.web.service.ResidentsService;

@Controller
public class LoginController {

	private ResidentsService residentsService;

	@Autowired
	public void setResidentsService(ResidentsService residentsService) {
		this.residentsService = residentsService;
	}

	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}

	@RequestMapping("/denied")
	public String showDenied() {
		return "denied";
	}

	@RequestMapping("/loggedout")
	public String showLoggedOut() {
		return "/";
	}
	
	@RequestMapping(value = "/api/checklogininformation/{username}/{password}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String checkLoginInformation(@PathVariable String username, @PathVariable String password) {
		System.out.println("I AM HIT");
		Resident resident =  residentsService.getResidentByUsername(username, password);
		if(resident != null) {
			return resident.getEmail();
		}
		return "FAILURE";
	}
}
