var myApp = angular.module("statsView", []);

myApp.controller('statsCtrl', ['$scope', '$http', function($scope, $http) {
	
	$scope.councilInformation = [];
	$scope.userInformation = [];
	$scope.reportInformation = [];
	$scope.theftReports = []
	$scope.burglaryReports = []
	$scope.alcoholAbuseReports = []
	$scope.drugAbuseReports = []
	$scope.joyRidingReports = []
	$scope.fightingReports = []
	$scope.vandelismReports = []
	$scope.otherReports = []	
	
	$scope.allUsers = {};
	$scope.numOfUsers = 0;
	$scope.numOfVol = 0;	
	$scope.numOfRes = 0;
	
	$scope.numOfElderlyAndDisabled = 0;
	$scope.numOfElderlyResidents = 0;
	$scope.numOfDisabledResidents = 0;
	$scope.numOfNormalResidents = 0;
	
	$scope.councilInit = "All";
	
	
	
	$scope.getAllInfo = function() {
		$http.get('http://localhost:8080/prasbic/api/getAllCouncils').success(function (councils) {
			$scope.councilInformation = councils;
		});
	};
	
	$scope.allClicked = function() {
		$scope.userStatsAndPieForAll();	
		$scope.initActiveResidents();
		$scope.initActiveVolunteers();
		$scope.initAllReports();
		$scope.initReportCategories();
		$scope.initChartForBusiestTimes();
		$scope.initChartToViewAllReports();
	}
	
	$scope.councilClicked = function(council) {
		$scope.userStatsAndPieForCouncil(council);
		$scope.initActiveCouncilResidents(council);
		$scope.initActiveCouncilVolunteers(council);
		$scope.initCouncilReports(council);
		$scope.initCouncilReportCategories(council);
		$scope.initChartForBusiestTimesWithCouncils(council);
		$scope.initChartToViewAgeGroupsOfReporteesOnBusiestDay(council);
	};
	
	$scope.getAllUsers = function() {
		$scope.initAllUsers();
		$scope.initActiveResidents();
		$scope.initActiveVolunteers();
		$scope.initAllReports();
		$scope.initReportCategories();
		$scope.initChartForBusiestTimes();
		$scope.initChartToViewAllReports();
	}
	
	$scope.allMyRes = [];
	$scope.allMyVol = [];
	$scope.busiestHourReports = [];

	$scope.initChartToViewAllReports = function() {
		var providerObjArray = [];
		$http.get('http://localhost:8080/prasbic/api/getBusiestCouncils').success(function (councilsCountHash) {

			for(var key in councilsCountHash) {
				var providerObj = {};
				providerObj.category = key;
				providerObj.column_1 = councilsCountHash[key];
				providerObjArray.push(providerObj);
			}
			$scope.makeLineGraphForYearlyReports("Most Reported Councils", providerObjArray);
		});
		
	}
	
	$scope.exportCharts = function() {
		console.log("CLICKED")
		var images = [];
	    var pending = AmCharts.charts.length;
	    console.log(pending);
	    for ( var i = 0; i < AmCharts.charts.length; i++ ) {
	        var chart = AmCharts.charts[ i ];
	        chart.export.capture( {}, function() {
	          this.toJPG( {}, function( data ) {
	            images.push( {
	              "image": data,
	              "fit": [ 523.28, 769.89 ]
	            } );
	            pending--;
	            if ( pending === 0 ) {
	              // all done - construct PDF
	              chart.export.toPDF( {
	                content: images
	              }, function( data ) {
	                this.download( data, "application/pdf", "amCharts.pdf" );
	              } );
	            }
	          } );
	        } );
	      }
	}
	
	$scope.initChartToViewAgeGroupsOfReporteesOnBusiestDay = function(council) {
		var providerObjArray = [];
		var providerObj = {};
		providerObj.underEighteen = 0;
		providerObj.eigthenToTwentyFive = 0;
		providerObj.twentySixToFourty = 0;
		providerObj.FourtyOneToFiftyFive = 0;
		providerObj.fiftySixToSeventy = 0;
		providerObj.seventyOnePlus = 0;

		$http.get('http://localhost:8080/prasbic/api/getAllReportsByCouncil/' + council.councilname).success(function (reports) {
			reports.forEach(function(report) {
				console.log(report.user);
				var userDOB = new Date(report.user.dob);
				var userDOBYear = userDOB.getFullYear();
				var todaysDate = new Date();
				var todaysYear = todaysDate.getFullYear();
				var userAge = todaysYear - userDOBYear;
				if(userAge < 18) {
					providerObj.underEighteen = providerObj.underEighteen + 1;
				} else if(userAge >= 18 && userAge <= 25) {
					providerObj.eigthenToTwentyFive = providerObj.eigthenToTwentyFive + 1;
				} else if(userAge >= 26 && userAge <= 40) {
					providerObj.twentySixToFourty = providerObj.twentySixToFourty + 1;
				} else if(userAge >= 41 && userAge <= 55) {
					providerObj.FourtyOneToFiftyFive = providerObj.FourtyOneToFiftyFive + 1;
				} else if(userAge >= 56 && userAge <= 70) {
					providerObj.fiftySixToSeventy = providerObj.fiftySixToSeventy + 1;
				} else if(userAge >= 71) {
					providerObj.seventyOnePlus = providerObj.seventyOnePlus + 1;
				}
			});
			for(var key in providerObj) {
				var myObj = {};
				if(key == "underEighteen") {
					myObj.category = "<18";
				} else if(key == "eigthenToTwentyFive") {
					myObj.category = "18-25";
				} else if(key == "twentySixToFourty") {
					myObj.category = "26-40";
				} else if(key == "FourtyOneToFiftyFive") {
					myObj.category = "41-55";
				} else if(key == "fiftySixToSeventy") {
					myObj.category = "56-70";
				} else if(key == "seventyOnePlus") {
					myObj.category = "71+";
				}
				myObj.column_1 = providerObj[key];
				providerObjArray.push(myObj);
			}
			$scope.makeLineGraphForYearlyReports("Busiest Day Reportee Ages", providerObjArray);
		});	
	}
	
	$scope.makeLineGraphForYearlyReports = function(graphTitle, dataToSet) {
		AmCharts.makeChart("chartdiv5",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"categoryAxis": {
						"gridPosition": "start"
					},
					"export": {
					    "enabled": true,
					    "libs": {
					      "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
					    },
					    "menu": []
					  },
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[value]]",
							"bullet": "round",
							"id": "AmGraph-1",
							"title": "Most Reported Councils",
							"valueField": "column_1"
						},
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"stackType": "regular",
							"title": "No Of Reports"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": graphTitle
						}
					],
					"dataProvider": dataToSet
				}
			);
	}
	
	$scope.initChartForBusiestTimesWithCouncils = function(council) {
		$http.get('http://localhost:8080/prasbic/api/getBusiestHoursWithCouncil/'+council.councilname).success(function (busiestTimesReports) {
			var anObj = {
					"oneAM" : 0,"twoAM" : 0,"threeAM" : 0,"fourAM" : 0,"fiveAM" : 0,"sixAM" : 0,
					"sevenAM" : 0,"eightAM" : 0,"nineAM" : 0,"tenAM" : 0,"elevenAM" : 0,"twelvePM" : 0,
					"onePM" : 0,"twoPM" : 0,"threePM" : 0,"fourPM" : 0,"fivePM" : 0,"sixPM" : 0,
					"sevenPM" : 0,"eightPM" : 0,"ninePM" : 0,"tenPM" : 0,"elevenPM" : 0
			};
			
			busiestTimesReports.forEach(function(rep) {
				$scope.busiestHourReports.push(rep);
				var myDate = new Date(rep.date);
				switch (myDate.getHours()) {
				    case 1: anObj.oneAM = anObj.oneAM + 1; break;
				    case 2: anObj.twoAM = anObj.twoAM + 1; break;
				    case 3: anObj.threeAM = anObj.threeAM + 1; break;
				    case 4: anObj.fourAM = anObj.fourAM + 1; break;
				    case 5: anObj.fiveAM = anObj.fiveAM + 1; break;
				    case 6: anObj.sixAM = anObj.sixAM + 1; break;
				    case 7: anObj.sevenAM = anObj.sevenAM + 1; break;
				    case 8: anObj.eightAM = anObj.eightAM + 1; break;
				    case 9: anObj.nineAM = anObj.nineAM + 1; break;
				    case 10: anObj.tenAM = anObj.tenAM + 1; break;
				    case 11: anObj.elevenAM = anObj.elevenAM + 1; break;
				    case 12: anObj.twelvePM = anObj.twelvePM + 1; break;
				    case 13: anObj.onePM = anObj.onePM + 1; break;
				    case 14: anObj.twoPM = anObj.twoPM + 1; break;
				    case 15: anObj.threePM = anObj.threePM + 1; break;
				    case 16: anObj.fourPM = anObj.fourPM + 1; break;
				    case 17: anObj.fivePM = anObj.fivePM + 1; break;
				    case 18: anObj.sixPM = anObj.sixPM + 1; break;
				    case 19: anObj.sevenPM = anObj.sevenPM + 1; break;
				    case 20: anObj.eightPM = anObj.eightPM + 1; break;
				    case 21: anObj.ninePM = anObj.ninePM + 1; break;
				    case 22: anObj.tenPM = anObj.tenPM + 1; break;
				    case 23: anObj.elevenPM = anObj.elevenPM + 1; break;
				}
				
			});
			$scope.makePieChartForBusiestTimes(anObj);
		});
	}
	
	$scope.initChartForBusiestTimes = function() {
		$http.get('http://localhost:8080/prasbic/api/getBusiestHours').success(function (busiestTimesReports) {
			
			var anObj = {
					"oneAM" : 0,"twoAM" : 0,"threeAM" : 0,"fourAM" : 0,"fiveAM" : 0,"sixAM" : 0,
					"sevenAM" : 0,"eightAM" : 0,"nineAM" : 0,"tenAM" : 0,"elevenAM" : 0,"twelvePM" : 0,
					"onePM" : 0,"twoPM" : 0,"threePM" : 0,"fourPM" : 0,"fivePM" : 0,"sixPM" : 0,
					"sevenPM" : 0,"eightPM" : 0,"ninePM" : 0,"tenPM" : 0,"elevenPM" : 0
			};
			
			busiestTimesReports.forEach(function(rep) {
				$scope.busiestHourReports.push(rep);
				var myDate = new Date(rep.date);
				switch (myDate.getHours()) {
				    case 1: anObj.oneAM = anObj.oneAM + 1; break;
				    case 2: anObj.twoAM = anObj.twoAM + 1; break;
				    case 3: anObj.threeAM = anObj.threeAM + 1; break;
				    case 4: anObj.fourAM = anObj.fourAM + 1; break;
				    case 5: anObj.fiveAM = anObj.fiveAM + 1; break;
				    case 6: anObj.sixAM = anObj.sixAM + 1; break;
				    case 7: anObj.sevenAM = anObj.sevenAM + 1; break;
				    case 8: anObj.eightAM = anObj.eightAM + 1; break;
				    case 9: anObj.nineAM = anObj.nineAM + 1; break;
				    case 10: anObj.tenAM = anObj.tenAM + 1; break;
				    case 11: anObj.elevenAM = anObj.elevenAM + 1; break;
				    case 12: anObj.twelvePM = anObj.twelvePM + 1; break;
				    case 13: anObj.onePM = anObj.onePM + 1; break;
				    case 14: anObj.twoPM = anObj.twoPM + 1; break;
				    case 15: anObj.threePM = anObj.threePM + 1; break;
				    case 16: anObj.fourPM = anObj.fourPM + 1; break;
				    case 17: anObj.fivePM = anObj.fivePM + 1; break;
				    case 18: anObj.sixPM = anObj.sixPM + 1; break;
				    case 19: anObj.sevenPM = anObj.sevenPM + 1; break;
				    case 20: anObj.eightPM = anObj.eightPM + 1; break;
				    case 21: anObj.ninePM = anObj.ninePM + 1; break;
				    case 22: anObj.tenPM = anObj.tenPM + 1; break;
				    case 23: anObj.elevenPM = anObj.elevenPM + 1; break;
				}
				
			});
			$scope.makePieChartForBusiestTimes(anObj);
		});
	}
	
	$scope.doesContain = function(currUser, listOfTypeOfUser) {
		var isAlreadyThere = false;
		for(var i = 0; i < listOfTypeOfUser.length; i++) {
			if(listOfTypeOfUser[i].name == currUser) {
				isAlreadyThere = true;
				listOfTypeOfUser[i].value = listOfTypeOfUser[i].value + 1;
				break;
			}
		}
		if(isAlreadyThere) {
			return true;
		} else {
			return false;
		}
	}
	
	$scope.initActiveCouncilResidents = function(council) {
		$http.get('http://localhost:8080/prasbic/api/getAllReportsByCouncil/' + council.councilname).success(function (users) {
			var usersToSendToFunc = [];
			users.forEach(function(user) {
				usersToSendToFunc.push(user.username);
			});
			$scope.allMyRes = [];
			usersToSendToFunc.forEach(function(user) {
				var testuser = {};
				if($scope.allMyRes.length == 0) {
					testuser.name = user;
					testuser.value = 1;
					$scope.allMyRes.push(testuser);
				} else if(!$scope.doesContain(user, $scope.allMyRes)) {
					testuser.name = user;
					testuser.value = 1;
					$scope.allMyRes.push(testuser);
				}				
			});
		});
	}
	
	$scope.initCouncilReportCategories = function(council) {
		
		var reportCatObj = {};
		var bVal = 0; var fVal = 0; var tVal = 0; var vVal = 0; var oVal = 0; var aVal = 0; var dVal = 0; var jVal = 0;
		
		$scope.reportInformation.forEach(function(report) {
			if(report.councilname == council.councilname) {
				switch (report.category) {
			    case "Other":
			    	oVal = oVal + 1;
			        break;
			    case "Burglary":
			    	bVal = bVal + 1;
			        break;
			    case "Theft":
			    	tVal = tVal + 1;
			        break;
			    case "Fighting":
			    	fVal = fVal + 1;
			        break;
			    case "Alcohol Abuse":
			    	aVal = aVal + 1;
			        break;
			    case "Drug Abuse":
			    	dVal = dVal + 1;
			        break;
			    case "Joy Riding":
			    	jVal = jVal + 1;
			        break;
			    case "Vandalism":
			    	vVal = vVal + 1;
			        break;
				}
			}
		});
		reportCatObj.oVal=oVal;
		reportCatObj.tVal=tVal;
		reportCatObj.bVal=bVal;
		reportCatObj.vVal=vVal;
		reportCatObj.aVal=aVal;
		reportCatObj.dVal=dVal;
		reportCatObj.fVal=fVal;
		reportCatObj.jVal=jVal;
		$scope.makeReportChart2(reportCatObj);
		
	};
	
	$scope.initReportCategories = function() {
		var reportCatObj = {};
		var bVal = 0; var fVal = 0; var tVal = 0; var vVal = 0; var oVal = 0; var aVal = 0; var dVal = 0; var jVal = 0;
		
		$http.get('http://localhost:8080/prasbic/api/getAllReports').success(function (reports) {
			reports.forEach(function(re) {
				switch (re.category) {
			    case "Other":
			    	oVal = oVal + 1;
			        break;
			    case "Burglary":
			    	bVal = bVal + 1;
			        break;
			    case "Theft":
			    	tVal = tVal + 1;
			        break;
			    case "Fighting":
			    	fVal = fVal + 1;
			        break;
			    case "Alcohol Abuse":
			    	aVal = aVal + 1;
			        break;
			    case "Drug Abuse":
			    	dVal = dVal + 1;
			        break;
			    case "Joy Riding":
			    	jVal = jVal + 1;
			        break;
			    case "Vandalism":
			    	vVal = vVal + 1;
			        break;
				}
				
			})
			reportCatObj.oVal=oVal;
			reportCatObj.tVal=tVal;
			reportCatObj.bVal=bVal;
			reportCatObj.vVal=vVal;
			reportCatObj.aVal=aVal;
			reportCatObj.dVal=dVal;
			reportCatObj.fVal=fVal;
			reportCatObj.jVal=jVal;
			$scope.makeReportChart2(reportCatObj);
		});
	}
	
	$scope.initCouncilReports = function(council) {
		var dayValuesObj = {};
		var monVal = 0;
		var tueVal = 0;
		var wedVal = 0;
		var thursVal = 0;
		var friVal = 0;
		var satVal = 0;
		var sunVal = 0;
		
		$scope.reportInformation.forEach(function(report) {
			if(report.councilname == council.councilname) {
				var dateStr = JSON.parse(report.date);  			        
				var date = new Date(dateStr);
				
				switch (date.getDay()) {
			    case 0:
			        sunVal = sunVal + 1;
			        break;
			    case 1:
			    	monVal = monVal + 1;
			        break;
			    case 2:
			    	tueVal = tueVal + 1;
			        break;
			    case 3:
			    	wedVal = wedVal + 1;
			        break;
			    case 4:
			    	thursVal = thursVal + 1;
			        break;
			    case 5:
			    	friVal = friVal + 1;
			        break;
			    case 6:
			    	satVal = satVal + 1;
			        break;
				}
			}
		});
		dayValuesObj.monVal = monVal;
		dayValuesObj.tueVal = tueVal;
		dayValuesObj.wedVal = wedVal;
		dayValuesObj.thursVal = thursVal;
		dayValuesObj.friVal = friVal;
		dayValuesObj.satVal = satVal;
		dayValuesObj.sunVal = sunVal;
		
		$scope.makeReportChart1(dayValuesObj);
	}
	
	$scope.initAllReports = function() {
		$scope.reportInformation = [];
		var dayValuesObj = {};
		var monVal = 0;
		var tueVal = 0;
		var wedVal = 0;
		var thursVal = 0;
		var friVal = 0;
		var satVal = 0;
		var sunVal = 0;
		
		$http.get('http://localhost:8080/prasbic/api/getAllReports').success(function (reports) {
			$scope.reportInformation = reports;
			reports.forEach(function(report) {
				var dateStr = JSON.parse(report.date);  			        
				var date = new Date(dateStr);
				
				switch (date.getDay()) {
			    case 0:
			        sunVal = sunVal + 1;
			        break;
			    case 1:
			    	monVal = monVal + 1;
			        break;
			    case 2:
			    	tueVal = tueVal + 1;
			        break;
			    case 3:
			    	wedVal = wedVal + 1;
			        break;
			    case 4:
			    	thursVal = thursVal + 1;
			        break;
			    case 5:
			    	friVal = friVal + 1;
			        break;
			    case 6:
			    	satVal = satVal + 1;
			        break;
				}
			});
			dayValuesObj.monVal = monVal;
			dayValuesObj.tueVal = tueVal;
			dayValuesObj.wedVal = wedVal;
			dayValuesObj.thursVal = thursVal;
			dayValuesObj.friVal = friVal;
			dayValuesObj.satVal = satVal;
			dayValuesObj.sunVal = sunVal;
			
			$scope.makeReportChart1(dayValuesObj);
		});	
	}
	
	$scope.initActiveCouncilVolunteers = function(council) {
		$http.get('http://localhost:8080/prasbic/api/getAllCheckedCheckupsByCouncil/'+council.councilname).success(function (users) {
			var usersToSendToFunc2 = [];
			users.forEach(function(user) {
				usersToSendToFunc2.push(user.volunteer.username);
			});
			$scope.allMyVol = [];
			usersToSendToFunc2.forEach(function(user) {
				var testuser = {};
				if($scope.allMyVol.length == 0) {
					testuser.name = user;
					testuser.value = 1;
					$scope.allMyVol.push(testuser);
				} else if(!$scope.doesContain(user, $scope.allMyVol)) {
					testuser.name = user;
					testuser.value = 1;
					$scope.allMyVol.push(testuser);
				}				
			});
		});
	}
	
	$scope.initActiveResidents = function() {
		$http.get('http://localhost:8080/prasbic/api/getAllResidentsFromReports').success(function (users) {
			$scope.allMyRes = [];
			users.forEach(function(user) {
				var testuser = {};
				if($scope.allMyRes.length == 0) {
					testuser.name = user;
					testuser.value = 1;
					$scope.allMyRes.push(testuser);
				} else if(!$scope.doesContain(user, $scope.allMyRes)) {
					testuser.name = user;
					testuser.value = 1;
					$scope.allMyRes.push(testuser);
				}				
			});
		});
	}
	
	$scope.initActiveVolunteers = function() {
		$http.get('http://localhost:8080/prasbic/api/getAllVolunteersFromCheckups').success(function (users) {
			users.forEach(function(user) {
				var testuser = {};
				if($scope.allMyVol.length == 0) {
					testuser.name = user;
					testuser.value = 1;
					$scope.allMyVol.push(testuser);
				} else if(!$scope.doesContain(user, $scope.allMyVol)) {
					testuser.name = user;
					testuser.value = 1;
					$scope.allMyVol.push(testuser);
				}				
			});
		});
	}

	
	$scope.makeReportChart1 = function(dayValues) {
		AmCharts.makeChart("chartdiv",
				{
					"type": "serial",
					"categoryField": "category",
					"export": {
					    "enabled": true,
					    "libs": {
					      "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
					    },
					    "menu": []
					  },
					"marginLeft": 19,
					"marginTop": 21,
					"startDuration": 1,
					"processCount": 999,
					"theme": "light",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"colorField": "color",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"lineColorField": "color",
							"title": "1 Busiest Report Times",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "No Of Repors"
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [
						{
							"id": "Title-1",
							"size": 12,
							"text": "Busiest Report Days (Overall)"
						}
					],
					"dataProvider": [
						{
							"category": "Mon",
							"column-1": dayValues.monVal
						},
						{
							"category": "Tue",
							"column-1": dayValues.tueVal
						},
						{
							"category": "Wed",
							"column-1": dayValues.wedVal
						},
						{
							"category": "Thu",
							"column-1": dayValues.thursVal
						},
						{
							"category": "Fri",
							"column-1": dayValues.friVal
						},
						{
							"category": "Sat",
							"column-1": dayValues.satVal
						},
						{
							"category": "Sun",
							"column-1": dayValues.sunVal
						}
					]
				}
			);
	}
	
	$scope.makePieChartAllUsers = function(resNum, volNum) {
		AmCharts.makeChart("chartdiv1",
			{
				"type": "pie",
				"angle": 36,
				"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
				"depth3D": 18,
				"innerRadius": -3,
				"labelRadius": 21,
				"colors": [
					"#0071ff",
					"#ff0f00"
				],
				"innerRadius": 10,
				"titleField": "category",
				"valueField": "column-1",
				"allLabels": [],
				"balloon": {},
				"pathToImages": "http://www.amcharts.com/lib/3/images/",
				"legend": {
					"enabled": true,
					"align": "center",
					"markerType": "circle"
				},
				"titles": [
							{
								"id": "Title-2",
								"size": 12,
								"text": "Type of Users"
							}
						],
				"dataProvider": [
					{
						"category": "Volunteers",
						"column-1": volNum
					},
					{
						"category": "Residents",
						"column-1": resNum
					}
				],
				"export": {
				    "enabled": true,
				    "libs": {
				      "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
				    },
				    "menu": []
				  }
			}
		);
	};
	
	$scope.makeReportChart2 = function(reportCat) {
		AmCharts.makeChart("chartdiv3",
				{
					"type": "pie",
					"angle": 29.7,
					"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
					"depth3D": 15,
					"innerRadius": "40%",
					"titleField": "category",
					"valueField": "column-1",
					"processTimeout": -1,
					"theme": "default",
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"align": "center",
						"markerType": "circle"
					},
					"titles": [
								{
									"id": "Title-2",
									"size": 12,
									"text": "Report Categories"
								}
							],
					"dataProvider": [
						{
							"category": "Burglary",
							"column-1": reportCat.bVal
						},
						{
							"category": "Fighting",
							"column-1": reportCat.fVal
						},
						{
							"category": "Drug Abuse",
							"column-1": reportCat.dVal
						},
						{
							"category": "Alcohol Abuse",
							"column-1": reportCat.aVal
						},
						{
							"category": "Theft",
							"column-1": reportCat.tVal
						},
						{
							"category": "Vandalism",
							"column-1": reportCat.vVal
						},
						{
							"category": "Other",
							"column-1": reportCat.oVal
						},
						{
							"category": "Joy Riding",
							"column-1": reportCat.jVal
						}
					], 
					"export": {
					    "enabled": true,
					    "libs": {
					      "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
					    },
					    "menu": []
					  }
				}
			);
	};
	
	$scope.makePieChartTypeOfResidents = function(elderlyNum, disabledNum, bothNum, normalNum) {
		AmCharts.makeChart("chartdiv2",
			{
				"type": "pie",
				"angle": 36,
				"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
				"depth3D": 18,
				"innerRadius": -3,
				"labelRadius": 21,
				"colors": [
				    "#ff0f00", 
					"#0071ff",
					"#00FF00", 
					"#FFFF00"
				],
				"innerRadius": 10,
				"titleField": "category",
				"valueField": "column-1",
				"allLabels": [],
				"balloon": {},
				"legend": {
					"enabled": true,
					"align": "center",
					"markerType": "circle"
				},
				"titles": [
							{
								"id": "Title-2",
								"size": 12,
								"text": "Type of Residents"
							}
						],
				"dataProvider": [
					{
						"category": "Elderly Res",
						"column-1": elderlyNum
					},
					{
						"category": "Disabled Res",
						"column-1": disabledNum
					},
					{
						"category": "Elderly & Disabled Res",
						"column-1": bothNum
					},
					{
						"category": "Normal Res",
						"column-1": normalNum
					}
				], 
				"export": {
				    "enabled": true,
				    "libs": {
				      "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
				    },
				    "menu": []
				  }
			}
		);
	};

	$scope.userStatsAndPieForAll = function() {
		$scope.userInformation = [];
		$scope.councilInit = "All";
			
		var numOfResidents = 0;
		var numOfVolunteers = 0;
		var numOfUsers = 0;
		
		var numOfElderlyResidents = 0;
		var numOfDisabledResidents = 0;
		var numOfDisabledAndElderlyResidents = 0;
		var numOfNormalResidents = 0;
		
		$scope.allUsers.forEach(function(user) {
			if(user.authority == "ROLE_USER") {
				var disabledOrElderly = false;
				var dob = new Date(user.dob);
				var currDate = new Date();
				if(user.disability == true && (dob < currDate.getFullYear() - 70)) {
					numOfDisabledAndElderlyResidents = numOfDisabledAndElderlyResidents + 1;
				} else {
					if(user.disability == true) {
						disabledOrElderly = true;
						numOfDisabledResidents = numOfDisabledResidents + 1;
					}
					var dob = new Date(user.dob);
					var currDate = new Date();
					if(dob < currDate.getFullYear() - 70) {
						disabledOrElderly = true;
						numOfElderlyResidents = numOfElderlyResidents + 1;
					}
					if(disabledOrElderly == false) {
						numOfNormalResidents = numOfNormalResidents + 1;
					}
				}
				numOfResidents = numOfResidents + 1;
			} else if(user.authority == "ROLE_VOLUNTEER") {
				numOfVolunteers = numOfVolunteers + 1;
			}
			numOfUsers = numOfUsers + 1;
			$scope.userInformation.push(user);
		})
		
		$scope.numOfRes = numOfResidents;
		$scope.numOfVol = numOfVolunteers;
		$scope.numOfUsers = numOfUsers;
		
		$scope.numOfElderlyAndDisabled = numOfDisabledAndElderlyResidents;
		$scope.numOfElderlyResidents = numOfElderlyResidents;
		$scope.numOfDisabledResidents = numOfDisabledResidents;
		$scope.numOfNormalResidents = numOfNormalResidents;
		
		$scope.numOfUsers = $scope.userInformation.length -1;
		$scope.makePieChartTypeOfResidents($scope.numOfElderlyResidents, $scope.numOfDisabledResidents, $scope.numOfElderlyAndDisabled, $scope.numOfNormalResidents);
		$scope.makePieChartAllUsers($scope.numOfRes, $scope.numOfVol);
	}
	
	$scope.userStatsAndPieForCouncil = function(council) {
		$scope.userInformation = [];
		$scope.councilInit = council.councilname;
			
		var numOfResidents = 0;
		var numOfVolunteers = 0;
		var numOfUsers = 0;
		
		var numOfElderlyResidents = 0;
		var numOfDisabledResidents = 0;
		var numOfDisabledAndElderlyResidents = 0;
		var numOfNormalResidents = 0;
		
		$scope.allUsers.forEach(function(user) {
			if(user.council != undefined) {
				if(user.council.councilname == $scope.councilInit) {
					if(user.authority == "ROLE_USER") {
						var disabledOrElderly = false;
						var dob = new Date(user.dob);
						var currDate = new Date();
						if(user.disability == true && (dob < currDate.getFullYear() - 70)) {
							numOfDisabledAndElderlyResidents = numOfDisabledAndElderlyResidents + 1;
						} else {
							if(user.disability == true) {
								disabledOrElderly = true;
								numOfDisabledResidents = numOfDisabledResidents + 1;
							}
							var dob = new Date(user.dob);
							var currDate = new Date();
							if(dob < currDate.getFullYear() - 70) {
								disabledOrElderly = true;
								numOfElderlyResidents = numOfElderlyResidents + 1;
							}
							if(disabledOrElderly == false) {
								numOfNormalResidents = numOfNormalResidents + 1;
							}
						}
						numOfResidents = numOfResidents + 1;
					} else if(user.authority == "ROLE_VOLUNTEER") {
						numOfVolunteers = numOfVolunteers + 1;
					}
					numOfUsers = numOfUsers + 1;
					$scope.userInformation.push(user);
				}
			}
		})
		$scope.numOfRes = numOfResidents;
		$scope.numOfVol = numOfVolunteers;
		$scope.numOfUsers = numOfUsers;
		
		$scope.numOfElderlyAndDisabled = numOfDisabledAndElderlyResidents;
		$scope.numOfElderlyResidents = numOfElderlyResidents;
		$scope.numOfDisabledResidents = numOfDisabledResidents;
		$scope.numOfNormalResidents = numOfNormalResidents;
		
		$scope.numOfUsers = $scope.userInformation.length -1;
		$scope.makePieChartTypeOfResidents($scope.numOfElderlyResidents, $scope.numOfDisabledResidents, $scope.numOfElderlyAndDisabled, $scope.numOfNormalResidents);
		$scope.makePieChartAllUsers($scope.numOfRes, $scope.numOfVol);
	}
	
	$scope.initAllUsers = function() {
		$http.get('http://localhost:8080/prasbic/api/getAllUsers').success(function (users) {
			$scope.allUsers = users;
			var numOfElderlyResidents = 0;
			var numOfDisabledResidents = 0;
			var numOfDisabledAndElderlyResidents = 0;
			var numOfNormalResidents = 0;
			
			users.forEach(function(user) {
				if(user.authority == "ROLE_USER") {
					var disabledOrElderly = false;
					var dob = new Date(user.dob);
					var currDate = new Date();
					if(user.disability == true && (dob < currDate.getFullYear() - 70)) {
						numOfDisabledAndElderlyResidents = numOfDisabledAndElderlyResidents + 1;
					} else {
						if(user.disability == true) {
							disabledOrElderly = true;
							numOfDisabledResidents = numOfDisabledResidents + 1;
						}
						var dob = new Date(user.dob);
						var currDate = new Date();
						if(dob < currDate.getFullYear() - 70) {
							disabledOrElderly = true;
							numOfElderlyResidents = numOfElderlyResidents + 1;
						}
						if(disabledOrElderly == false) {
							numOfNormalResidents = numOfNormalResidents + 1;
						}
					}
					$scope.numOfRes = $scope.numOfRes + 1;
				} else if(user.authority == "ROLE_VOLUNTEER") {
					$scope.numOfVol = $scope.numOfVol + 1;
				}
				$scope.userInformation.push(user);
			})
			
			$scope.numOfElderlyAndDisabled = numOfDisabledAndElderlyResidents;
			$scope.numOfElderlyResidents = numOfElderlyResidents;
			$scope.numOfDisabledResidents = numOfDisabledResidents;
			$scope.numOfNormalResidents = numOfNormalResidents;
			
			$scope.numOfUsers = $scope.userInformation.length -1;
			$scope.makePieChartTypeOfResidents($scope.numOfElderlyResidents, $scope.numOfDisabledResidents, $scope.numOfElderlyAndDisabled, $scope.numOfNormalResidents);
			$scope.makePieChartAllUsers($scope.numOfRes, $scope.numOfVol);
		});
	};
		
		$scope.makePieChartForBusiestTimesWithCouncil = function(hourObj) {
			AmCharts.makeChart("chartdiv4",
				{
					"type": "serial",
					"categoryField": "date",
					"dataDateFormat": "YYYY-MM-DD HH",
					"categoryAxis": {
						"minPeriod": "hh",
						"parseDates": true
					},
					"export": {
					    "enabled": true,
					    "libs": {
					      "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
					    },
					    "menu": []
					  },
					"chartCursor": {
						"enabled": true,
						"categoryBalloonDateFormat": "JJ:NN"
					},
					"chartScrollbar": {
						"enabled": false
					},
					"trendLines": [],
					"graphs": [
						{
							"bullet": "round",
							"id": "AmGraph-1",
							"title": "2 Busiest Report Times",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "No Of Reports"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Busiest Times for the Busiest Day"
						}
					],
					"dataProvider": [
						{
							"column-1": 10,
							"date": "2014-03-01 00"
						},
						{
							"column-1": hourObj.oneAM,
							"date": "2014-03-01 01"
						},
						{
							"column-1": hourObj.twoAM,
							"date": "2014-03-01 02"
						},
						{
							"column-1": hourObj.threeAM,
							"date": "2014-03-01 03"
						},
						{
							"column-1": hourObj.fourAM,
							"date": "2014-03-01 04"
						},
						{
							"column-1": hourObj.fiveAM,
							"date": "2014-03-01 05"
						},
						{
							"column-1": hourObj.sixAM,
							"date": "2014-03-01 06"
						},
						{
							"column-1": hourObj.sevenAM,
							"date": "2014-03-01 07"
						},
						{
							"column-1": hourObj.eightAM,
							"date": "2014-03-01 08"
						},
						{
							"column-1": hourObj.nineAM,
							"date": "2014-03-01 09"
						},
						{
							"column-1": hourObj.tenAM,
							"date": "2014-03-01 10"
						},
						{
							"column-1": hourObj.elevenAM,
							"date": "2014-03-01 11"
						},
						{
							"column-1": hourObj.twelvePM,
							"date": "2014-03-01 12"
						},
						{
							"column-1": hourObj.onePM,
							"date": "2014-03-01 13"
						},
						{
							"column-1": hourObj.twoPM,
							"date": "2014-03-01 14"
						},
						{
							"column-1": hourObj.threePM,
							"date": "2014-03-01 15"
						},
						{
							"column-1": hourObj.fourPM,
							"date": "2014-03-01 16"
						},
						{
							"column-1": hourObj.fivePM,
							"date": "2014-03-01 17"
						},
						{
							"column-1": hourObj.sixPM,
							"date": "2014-03-01 18"
						},
						{
							"column-1": hourObj.sevenPM,
							"date": "2014-03-01 19"
						},
						{
							"column-1": hourObj.eightPM,
							"date": "2014-03-01 20"
						},
						{
							"column-1": hourObj.ninePM,
							"date": "2014-03-01 21"
						},
						{
							"column-1": hourObj.tenPM,
							"date": "2014-03-01 22"
						},
						{
							"column-1": hourObj.elevenPM,
							"date": "2014-03-01 23"
						}
						,
						{
							"column-1": hourObj.elevenPM,
							"date": "2014-03-01 00"
						}
					]
				}
			);
		}
		
		$scope.makePieChartForBusiestTimes = function(hourObj) {
			AmCharts.makeChart("chartdiv4",
				{
					"type": "serial",
					"categoryField": "date",
					"dataDateFormat": "YYYY-MM-DD HH",
					"categoryAxis": {
						"minPeriod": "hh",
						"parseDates": true
					},
					"export": {
					    "enabled": true,
					    "libs": {
					      "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
					    },
					    "menu": []
					  },
					"chartCursor": {
						"enabled": true,
						"categoryBalloonDateFormat": "JJ:NN"
					},
					"chartScrollbar": {
						"enabled": false
					},
					"trendLines": [],
					"graphs": [
						{
							"bullet": "round",
							"id": "AmGraph-1",
							"title": "Busiest Report Times",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "No Of Reports"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Busiest Times for the Busiest Day"
						}
					],
					"dataProvider": [
						{
							"column-1": hourObj.oneAM,
							"date": "2014-03-01 01"
						},
						{
							"column-1": hourObj.twoAM,
							"date": "2014-03-01 02"
						},
						{
							"column-1": hourObj.threeAM,
							"date": "2014-03-01 03"
						},
						{
							"column-1": hourObj.fourAM,
							"date": "2014-03-01 04"
						},
						{
							"column-1": hourObj.fiveAM,
							"date": "2014-03-01 05"
						},
						{
							"column-1": hourObj.sixAM,
							"date": "2014-03-01 06"
						},
						{
							"column-1": hourObj.sevenAM,
							"date": "2014-03-01 07"
						},
						{
							"column-1": hourObj.eightAM,
							"date": "2014-03-01 08"
						},
						{
							"column-1": hourObj.nineAM,
							"date": "2014-03-01 09"
						},
						{
							"column-1": hourObj.tenAM,
							"date": "2014-03-01 10"
						},
						{
							"column-1": hourObj.elevenAM,
							"date": "2014-03-01 11"
						},
						{
							"column-1": hourObj.twelvePM,
							"date": "2014-03-01 12"
						},
						{
							"column-1": hourObj.onePM,
							"date": "2014-03-01 13"
						},
						{
							"column-1": hourObj.twoPM,
							"date": "2014-03-01 14"
						},
						{
							"column-1": hourObj.threePM,
							"date": "2014-03-01 15"
						},
						{
							"column-1": hourObj.fourPM,
							"date": "2014-03-01 16"
						},
						{
							"column-1": hourObj.fivePM,
							"date": "2014-03-01 17"
						},
						{
							"column-1": hourObj.sixPM,
							"date": "2014-03-01 18"
						},
						{
							"column-1": hourObj.sevenPM,
							"date": "2014-03-01 19"
						},
						{
							"column-1": hourObj.eightPM,
							"date": "2014-03-01 20"
						},
						{
							"column-1": hourObj.ninePM,
							"date": "2014-03-01 21"
						},
						{
							"column-1": hourObj.tenPM,
							"date": "2014-03-01 22"
						},
						{
							"column-1": hourObj.elevenPM,
							"date": "2014-03-01 23"
						}
					]
				}
			);
		}
	
}]);