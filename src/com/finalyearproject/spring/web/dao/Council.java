package com.finalyearproject.spring.web.dao;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="councils")
public class Council {
	
	@Id
	private String councilname;
	private String contactnum;
	private String councilemail;
	private String councilhq;
	
	public Council() {
		
	}

	public Council(String councilname, String contactnum, String councilemail, String councilhq) {
		this.councilname = councilname;
		this.contactnum = contactnum;
		this.councilemail = councilemail;
		this.councilhq = councilhq;
	}

	public String getCouncilname() {
		return councilname;
	}

	public void setCouncilname(String councilname) {
		this.councilname = councilname;
	}

	public String getContactnum() {
		return contactnum;
	}

	public void setContactnum(String contactnum) {
		this.contactnum = contactnum;
	}

	public String getCouncilemail() {
		return councilemail;
	}

	public void setCouncilemail(String councilemail) {
		this.councilemail = councilemail;
	}

	public String getCouncilhq() {
		return councilhq;
	}

	public void setCouncilhq(String councilhq) {
		this.councilhq = councilhq;
	}

	@Override
	public String toString() {
		return "Council [councilname=" + councilname + ", contactnum=" + contactnum + ", councilemail=" + councilemail
				+ ", councilhq=" + councilhq + "]";
	}
	
}
