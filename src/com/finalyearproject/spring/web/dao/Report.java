package com.finalyearproject.spring.web.dao;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="reports")
public class Report implements Comparable {
	
	@Id
	@GeneratedValue
	private int id;
	private Date date;
	private String address1;
	private String city;
	private String county;
	private double latitude;
	private double longitude;
	private String category;
	private String description;
	@ManyToOne
	@JoinColumn(name="username")
	private User user;
	@ManyToOne
	@JoinColumn(name="councilname")
	private Council council;
	@Transient
	private int reportCount;
	
	
	public Report() {
		this.user = new User();
	}

	public Report(User user, Council council, Date date, String address1,
			String city, String county, double latitude, double longitude, String category, String description) {
		this.user = user;
		this.council = council;
		this.date = date;
		this.address1 = address1;
		this.city = city;
		this.county = county;
		this.latitude = latitude;
		this.longitude = longitude;
		this.category = category;
		this.description = description;
	}
	
	public Report(Council council, String category, int reportCount) {
		this.council = council;
		this.category = category;
		this.reportCount = reportCount;
	}
	
	public Report(String category, int reportCount) {
		this.category = category;
		this.reportCount = reportCount;
	}
	
	public Report(int id, User user, Council council, Date date, String address1,
			String city, String county, double latitude, double longitude, String category, String description) {
		this.id = id;
		this.user = user;
		this.council = council;
		this.date = date;
		this.address1 = address1;
		this.city = city;
		this.county = county;
		this.latitude = latitude;
		this.longitude = longitude;
		this.category = category;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
	
	public int getReportCount() {
		return reportCount;
	}

	public void setReportCount(int reportCount) {
		this.reportCount = reportCount;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUsername() {
		return user.getUsername();
	}

	public Council getCouncil() {
		return council;
	}

	public void setCouncil(Council council) {
		this.council = council;
	}

	public String getCouncilname() {
		return council.getCouncilname();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address1 == null) ? 0 : address1.hashCode());
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((county == null) ? 0 : county.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		long temp;
		temp = Double.doubleToLongBits(latitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(longitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Report other = (Report) obj;
		if (address1 == null) {
			if (other.address1 != null)
				return false;
		} else if (!address1.equals(other.address1))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (county == null) {
			if (other.county != null)
				return false;
		} else if (!county.equals(other.county))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (Double.doubleToLongBits(latitude) != Double.doubleToLongBits(other.latitude))
			return false;
		if (Double.doubleToLongBits(longitude) != Double.doubleToLongBits(other.longitude))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Report [id=" + id + ", date=" + date + ", address1=" + address1 + ", city=" + city + ", county="
				+ county + ", latitude=" + latitude + ", longitude=" + longitude + ", category=" + category
				+ ", description=" + description + ", user=" + user + "]";
	}

	@Override
	public int compareTo(Object comparestu) {
		int compareId = ((Report)comparestu).getId();
		return this.id-compareId;
	}
	
}
