package com.finalyearproject.spring.web.test.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Date;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.finalyearproject.spring.web.dao.Council;
import com.finalyearproject.spring.web.dao.CouncilsDao;
import com.finalyearproject.spring.web.dao.Volunteer;
import com.finalyearproject.spring.web.dao.VolunteersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = { "classpath:com/finalyearproject/spring/web/config/dao-context.xml",
		"classpath:com/finalyearproject/spring/web/config/security-context.xml",
		"classpath:com/finalyearproject/spring/web/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class VolunteersDaoTests {

	@Autowired
	private VolunteersDao volunteersDao;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private CouncilsDao councilsDao;
	
	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		jdbc.execute("delete from checkups");
		jdbc.execute("delete from reports");
		jdbc.execute("delete from residents");
		jdbc.execute("delete from volunteers");
		jdbc.execute("delete from councils");
		jdbc.execute("delete from users");
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testCreateVolunteer() {

		Council council = new Council("Fingal County Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council);

		Date myDob = new Date();
		myDob.setYear(myDob.getYear() - 18);

		Volunteer vol = new Volunteer("SWard123", "Shane1994", "Shane@gmail.com", true, "ROLE_VOLUNTEER", 
				"Shane", "Ward", "16 Swords Manor Drive", "Swords", "Dublin", myDob, "0892052140", 
				53.461638, -6.242750, council);
		
		assertEquals("Volunteer created but not saved.", 0, volunteersDao.getAllVolunteers().size());
		volunteersDao.create(vol);
		assertEquals("The volunteer has been saved so there should be one in the database", 1,
				volunteersDao.getAllVolunteers().size());
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testPasswordEncryption() {
		Council council = new Council("Fingal County Council", "(0)1 890 5000", "info@fingalcoco.ie",
				"County Hall, Main St, Swords, Co. Dublin");
		councilsDao.saveOrUpdate(council);

		Date myDob = new Date();
		myDob.setYear(myDob.getYear() - 18);
		Volunteer vol = new Volunteer("SWard123", "Shane1994", "Shane@gmail.com", true, "ROLE_VOLUNTEER", 
				"Shane", "Ward", "16 Swords Manor Drive", "Swords", "Dublin", myDob, "0892052140", 
				53.461638, -6.242750, council);	
		String password = vol.getPassword();
		volunteersDao.create(vol);
		
		Volunteer retrievedVolunteer = volunteersDao.getVolunteerByUsername("SWard123");
		String retreivedPassword = retrievedVolunteer.getPassword();
		assertNotEquals("The password should be encrypted", password, retreivedPassword);
	}
	
	
}
