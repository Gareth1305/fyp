var myApp = angular.module("statsVolResView", []);

myApp.controller('statsVolResCtrl', [ '$scope', '$http',
	function($scope, $http) {
	
		$scope.userObj = {};
	
		$scope.getUserInfo = function() {
			$http.get('http://localhost:8080/prasbic/api/getVolunteerById/'+$scope.username).success(function (volunteer) {
				$scope.userObj = volunteer;
				$scope.populateGraphs();
			});
		}
	
		$scope.populateGraphs = function() {
			$scope.initChart1();
			$scope.initChart2();
			$scope.initChart3();
			$scope.initChart4();
		}
	
		$scope.initChart1 = function() {
			$http.get('http://localhost:8080/prasbic/api/getResidentsByCouncilName/' + $scope.userObj.council.councilname).success(function (residents) {
				var numOfElderlyResidents = 0;
				var numOfDisabledResidents = 0;
				var numOfDisabledAndElderlyResidents = 0;
				var numOfNormalResidents = 0;
				var volunteerLatLng = new google.maps.LatLng($scope.userObj.latitude, $scope.userObj.longitude);
				residents.forEach(function(res) {
					var reportLatLng = new google.maps.LatLng(res.latitude, res.longitude);
					var dis = $scope.getDistance(volunteerLatLng, reportLatLng);

					var disabledOrElderly = false;
					var dob = new Date(res.dob);
					var currDate = new Date();
					
					var dis = $scope.getDistance(volunteerLatLng, reportLatLng);
					
					if(dis <= 250) {
						if(res.disability == true && (dob < currDate.getFullYear() - 70)) {
							numOfDisabledAndElderlyResidents = numOfDisabledAndElderlyResidents + 1;
						} else {
							if(res.disability == true) {
								disabledOrElderly = true;
								numOfDisabledResidents = numOfDisabledResidents + 1;
							}
							if(dob.getFullYear() < currDate.getFullYear() - 70) {
								disabledOrElderly = true;
								numOfElderlyResidents = numOfElderlyResidents + 1;
							}
							if(disabledOrElderly == false) {
								numOfNormalResidents = numOfNormalResidents + 1;
							}
						}
					}
				});
				$scope.createChart1(numOfNormalResidents, numOfDisabledResidents, numOfElderlyResidents, numOfDisabledAndElderlyResidents);
			});
		}
		
		$scope.createChart1 = function(nor, dis, eld, both) {
			AmCharts.makeChart("chartdiv1",
					{
						"type": "pie",
						"angle": 35,
						"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
						"depth3D": 20,
						"colors": [
							"#FF0F00",
							"#04D215",
							"#0D8ECF",
							"#FFFF00"
						],
						"titleField": "category",
						"valueField": "column-1",
						"theme": "default",
						"allLabels": [],
						"balloon": {},
						"legend": {
							"enabled": true,
							"align": "center",
							"markerType": "circle"
						},
						"titles": [{
							"id": "Title-1",
							"size": 14,
							"text": "Resident Types - Your Area"
						}],
						"dataProvider": [
							{
								"category": "Normal Residents",
								"column-1": nor
							},
							{
								"category": "Elderly Residents",
								"column-1": eld
							},
							{
								"category": "Elderly/Disabled Residents",
								"column-1": both
							},
							{
								"category": "Disabled Residents",
								"column-1": dis
							}
						]
					}
				);
		}
	
		$scope.initChart2 = function() {
			$http.get('http://localhost:8080/prasbic/api/getAllReportsByCouncil/' + $scope.userObj.council.councilname).success(function (reports) {
				
				var numOfElderlyResidents = 0;
				var numOfDisabledResidents = 0;
				var numOfDisabledAndElderlyResidents = 0;
				var numOfNormalResidents = 0;
				var volunteerLatLng = new google.maps.LatLng($scope.userObj.latitude, $scope.userObj.longitude);

				reports.forEach(function(report) {
					var reportLatLng = new google.maps.LatLng(report.user.latitude, report.user.longitude);

					var disabledOrElderly = false;
					var dob = new Date(report.user.dob);
					var currDate = new Date();
					
					var dis = $scope.getDistance(volunteerLatLng, reportLatLng);
					
					if(dis <= 250) {
						if(report.user.disability == true && (dob < currDate.getFullYear() - 70)) {
							numOfDisabledAndElderlyResidents = numOfDisabledAndElderlyResidents + 1;
						} else {
							if(report.user.disability == true) {
								disabledOrElderly = true;
								numOfDisabledResidents = numOfDisabledResidents + 1;
							}
							var dob = new Date(report.user.dob);
							var currDate = new Date();
							if(dob.getFullYear() < currDate.getFullYear() - 70) {
								disabledOrElderly = true;
								numOfElderlyResidents = numOfElderlyResidents + 1;
							}
							if(disabledOrElderly == false) {
								numOfNormalResidents = numOfNormalResidents + 1;
							}
						}
					}
				})				
				$scope.createChart2(numOfNormalResidents, numOfDisabledResidents, numOfElderlyResidents, numOfDisabledAndElderlyResidents);
			});
		}
		
		$scope.createChart2 = function(nor, dis, eld, both) {
			AmCharts.makeChart("chartdiv2",
					{
						"type": "pie",
						"angle": 35,
						"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
						"depth3D": 20,
						"colors": [
							"#FF0F00",
							"#04D215",
							"#0D8ECF",
							"#FFFF00"
						],
						"titleField": "category",
						"valueField": "column-1",
						"theme": "default",
						"allLabels": [],
						"balloon": {},
						"legend": {
							"enabled": true,
							"align": "center",
							"markerType": "circle"
						},
						"titles": [{
							"id": "Title-1",
							"size": 14,
							"text": "Percentage Of Reports By User Type In Your Area"
						}],
						"dataProvider": [
							{
								"category": "Normal Residents",
								"column-1": nor
							},
							{
								"category": "Elderly Residents",
								"column-1": eld
							},
							{
								"category": "Elderly/Disabled Residents",
								"column-1": both
							},
							{
								"category": "Disabled Residents",
								"column-1": dis
							}
						]
					}
				);
		}
		
		$scope.initChart3 = function() {
			$http.get('http://localhost:8080/prasbic/api/getAllReportsByCouncil/' + $scope.userObj.council.councilname).success(function (reports) {
				
				var dayValuesObj = {};
				var monVal = 0;
				var tueVal = 0;
				var wedVal = 0;
				var thursVal = 0;
				var friVal = 0;
				var satVal = 0;
				var sunVal = 0;
				
				var volunteerLatLng = new google.maps.LatLng($scope.userObj.latitude, $scope.userObj.longitude);
				
				reports.forEach(function(report) {
					var reportLatLng = new google.maps.LatLng(report.user.latitude, report.user.longitude);
					var dis = $scope.getDistance(volunteerLatLng, reportLatLng);
					
					if(dis <= 250) { 
						var dateStr = JSON.parse(report.date);  			        
						var date = new Date(dateStr);
						
						switch (date.getDay()) {
					    case 0:
					        sunVal = sunVal + 1;
					        break;
					    case 1:
					    	monVal = monVal + 1;
					        break;
					    case 2:
					    	tueVal = tueVal + 1;
					        break;
					    case 3:
					    	wedVal = wedVal + 1;
					        break;
					    case 4:
					    	thursVal = thursVal + 1;
					        break;
					    case 5:
					    	friVal = friVal + 1;
					        break;
					    case 6:
					    	satVal = satVal + 1;
					        break;
						}
					}
				});
				dayValuesObj.monVal = monVal;
				dayValuesObj.tueVal = tueVal;
				dayValuesObj.wedVal = wedVal;
				dayValuesObj.thursVal = thursVal;
				dayValuesObj.friVal = friVal;
				dayValuesObj.satVal = satVal;
				dayValuesObj.sunVal = sunVal;
				$scope.createChart3(dayValuesObj);
			});
		}
		
		$scope.createChart3 = function(dayValues) {
			AmCharts.makeChart("chartdiv3",
					{
						"type": "serial",
						"categoryField": "category",
						"export": {
						    "enabled": true,
						    "libs": {
						      "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
						    },
						    "menu": []
						  },
						"marginLeft": 19,
						"marginTop": 21,
						"startDuration": 1,
						"processCount": 999,
						"theme": "light",
						"categoryAxis": {
							"gridPosition": "start"
						},
						"trendLines": [],
						"graphs": [
							{
								"colorField": "color",
								"fillAlphas": 1,
								"id": "AmGraph-1",
								"lineColorField": "color",
								"title": "graph 1",
								"type": "column",
								"valueField": "column-1"
							}
						],
						"guides": [],
						"valueAxes": [
							{
								"id": "ValueAxis-1",
								"title": "No Of Repors"
							}
						],
						"allLabels": [],
						"balloon": {},
						"titles": [
							{
								"id": "Title-1",
								"size": 12,
								"text": "Busiest Report Days (For Your Area)"
							}
						],
						"dataProvider": [
							{
								"category": "Mon",
								"column-1": dayValues.monVal
							},
							{
								"category": "Tue",
								"column-1": dayValues.tueVal
							},
							{
								"category": "Wed",
								"column-1": dayValues.wedVal
							},
							{
								"category": "Thu",
								"column-1": dayValues.thursVal
							},
							{
								"category": "Fri",
								"column-1": dayValues.friVal
							},
							{
								"category": "Sat",
								"column-1": dayValues.satVal
							},
							{
								"category": "Sun",
								"column-1": dayValues.sunVal
							}
						]
					}
				);
		}
		
		$scope.initChart4 = function() {
			$http.get('http://localhost:8080/prasbic/api/getBusiestHoursWithCouncil/' + $scope.userObj.council.councilname).success(function (reports) {
				var anObj = {
						"oneAM" : 0,"twoAM" : 0,"threeAM" : 0,"fourAM" : 0,"fiveAM" : 0,"sixAM" : 0,
						"sevenAM" : 0,"eightAM" : 0,"nineAM" : 0,"tenAM" : 0,"elevenAM" : 0,"twelvePM" : 0,
						"onePM" : 0,"twoPM" : 0,"threePM" : 0,"fourPM" : 0,"fivePM" : 0,"sixPM" : 0,
						"sevenPM" : 0,"eightPM" : 0,"ninePM" : 0,"tenPM" : 0,"elevenPM" : 0
				};
				
				
				var volunteerLatLng = new google.maps.LatLng($scope.userObj.latitude, $scope.userObj.longitude);
				
				reports.forEach(function(rep) {
					var reportLatLng = new google.maps.LatLng(rep.user.latitude, rep.user.longitude);
					var dis = $scope.getDistance(volunteerLatLng, reportLatLng);
					
					if(dis <= 250) { 						
						var myDate = new Date(rep.date);
						switch (myDate.getHours()) {
						    case 1: anObj.oneAM = anObj.oneAM + 1; break;
						    case 2: anObj.twoAM = anObj.twoAM + 1; break;
						    case 3: anObj.threeAM = anObj.threeAM + 1; break;
						    case 4: anObj.fourAM = anObj.fourAM + 1; break;
						    case 5: anObj.fiveAM = anObj.fiveAM + 1; break;
						    case 6: anObj.sixAM = anObj.sixAM + 1; break;
						    case 7: anObj.sevenAM = anObj.sevenAM + 1; break;
						    case 8: anObj.eightAM = anObj.eightAM + 1; break;
						    case 9: anObj.nineAM = anObj.nineAM + 1; break;
						    case 10: anObj.tenAM = anObj.tenAM + 1; break;
						    case 11: anObj.elevenAM = anObj.elevenAM + 1; break;
						    case 12: anObj.twelvePM = anObj.twelvePM + 1; break;
						    case 13: anObj.onePM = anObj.onePM + 1; break;
						    case 14: anObj.twoPM = anObj.twoPM + 1; break;
						    case 15: anObj.threePM = anObj.threePM + 1; break;
						    case 16: anObj.fourPM = anObj.fourPM + 1; break;
						    case 17: anObj.fivePM = anObj.fivePM + 1; break;
						    case 18: anObj.sixPM = anObj.sixPM + 1; break;
						    case 19: anObj.sevenPM = anObj.sevenPM + 1; break;
						    case 20: anObj.eightPM = anObj.eightPM + 1; break;
						    case 21: anObj.ninePM = anObj.ninePM + 1; break;
						    case 22: anObj.tenPM = anObj.tenPM + 1; break;
						    case 23: anObj.elevenPM = anObj.elevenPM + 1; break;
						}
					}				
				});
				$scope.createChart4(anObj);
			});
		}
		
		$scope.createChart4 = function(hourObj) {
			AmCharts.makeChart("chartdiv4",
					{
						"type": "serial",
						"categoryField": "date",
						"dataDateFormat": "YYYY-MM-DD HH",
						"categoryAxis": {
							"minPeriod": "hh",
							"parseDates": true
						},
						"export": {
						    "enabled": true,
						    "libs": {
						      "path": "http://www.amcharts.com/lib/3/plugins/export/libs/"
						    },
						    "menu": []
						  },
						"chartCursor": {
							"enabled": true,
							"categoryBalloonDateFormat": "JJ:NN"
						},
						"chartScrollbar": {
							"enabled": false
						},
						"trendLines": [],
						"graphs": [
							{
								"bullet": "round",
								"id": "AmGraph-1",
								"title": "graph 1",
								"valueField": "column-1"
							}
						],
						"guides": [],
						"valueAxes": [
							{
								"id": "ValueAxis-1",
								"title": "Axis title"
							}
						],
						"allLabels": [],
						"balloon": {},
						"legend": {
							"enabled": true,
							"useGraphSettings": true
						},
						"titles": [
							{
								"id": "Title-1",
								"size": 15,
								"text": "Busiest Times for the Busiest Day"
							}
						],
						"dataProvider": [
							{
								"column-1": hourObj.oneAM,
								"date": "2014-03-01 01"
							},
							{
								"column-1": hourObj.twoAM,
								"date": "2014-03-01 02"
							},
							{
								"column-1": hourObj.threeAM,
								"date": "2014-03-01 03"
							},
							{
								"column-1": hourObj.fourAM,
								"date": "2014-03-01 04"
							},
							{
								"column-1": hourObj.fiveAM,
								"date": "2014-03-01 05"
							},
							{
								"column-1": hourObj.sixAM,
								"date": "2014-03-01 06"
							},
							{
								"column-1": hourObj.sevenAM,
								"date": "2014-03-01 07"
							},
							{
								"column-1": hourObj.eightAM,
								"date": "2014-03-01 08"
							},
							{
								"column-1": hourObj.nineAM,
								"date": "2014-03-01 09"
							},
							{
								"column-1": hourObj.tenAM,
								"date": "2014-03-01 10"
							},
							{
								"column-1": hourObj.elevenAM,
								"date": "2014-03-01 11"
							},
							{
								"column-1": hourObj.twelvePM,
								"date": "2014-03-01 12"
							},
							{
								"column-1": hourObj.onePM,
								"date": "2014-03-01 13"
							},
							{
								"column-1": hourObj.twoPM,
								"date": "2014-03-01 14"
							},
							{
								"column-1": hourObj.threePM,
								"date": "2014-03-01 15"
							},
							{
								"column-1": hourObj.fourPM,
								"date": "2014-03-01 16"
							},
							{
								"column-1": hourObj.fivePM,
								"date": "2014-03-01 17"
							},
							{
								"column-1": hourObj.sixPM,
								"date": "2014-03-01 18"
							},
							{
								"column-1": hourObj.sevenPM,
								"date": "2014-03-01 19"
							},
							{
								"column-1": hourObj.eightPM,
								"date": "2014-03-01 20"
							},
							{
								"column-1": hourObj.ninePM,
								"date": "2014-03-01 21"
							},
							{
								"column-1": hourObj.tenPM,
								"date": "2014-03-01 22"
							},
							{
								"column-1": hourObj.elevenPM,
								"date": "2014-03-01 23"
							}
						]
					}
				);
		}
		
		$scope.rad = function(x) {
	    	return x * Math.PI / 180;
		};

		$scope.getDistance = function(p1, p2) {
			var R = 6378137; // Earth’s mean radius in meter
			var dLat = $scope.rad(p2.lat() - p1.lat());
			var dLong = $scope.rad(p2.lng() - p1.lng());
			var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.cos($scope.rad(p1.lat())) * Math.cos($scope.rad(p2.lat())) *
				Math.sin(dLong / 2) * Math.sin(dLong / 2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			var d = R * c;
			return d;
		};
} ]);