var myApp = angular.module("myApp", []);

//    $scope.items = [];
//    var i = 0;
//    $interval(function () {
//        $http.get('http://localhost:8080/prasbic/api/getAllReports').
//        success(function (data) {
//            if($scope.reports.length != 0) {
//            	var dataObj = [];
//            	for(var l=0; l<data.length; l++) {
//            		var dataLoc = new google.maps.LatLng(data[l].latitude, data[l].longitude);
//            		var volunLoc = new google.maps.LatLng($scope.volLocation.lat, $scope.volLocation.lng);
//            		var d = getDistance(dataLoc, volunLoc);
//            		if(d < 250) {
//            			dataObj.push(data[l]);
//            		}
//            	}
//            	if(dataObj.length > $scope.reports.length) {
//                    for(var j=$scope.reports.length; j<dataObj.length; j++) {
//                        $scope.reports.push(dataObj[j]);
//                    }
//                    $scope.reports.length = dataObj.length;
//                }
//            } else {
//            	console.log("Reports have not beeen initialized as the vols address has not been found")
//            }
//        });
//    }, 10000);
//}]);
//
myApp.filter('orderObjectBy', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };
});

myApp.controller('ResidentInfoCtrl', ['$scope', '$interval', '$http', function($scope, $interval, $http, $dialogs) {
	$scope.setVolInfo = function() {
		$http.get('http://localhost:8080/prasbic/api/getVolunteerByUsername/'+$scope.username)
	    .success(function (data2) {
	    	$scope.volunteerObj = data2;
	    	if($scope.volunteerObj == undefined) {
	    		$scope.setVolInfo()
	    	} else {
	    		$scope.showResidentsToLookAfter();
	    	}
	    });
	}
	
	$scope.rad = function(x) {
    	return x * Math.PI / 180;
	};

	$scope.getDistance = function(p1, p2) {
		var R = 6378137; // Earth’s mean radius in meter
		var dLat = $scope.rad(p2.lat() - p1.lat());
		var dLong = $scope.rad(p2.lng() - p1.lng());
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos($scope.rad(p1.lat())) * Math.cos($scope.rad(p2.lat())) *
			Math.sin(dLong / 2) * Math.sin(dLong / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;
		return d;
	};
	
	$scope.resObjs = [];
	$scope.volObj = {};
	$scope.volAddress = {};
		
	$scope.showResidentsToLookAfter = function() {		
		var volLatlngToCheck = new google.maps.LatLng($scope.volunteerObj.latitude, $scope.volunteerObj.longitude);
		
			$http.get('http://localhost:8080/prasbic/api/getElderlyAndDisabledResidents')
		    .success(function (data) {   
		    	data.forEach(function(entry) {
		    		var resLatlngToCheck = new google.maps.LatLng(entry.latitude, entry.longitude);
		    		$http.get('http://localhost:8080/prasbic/api/getCheckupsByUsername/'+entry.username)
		    	    .success(function (data2) {	    	
		    	    	entry.checkups = data2;
		    	    	entry.checkupslength = data2.length;
		    	    });
		    		$http.get('http://localhost:8080/prasbic/api/getReportsByUsername/'+entry.username)
		    	    .success(function (data2) {
		    	    	entry.reports = data2;
		    	    	entry.reportlength = data2.length;
		    	    });
		    		if($scope.getDistance(volLatlngToCheck, resLatlngToCheck) < 250) {
		    			$scope.resObjs.push(entry);
		    		} else {
		    			console.log("Elderly/Disabled Res is outside the vol area");
		    		}
		    		
		    	});
		    });
	}
	
	$scope.geocodeAddressToLatLng = function(address) {
		var geocoder = new google.maps.Geocoder;
		geocoder.geocode({'address': address}, function(results, status) {
		    if (status === google.maps.GeocoderStatus.OK) {
		    	$scope.volAddress.lat = results[0].geometry.location.lat();
		    	$scope.volAddress.lng = results[0].geometry.location.lng();
		    }
		});
	}	
	
	$scope.currUser = {};
	
	$scope.setUserInfo = function(user) {
		console.log(user);
		var dob = new Date(user.dob);
		var currDate = new Date();
		if((currDate.getFullYear() - dob.getFullYear()) > 70) {
			user.isElderly = "Yes";
		} else {
			user.isElderly = "No";
		}
		if(user.checkups.length == 0) {
			if(user.reports.length!=0) {
				user.reports.forEach(function(report) {
					report.status = false;
				});	
			} else {
				console.log("report length is 0")
			}
		} else {
			if(user.reports.length !=0) {
				user.reports.forEach(function(report) {
					user.checkups.forEach(function(checkup) {
						if(checkup.report.id === report.id) {
							report.status = true;
						}
					});
				});
			}
		}
		$scope.currUser = user;
	};
	 
	$scope.getUser = function() {
		return $scope.currUser;
	}
	$scope.buttonText = "Open";
	$scope.btntoggle = "status_button btn btn-primary";
	$scope.buttonAvailability = false;
	$scope.openUserBtnClicked = function(clickUserReport, currUser) {
		console.log(clickUserReport);
		console.log(currUser);
		var resObjToSend = {};
		resObjToSend.id = clickUserReport.id;
		resObjToSend.username = clickUserReport.username;
		resObjToSend.volUsername = $scope.username;
		$http.post('http://localhost:8080/prasbic/api/checkedOnUser',
				JSON.stringify(resObjToSend)).success(
				function(data, status, headers, config) {
					if (status == 200) {
						currUser.checkupslength = currUser.checkupslength - 1;
						$scope.btntoggle = "status_button btn btn-default";
						$scope.buttonAvailability = true;
						$scope.buttonText = "Closed";
					} else {
						console.log("There has been an error");
					}
				}).error(function(error) {
			alert("An error has occurred")
		});
	}
}]);

myApp.directive("myMaps", ['$http', function ($http, $scope, $interval) {
	return {
		restrict : 'E',
		template : '<div></div>',
		replace : true,
		controller : function($scope, $attrs, $interval) {
			
			var markers = [];
			var numOfReports = 0;
			$scope.numReports = 0;
			$scope.volarea = {};
			$scope.livereportarea = {};
			$scope.circleradius = 250;
			$scope.reportsInVolArea = [];
			
			$scope.reports = [];
			$scope.getReports = function() {
				$http.get('http://localhost:8080/prasbic/api/getAllReports').
	            success(function (data) {  
	            	$scope.numReports = data.length;
	            	$scope.reports = data;
	            });
			};
			
			(function() {
		    	$http.get('http://localhost:8080/prasbic/api/getVolunteerByUsername/'+$scope.username).
	            success(function (data) {   
	            	geocodeAddressToLatLng(data.address+", "+data.city+", "+data.county);
	            });
		    })()
			
		    function geocodeAddressToLatLng(address) {
				var geocoder = new google.maps.Geocoder;
				geocoder.geocode({'address': address}, function(results, status) {
				    if (status === google.maps.GeocoderStatus.OK) {
				    	$scope.volarea.lat = results[0].geometry.location.lat();
				    	$scope.volarea.lng = results[0].geometry.location.lng();
				    	showPosition($scope.volarea.lat, $scope.volarea.lng);
				    }
				  });
			}	
			var map;
			function showPosition(lat, lng) {
				var myLatLng = new google.maps.LatLng(lat, lng);
				var infowindow = new google.maps.InfoWindow();
				map = new google.maps.Map(document.getElementById($attrs.id), {
					center : myLatLng,
					zoom : 15,
					mapTypeId : google.maps.MapTypeId.SATELLITE
				});
				var marker = new google.maps.Marker({
      			  position: myLatLng,
      			  map: map, 
      			  icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
	  			});
	      		google.maps.event.addListener(marker, 'click', function () {
	              infowindow.setContent("<h3>Your Volunteering Area!</h3>");
	              infowindow.open(map, this);
	      		});
	      		var circle = new google.maps.Circle({
	      		  map: map,
	      		  radius: $scope.circleradius,    // 10 miles in metres
	      		  fillColor: '#AA0000',
	      		  strokeColor: '#000000',
	              strokeWeight: 1,
	      		});
	      		circle.bindTo('center', marker, 'position');
				setMarkers();	
			}
			var markers = [];
			function setMarkers() {
				var marker;var infowindow = new google.maps.InfoWindow();
            	for (var i = 0; i < $scope.numReports; i++) {
            		

            		
            		var icon;
            		switch($scope.reports[i].category) {
            			case 'Alcohol Abuse':
            				icon = 'http://localhost:8080/prasbic/static/images/alcohalabuse.png';
            				break;
            			
            			case 'Drug Abuse':
            				icon = 'http://localhost:8080/prasbic/static/images/drugabuse.png';
            				break;
            				
            			case 'Burglary':
            				icon = 'http://localhost:8080/prasbic/static/images/burglary.png';
            				break;
            				
            			case 'Fighting':
            				icon = 'http://localhost:8080/prasbic/static/images/fighting.png';
            				break;
            				
            			case 'Other':
            				icon = 'http://localhost:8080/prasbic/static/images/other.png';
            				break;
            				
            			case 'Theft':
            				icon = 'http://localhost:8080/prasbic/static/images/theft.png';
            				break;
            				
            			case 'Joy Riding':
            				icon = 'http://localhost:8080/prasbic/static/images/joyriding.png';
            				break;
            				
            			case 'Vandalism':
            				icon = 'http://localhost:8080/prasbic/static/images/vandalism.png';
            				break;
            		}
            		
            		var reportLatLng = new google.maps.LatLng($scope.reports[i].latitude, $scope.reports[i].longitude);
    				var volunteerLatLng = new google.maps.LatLng($scope.volarea.lat, $scope.volarea.lng);
    				var dis = $scope.getDistance(volunteerLatLng, reportLatLng);
            		var formattedDate = Math.floor((new Date()-new Date($scope.reports[i].date))/86400000);
            		// The above line only shows reports for the last week.
            		// TODO Either update data or change to 1 month for demo purposes
            		if($scope.reports[i].id == 350) {
        				console.log(new Date())
        				console.log(new Date($scope.reports[i].date))
                		console.log($scope.reports[i]);
                		console.log("Formatted Date: " + formattedDate);
                		console.log("Distance: " + dis);
            		}
            		if(formattedDate <= 7 && formattedDate >= 0) {
//            			console.log($scope.reports[i])
	    				if(dis <= 250) {
//	    					console.log($scope.reports[i])
	    					var date = new Date($scope.reports[i].date);
	    					$scope.reports[i].date = date;
	    					$scope.reportsInVolArea.push($scope.reports[i]);
		    				var p1 = new google.maps.LatLng($scope.reports[i].latitude, $scope.reports[i].longitude);
		            		markerType = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
		            		marker = new google.maps.Marker({
								  map: map,
								  icon: icon,
				      			  position: {lat: $scope.reports[i].latitude, lng: $scope.reports[i].longitude},
				      			  animation: google.maps.Animation.DROP, 
				      			  id: $scope.reports[i].id,
				      			  title: $scope.reports[i].category,
				  			  });
		            		markers.push(marker);
							 google.maps.event.addListener(marker, 'click', (function(marker, i) {
								 return function() {
									 toggleBounce(marker);
								 }
							 })(marker, i));
	    				} else {
							var circle ={
								    path: google.maps.SymbolPath.CIRCLE,
								    fillColor: 'red',
								    fillOpacity: .4,
								    scale: 4,
								    strokeColor: 'black',
								    strokeWeight: .3
								};
							marker = new google.maps.Marker({
								icon: circle,
								map: map, 
								position: {lat: reportLatLng.lat(), lng: reportLatLng.lng()},
								animation: google.maps.Animation.DROP
							});						
	    				}
            		}
            	}
            	$scope.isComplete = true;
			}
			
			var marker;
			function addMarkerWithTimeout(position) {
				var reportLatLng = new google.maps.LatLng(position.latitude, position.longitude);
				var volunteerLatLng = new google.maps.LatLng($scope.volarea.lat, $scope.volarea.lng);
				var dis = $scope.getDistance(volunteerLatLng, reportLatLng);
				var markerType;
				if(dis <= 250) {
					$scope.reportsInVolArea.push(position);
					markerType = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
					var infowindow = new google.maps.InfoWindow();
				}
			}
			function toggleBounce(marker) {
				if (marker.getAnimation() !== null) {
					marker.setAnimation(null);
				} else {
					marker.setAnimation(google.maps.Animation.BOUNCE);
					setTimeout(function () {
				        marker.setAnimation(null);
				    }, 4560);
				}
			}
			
			$scope.selectedRow = null;		
			
			$scope.setClickedRow = function(index, report){
				markers.forEach(function(m) {
					if(m.id == report.id) {
						toggleBounce(m);
					}
				});
				$scope.selectedRow = index;
			}
			
			$scope.rad = function(x) {
		    	return x * Math.PI / 180;
			};

			$scope.getDistance = function(p1, p2) {
				var R = 6378137; // Earth’s mean radius in meter
				var dLat = $scope.rad(p2.lat() - p1.lat());
				var dLong = $scope.rad(p2.lng() - p1.lng());
				var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
					Math.cos($scope.rad(p1.lat())) * Math.cos($scope.rad(p2.lat())) *
					Math.sin(dLong / 2) * Math.sin(dLong / 2);
				var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
				var d = R * c;
				return d;
			};
			
			$interval(function () {
				$http.get('http://localhost:8080/prasbic/api/getAllReports').
	            success(function (data) {   
	            	if(data.length > $scope.numReports) {
	            		for(var i = $scope.numReports; i < data.length; i++) {
	            			var latlngObj = {
	            				latitude: data[i].latitude, 
	            				longitude: data[i].longitude, 
	            				category: data[i].category,
	            				description: data[i].description
	            			};
	            			var newReportLoc = new google.maps.LatLng(data[i].latitude, data[i].longitude);
	            			addMarkerWithTimeout(latlngObj);
	            		}
	            	}
	            	$scope.numReports = data.length;
	            });
		    }, 3000);
		} 
	};	
}])

myApp.filter('orderReportByStatus', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };
});