var app = angular.module("homeApp", []);

app.controller('HomeCtrl', function($scope, $log, $timeout, $http) {
	console.log("Controller has been called");

	$scope.makeGraph = function() {
		AmCharts.makeChart("chartdiv", {
			"type" : "serial",
			"categoryField" : "date",
			"dataDateFormat" : "YYYY-MM",
			"colors" : [ "#3b5998", "#FCD202", "#B0DE09", "#0D8ECF", "#2A0CD0",
					"#CD0D74", "#CC0000", "#00CC00", "#0000CC", "#DDDDDD",
					"#999999", "#333333", "#990000" ],
			"theme" : "default",
			"categoryAxis" : {
				"minPeriod" : "MM",
				"parseDates" : true
			},
			"chartCursor" : {
				"enabled" : true,
				"categoryBalloonDateFormat" : "MMM YYYY"
			},
			"trendLines" : [],
			"graphs" : [ {
				"bullet" : "round",
				"id" : "AmGraph-1",
				"title" : "graph 1",
				"valueField" : "column-1"
			}],
			"guides" : [],
			"valueAxes" : [ {
				"id" : "ValueAxis-1",
				"title" : "No. Of Repoirts"
			} ],
			"allLabels" : [],
			"balloon" : {},
			"legend" : {
				"enabled" : true,
				"useGraphSettings" : true
			},
			"titles" : [ {
				"id" : "Title-1",
				"size" : 15,
				"text" : "Number of Reports Per Month"
			} ],
			"dataProvider" : [ {
				"date" : "2014-01",
				"column-1" : 8
			}, {
				"date" : "2014-02",
				"column-1" : 6
			}, {
				"date" : "2014-03",
				"column-1" : 2
			}, {
				"date" : "2014-04",
				"column-1" : 1
			}, {
				"date" : "2014-05",
				"column-1" : 2
			}, {
				"date" : "2014-06",
				"column-1" : 3
			}, {
				"date" : "2014-07",
				"column-1" : 6
			} ]
		});
	}

});