package com.finalyearproject.spring.web.test.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.finalyearproject.spring.web.dao.User;
import com.finalyearproject.spring.web.dao.UsersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = { "classpath:com/finalyearproject/spring/web/config/dao-context.xml",
		"classpath:com/finalyearproject/spring/web/config/security-context.xml",
		"classpath:com/finalyearproject/spring/web/test/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class UsersDaoTests {

	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private DataSource dataSource;

	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		jdbc.execute("delete from checkups");
		jdbc.execute("delete from reports");
		jdbc.execute("delete from residents");
		jdbc.execute("delete from volunteers");
		jdbc.execute("delete from councils");
		jdbc.execute("delete from users");
	}

	@Test
	public void testCreateUser() {
		User user = new User("JBloggs123", "Joe1994", "Joe@gmail.com", true, "ROLE_ADMIN", "Joe", "Bloggs");
		String password = user.getPassword();
		
		List<User> users = new ArrayList<>();
		users = usersDao.getAllUsers();
		
		assertEquals("No users have been saved. Value should be 0", 0, users.size());
		
		usersDao.create(user);		
		users = usersDao.getAllUsers();

		assertEquals("One user has been saved. Value should be 1", 1, users.size());
		assertNotEquals("Passwords should be different due to encryption", password, users.get(0).getPassword());
	}
	
	@Test
	public void testRetrievalOfUser() {
		User user = new User("JBloggs123", "Joe1994", "Joe@gmail.com", true, "ROLE_ADMIN", "Joe", "Bloggs");
		User user1 = new User("AApplegate123", "Alice1994", "Alice@gmail.com", true, "ROLE_ADMIN", "Alice", "Applegate");
		User user2= new User("GDevlin123", "Ger1994", "Ger@gmail.com", true, "ROLE_ADMIN", "Ger", "Devlin");
		User user3 = new User("CCarey123", "Conor1994", "Conor@gmail.com", true, "ROLE_ADMIN", "Conor", "Carey");
		User user4 = new User("GBurke123", "Glenn1994", "Glenn@gmail.com", true, "ROLE_ADMIN", "Glenn", "Burke");
				
		usersDao.create(user);
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		List<User> users = new ArrayList<>();
		users = usersDao.getAllUsers();
		
		assertEquals("Amount of users should be 5 as 5 are created above", 5, users.size());
		assertNotEquals("Checking an incorrect value for the test case above", 3, users.size());
		
		assertNull("Giving an incorrect user name so I am expecting a null value", usersDao.getUserByUsername("DummyUsername"));
		assertNotNull("Giving a correct username, similar to the test above", usersDao.getUserByUsername(user3.getUsername()));
	}

	@Test
	public void testUpdateUser() {
		User user = new User("JBloggs123", "Joe1994", "Joe@gmail.com", true, "ROLE_ADMIN", "Joe", "Bloggs");
		usersDao.create(user);	
		
		User retrievedUser = usersDao.getUserByUsername("JBloggs123");
		String newFirstname = "Shane";
		retrievedUser.setFirstname(newFirstname);

		assertNotEquals("Usernames should not be the same", user.getFirstname(), retrievedUser.getFirstname());
		
		usersDao.saveOrUpdate(retrievedUser);
		
		User retrievedUser2 = usersDao.getUserByUsername("JBloggs123");
		String retrievedFirstname = retrievedUser2.getFirstname();
		assertEquals("Firstnames should be the same",newFirstname, retrievedFirstname);
	}
	
	@Test
	public void testPasswordEncryption() {
		User user = new User("JBloggs123", "Joe1994", "Joe@gmail.com", true, "ROLE_ADMIN", "Joe", "Bloggs");
		String password = user.getPassword();
		usersDao.create(user);
		
		User retrievedUser = usersDao.getUserByUsername("JBloggs123");
		String retreivedPassword = retrievedUser.getPassword();
		assertNotEquals("The password should be encrupted", password, retreivedPassword);
	}
}
